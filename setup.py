#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import datetime

targets = ('basic', 'pro', 'beta')
defaultTarget = 'beta'
target = defaultTarget
if len(sys.argv) > 1:
    target = sys.argv[1]

if target not in targets:
    print 'Error: Unknown setup target \'%s\'' % target
    print '    --> Target should be one of', targets
    print '    --> default:', defaultTarget
    sys.exit(-1)

print 'Building target [%s]\n' % target

################################################################
# First increment build

print 'Incrementing build number...'

file = open('lib/tool/version.py', 'rt')
lines = []
for line in file:
    if line.startswith('build = '):
        exec(line)
        build += 1
        line = 'build = %d\n' % build
    elif line.startswith('args = '):
        if target == 'basic':
            line = 'args = [\'-baseide\',]\n'
        else:
            line = 'args = []\n'
    elif line.startswith('beta = '):
        if target == 'beta':
            deadline = datetime.datetime.today() + datetime.timedelta(60)  # 60 days minimum
            if deadline.month <= 3:
                deadline = deadline.replace(month = 3, day = 31)
            elif deadline.month <= 6:
                deadline = deadline.replace(month = 6, day = 30)
            elif deadline.month <= 9:
                deadline = deadline.replace(month = 9, day = 30)
            else:
                deadline = deadline.replace(month = 12, day = 31)
            line = 'beta = %s\n' % deadline.strftime('(%Y, %m, %d)')
            print 'Beta expiration date:', deadline.strftime('(%Y, %m, %d)')
        else:
            line = 'beta = ()\n'
    lines.append(line)
file.close()
file = open('lib/tool/version.py', 'wt')
file.writelines(lines)
file.close()

import lib.tool.version as version

print 'Version name =', version.version
print 'Full version = %d.%d.%d.%d' % (version.major, version.minor, version.release, version.build)

################################################################
# then update build versions

print 'Updating \'version.txt\'...'

appName = 'In-Edit'
file = open('version.txt', 'rt')
lines = []
for line in file:
    index = line.find('filevers=(')
    if index >= 0:
        line = line[:index] + 'filevers=(%d, %d, %d, %d),\n' % (version.major, version.minor, version.release, version.build)

    index = line.find('prodvers=(')
    if index >= 0:
        line = line[:index] + 'prodvers=(%d, %d, %d, %d),\n' % (version.major, version.minor, version.release, version.build)

    index = line.find("StringStruct('FileVersion', '")
    if index >= 0:
        line = line[:index] + "StringStruct('FileVersion', '%d.%d.%d.%d'),\n" % (version.major, version.minor, version.release, version.build)

    index = line.find("StringStruct('ProductVersion', '")
    if index >= 0:
        line = line[:index] + "StringStruct('ProductVersion', '%d.%d.%d.%d'),\n" % (version.major, version.minor, version.release, version.build)

    index = line.find("StringStruct('PrivateBuild', '")
    if index >= 0:
        line = line[:index] + "StringStruct('PrivateBuild', '%d'),\n" % (version.build)

    index = line.find("StringStruct('SpecialBuild', '")
    if index >= 0:
        line = line[:index] + "StringStruct('SpecialBuild', '%d')])\n" % (version.build)

    lines.append(line)
file.close()
file = open('version.txt', 'wt')
file.writelines(lines)
file.close()

print 'Updating \'InEdit-setup.iss\'...'

file = open('InEdit-setup.iss', 'rt')
lines = []
for line in file:
    if line.startswith('AppName='):
        line = 'AppName=' + appName + '\n'
    if line.startswith('AppVerName='):
        line = 'AppVerName=' + appName + ' %d.%d\n' % (version.major, version.minor)
    if line.startswith('OutputBaseFilename='):
        line = 'OutputBaseFilename=' + appName + '-%d.%d.%d.%d-setup\n' % (version.major, version.minor, version.release, version.build)
    if line.startswith('UninstallDisplayName='):
        line = 'UninstallDisplayName=' + appName + ' ' + version.version + '\n'

    lines.append(line)
file.close()
file = open('InEdit-setup.iss', 'wt')
file.writelines(lines)
file.close()

#################################################################
import os

#import win32api
#win32api.ShellExecute(0, None,
                        #'C:\Python24\python',
                        #'D:\Develop\Python\pyinstaller_1.0\Build.py in-editIDE.spec',
                        #None,
                        #1)
print 'Make binaries...'
os.spawnl(os.P_WAIT, 'C:\\Python24\\python', 'C:\\Python24\\python', 'D:\\Develop\\Python\\pyinstaller_1.0\\Build.py', 'in-editIDE.spec')

print 'Building setup...'
try:
    import ctypes
except ImportError:
    try:
        import win32api
    except ImportError:
        import os
        os.startfile('InEdit-setup.iss')
    else:
        print "Ok, using win32api."
        win32api.ShellExecute(0, "compile",
                                        'InEdit-setup.iss',
                                        None,
                                        None,
                                        0)
else:
    print "Cool, you have ctypes installed."
    res = ctypes.windll.shell32.ShellExecuteA(0, "compile",
                                              'InEdit-setup.iss',
                                              None,
                                              None,
                                              0)
    if res < 32:
        raise RuntimeError, "ShellExecute failed, error %d" % res

#################################################################
## arguments for the setup() call

#import lib.tool.version as version

#productName = 'InEdit'
#productVersion = version.version

#InEdit = dict(
    #script = "in-editIDE.py",
    #other_resources = [(RT_MANIFEST, 1, manifest_template % dict(prog=productName))],
    #dest_base = r"prog\\" + productName)

#zipfile = r"lib\sharedlib"

#options = {"py2exe": {"compressed": 1,
                      #"optimize": 2}}

#################################################################
#import os

#class InnoScript:
    #def __init__(self,
                 #name,
                 #lib_dir,
                 #dist_dir,
                 #windows_exe_files = [],
                 #lib_files = [],
                 #version = productVersion):
        #self.lib_dir = lib_dir
        #self.dist_dir = dist_dir
        #if not self.dist_dir[-1] in "\\/":
            #self.dist_dir += "\\"
        #self.name = name
        #self.version = version
        #self.windows_exe_files = [self.chop(p) for p in windows_exe_files]
        #self.lib_files = [self.chop(p) for p in lib_files]

    #def chop(self, pathname):
        #assert pathname.startswith(self.dist_dir)
        #return pathname[len(self.dist_dir):]

    #def create(self, pathname="dist\\InEdit.iss"):
        #self.pathname = pathname
        #ofi = self.file = open(pathname, "w")
        #print >> ofi, "; WARNING: This script has been created by py2exe. Changes to this script"
        #print >> ofi, "; will be overwritten the next time py2exe is run!"
        #print >> ofi, r"[Setup]"
        #print >> ofi, r"AppName=%s" % self.name
        #print >> ofi, r"AppVerName=%s %s" % (self.name, self.version)
        #print >> ofi, r"DefaultDirName={pf}\%s" % self.name
        #print >> ofi, r"DefaultGroupName=%s" % self.name
        #print >> ofi, r"OutputBaseFilename=%s-%s-setup" % (self.name, self.version)
        #print >> ofi

        #print >> ofi, r"[Files]"
        #for path in self.windows_exe_files + self.lib_files:
            #print >> ofi, r'Source: "%s"; DestDir: "{app}\%s"; Flags: ignoreversion' % (path, os.path.dirname(path))
###        print >> ofi, r'Source: ..\images\*.gif; DestDir: {app}\images; Flags: overwritereadonly ignoreversion recursesubdirs'
###        print >> ofi, r'Source: ..\images\*.png; DestDir: {app}\images; Flags: overwritereadonly ignoreversion recursesubdirs'
        #print >> ofi

        #print >> ofi, r"[Icons]"
        #for path in self.windows_exe_files:
            #print >> ofi, r'Name: "{group}\%s"; Filename: "{app}\%s"; WorkingDir: {app}' % \
                  #(self.name, path)
        #print >> ofi, 'Name: "{group}\Uninstall %s"; Filename: {uninstallexe}; WorkingDir: {app}' % \
                    #self.name

    #def compile(self):
        #try:
            #import ctypes
        #except ImportError:
            #try:
                #import win32api
            #except ImportError:
                #import os
                #os.startfile(self.pathname)
            #else:
                #print "Ok, using win32api."
                #win32api.ShellExecute(0, "compile",
                                                #self.pathname,
                                                #None,
                                                #None,
                                                #0)
        #else:
            #print "Cool, you have ctypes installed."
            #res = ctypes.windll.shell32.ShellExecuteA(0, "compile",
                                                      #self.pathname,
                                                      #None,
                                                      #None,
                                                      #0)
            #if res < 32:
                #raise RuntimeError, "ShellExecute failed, error %d" % res


#################################################################
