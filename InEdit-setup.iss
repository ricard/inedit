[Setup]
OutputBaseFilename=In-Edit-0.3.0.42-setup
SourceDir=distin-editIDE
AppCopyright=SoftArchi
AppName=In-Edit
AppVerName=In-Edit 0.3
ShowLanguageDialog=yes
AppPublisher=SoftArchi
AppPublisherURL=http://www.softarchi.com
AppSupportURL=http://www.softarchi.com
AppUpdatesURL=http://www.softarchi.com
AppVersion=0.3
AppID={{CA6E1D4F-EB68-4015-8812-319044FD9680}
UninstallDisplayIcon={app}\in-editIDE.exe
UninstallDisplayName=In-Edit 0.3 Early Access
DefaultDirName={pf}\In-Edit
DefaultGroupName=SoftArchi\In-Edit
[Files]
Source: in-editIDE.exe; DestDir: {app}\bin; Flags: overwritereadonly ignoreversion
Source: *.pyd; DestDir: {app}\bin; Flags: overwritereadonly ignoreversion
Source: *.dll; DestDir: {app}\bin; Flags: overwritereadonly ignoreversion
;Source: lib\tool\data\*.png; DestDir: {app}\lib\tool\data; Flags: recursesubdirs overwritereadonly
Source: lib\tool\data\IDE.ico; DestDir: {app}\lib\tool\data; Flags: overwritereadonly
Source: ..\lib\tool\data\Anubis.ico; DestDir: {app}\lib\tool\data; Flags: overwritereadonly
Source: lib\tool\data\*.txt; DestDir: {app}\lib\tool\data; Flags: recursesubdirs overwritereadonly
;Source: images\*.gif; DestDir: {app}\images; Flags: recursesubdirs overwritereadonly
;Source: lexer.pyl; DestDir: {app}; Flags: overwritereadonly
Source: stc-styles.rc.cfg; DestDir: {app}\bin; Flags: overwritereadonly
Source: ..\doc\history.txt; DestDir: {app}\doc
Source: ..\doc\fr\*.*; DestDir: {app}\doc\fr; Flags: recursesubdirs
[Dirs]
Name: {app}\bin
Name: {app}\doc
Name: {app}\lib\tool
Name: {app}\lib\tool\data
;Name: {app}\images
;Name: {app}\images\full
[Icons]
Name: {group}\{cm:UninstallProgram, In-Edit}; Filename: {uninstallexe}
Name: {group}\in-editIDE; Filename: {app}\bin\in-editIDE.exe; WorkingDir: {app}; Comment: in-editIDE; Flags: createonlyiffileexists
Name: {group}\SoftArchi.com; Filename: {app}\Carlipa.url
Name: {userdesktop}\in-editIDE; Filename: {app}\bin\in-editIDE.exe; WorkingDir: {app}; Comment: in-editIDE; Flags: createonlyiffileexists; IconIndex: 0; Tasks: desktopicon
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\in-editIDE; Filename: {app}\bin\in-editIDE.exe; WorkingDir: {app}; Comment: "{cm:ProgramOnTheWeb,""in-editIDE""}"; IconIndex: 0; Tasks: quicklaunchicon; Flags: createonlyiffileexists
[INI]
Filename: {app}\SoftArchi.url; Section: InternetShortcut; Key: URL; String: http://www.softarchi.com/
[UninstallDelete]
Type: files; Name: {app}\SoftArchi.url
Name: {app}\support; Type: filesandordirs
[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}
Name: reset; Description: Reinitialize all settings; GroupDescription: Additional tasks:; Flags: unchecked
[InstallDelete]
Name: {app}\lexer_lextab.py; Type: files
Name: {app}\lexer_lextab.pyc; Type: files
Name: {app}\activegrid; Type: filesandordirs; Tasks: 
Name: {app}\images; Type: filesandordirs
Name: {app}\*.dll; Type: files
Name: {app}\*.pyd; Type: files
Name: {app}\*.pyl; Type: files
Name: {app}\in-editIDE.exe; Type: files
Name: {app}\stc-styles.rc.cfg; Type: files
[Run]
Filename: {app}\bin\in-editIDE.exe; WorkingDir: {app}; Flags: nowait postinstall; Description: {cm:LaunchProgram,In-Edit}
[Registry]
Root: HKCU; Subkey: Software\in-edit IDE; Flags: deletekey; Tasks: reset
Root: HKCR; SubKey: .anubis; ValueType: string; ValueData: Anubis Source File; Flags: uninsdeletekey
Root: HKCR; SubKey: Anubis Source File; ValueType: string; ValueData: Anubis Source File; Flags: uninsdeletekey
Root: HKCR; SubKey: Anubis Source File\Shell\Open\Command; ValueType: string; ValueData: """{app}\bin\in-editIDE.exe"" ""%1"""; Flags: uninsdeletevalue
Root: HKCR; Subkey: Anubis Source File\DefaultIcon; ValueType: string; ValueData: {app}\lib\tool\data\Anubis.ico,-1; Flags: uninsdeletevalue
Root: HKCR; SubKey: .inproj; ValueType: string; ValueData: In-Edit Project File; Flags: uninsdeletekey
Root: HKCR; SubKey: In-Edit Project File; ValueType: string; ValueData: In-Edit Project File; Flags: uninsdeletekey
Root: HKCR; SubKey: In-Edit Project File\Shell\Open\Command; ValueType: string; ValueData: """{app}\bin\in-editIDE.exe"" ""%1"""; Flags: uninsdeletevalue
Root: HKCR; Subkey: In-Edit Project File\DefaultIcon; ValueType: string; ValueData: {app}\lib\tool\data\IDE.ico,0; Flags: uninsdeletevalue
