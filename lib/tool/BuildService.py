#----------------------------------------------------------------------------
# Name:         BuildService.py
# Purpose:      Build Service.
#
# Author:       Cedric RICARD
#
# Created:      05/07/05
# CVS-ID:       $Id: BuildService.py,v 1.1 2005/07/06 17:18:50 ricard Exp $
# Copyright:    (c) 2005 I-Team.
#----------------------------------------------------------------------------

import wx
import wx.lib.intctrl
import wx.lib.docview
import wx.lib.dialogs
import wx.gizmos
import wx._core
import wx.lib.pydocview
from wx import ImageFromStream, BitmapFromImage
from wx import EmptyIcon
import cStringIO, zlib
import Service
import STCTextEditor
import CodeEditor
import DebuggerService
import OutlineService
import wx.lib.scrolledpanel as scrolled
import sys
import new
import time
import os
import threading
import ProjectEditor
import types
import re
from xml.dom.minidom import parse, parseString
if wx.Platform == '__WXMSW__':
    try:
        import win32api
        _PYWIN32_INSTALLED = True
    except ImportError:
        _PYWIN32_INSTALLED = False
    _WINDOWS = True
else:
    _WINDOWS = False

_ = wx.GetTranslation

_VERBOSE = False
_WATCHES_ON = False


class Builder:
    """Base class for language builders."""

    def __init__(self):
        pass

    def setup(self, wxComponent, callbackOnExit=None):
        self._wxComponent = wxComponent
        self._callbackOnExit = callbackOnExit
        self._executor = None
    
    def CanHandleFile(self, file):
        """Overide this function to return if given type of file can be handled by our Builder."""
        return False

    def Clean(self, filename):
        """Overide this."""
        pass
        
    def Compile(self, filename):
        """Overide this."""
        pass
    
    def Link(self, project):
        """Overide this."""
        pass
    
    def Make(self, project, makeall = False):
        """Overide this."""
        pass
    
    def Execute(self, cmd, args, cwd = None, env = None):
        """Call this function to start the compilation process."""
        if self._executor != None:
            raise RuntimeError, 'Another process is already running.'
        self._executor = DebuggerService.Executor(cmd, self._wxComponent, self._OnProcessExit)
        self._executor.Execute(args, startIn = cwd, environment = env)
    
    def StopExecution(self):
        if self._executor != None:
            self._executor.StopExecution()
            self._executor = None
    
    def _OnProcessExit(self):
        self._executor = None
        if self._callbackOnExit != None:
            self._callbackOnExit()
        
        
class BuildCommandUI(wx.Panel):
    """Currently not used."""
    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        self._noteBook = parent
        
        self._re = re.compile('^(?P<file>.*) \(line (?P<line>[0-9]+), column (?P<col>[0-9]+)\) (?P<type>\w+) (?P<ref>\w+):')
        self._events = []
        self._curEvent = -1

        # GUI Initialization follows
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self._tb = tb = wx.ToolBar(self,  -1, wx.DefaultPosition, (30,1000), wx.TB_VERTICAL| wx.TB_FLAT, "Builder" )
        tb.SetToolBitmapSize((16,16))
        sizer.Add(tb, 0, wx.EXPAND |wx.ALIGN_LEFT|wx.ALL, 1)

        tb.AddSimpleTool( BuildService.PREV_ERROR_ID, getUpBitmap(), _('Previous error'))
        wx.EVT_TOOL(self, BuildService.PREV_ERROR_ID, self.OnToolClicked)
        
        tb.AddSimpleTool(BuildService.NEXT_ERROR_ID, getDownBitmap(), _("Next error"))
        wx.EVT_TOOL(self, BuildService.NEXT_ERROR_ID, self.OnToolClicked)
        
        tb.Realize()
        self._textCtrl = STCTextEditor.TextCtrl(self, wx.NewId())
        sizer.Add(self._textCtrl, 1, wx.ALIGN_LEFT|wx.ALL|wx.EXPAND, 1)
        self._textCtrl.SetMarginWidth(1, 0)
        self._textCtrl.SetViewLineNumbers(False)
        self._textCtrl.SetReadOnly(True)
        if wx.Platform == '__WXMSW__':
            font = "Courier New"
        else:
            font = "Courier"
        self._textCtrl.SetFont(wx.Font(9, wx.DEFAULT, wx.NORMAL, wx.NORMAL, faceName = font))
        self._textCtrl.SetFontColor(wx.BLACK)
        self._textCtrl.StyleClearAll()
        self._textCtrl.StyleSetSpec(1, 'bold')
        self._textCtrl.StyleSetSpec(2, 'fore:#FF0000')   # STDERR
        self._textCtrl.StyleSetSpec(3, 'fore:#0000FF,back:#FFFF00,bold')   # Lines
        self._textCtrl.UpdateStyles()
     
        self.Bind(DebuggerService.EVT_UPDATE_STDTEXT, self.AppendText)
        self.Bind(DebuggerService.EVT_UPDATE_ERRTEXT, self.AppendErrorText)
        wx.stc.EVT_STC_DOUBLECLICK(self._textCtrl, self._textCtrl.GetId(), self.OnDoubleClick)

        self.SetSizer(sizer)
        sizer.Fit(self)
                
    #def BuildStarted(self):
        #pass
    
    #def BuildFinished(self):
        #pass
        ##self._tb.EnableTool(self.KILL_PROCESS_ID, False)
        ##nb = self.GetParent()
        ##for i in range(0,nb.GetPageCount()):
            ##if self == nb.GetPage(i):
                ##text = nb.GetPageText(i)
                ##newText = text.replace("Running", "Finished")
                ##nb.SetPageText(i, newText)
                ##break
                   
    def AppendText(self, event):
        textCtrl = self._textCtrl
        assert(isinstance(textCtrl, STCTextEditor.TextCtrl))
        pos = textCtrl.GetCurrentPos()
        textCtrl.SetReadOnly(False)
        textCtrl.AddText(event.value)
        if (hasattr(event, 'bold') and event.bold == True) or event.value.strip()[:3] == '***':
            textCtrl.StartStyling(pos, 0x1F)
            textCtrl.SetStyling(len(event.value), 1)
        elif self._re.match(event.value):
            textCtrl.StartStyling(pos, 0x1F)
            textCtrl.SetStyling(len(event.value), 2)
        textCtrl.ScrollToLine(textCtrl.GetLineCount())
        textCtrl.SetReadOnly(True)
        
    def AppendErrorText(self, event):
        textCtrl = self._textCtrl
        pos = textCtrl.GetCurrentPos()
        textCtrl.SetReadOnly(False)
        textCtrl.AddText(event.value)
        textCtrl.StartStyling(pos, 0x1F)
        match = self._re.search(event.value)
        if match:
            textCtrl.SetStyling(len(event.value), 3)
            self.AddBuildEvent(pos, pos + len(match.group(0)) , match)  #self.GetCurrentPos()-2
        else:
            textCtrl.SetStyling(len(event.value), 2)
        textCtrl.ScrollToLine(textCtrl.GetLineCount())
        textCtrl.SetReadOnly(True)
        
    #------------------------------------------------------------------------------
    # Event handling
    #-----------------------------------------------------------------------------
    
    def OnToolClicked(self, event):
        id = event.GetId()
        if id == BuildService.PREV_ERROR_ID:
            self.JumpToPrevEvent()
        elif id == BuildService.NEXT_ERROR_ID:
            self.JumpToNextEvent()
    
    #----------------------------------------------------------------------------
    # Service specific methods
    #----------------------------------------------------------------------------

    def ClearLines(self):
        self._textCtrl.SetReadOnly(False)
        self._textCtrl.ClearAll()
        self._textCtrl.SetReadOnly(True)


    def AddLines(self, text):
        self._textCtrl.SetReadOnly(False)
        self._textCtrl.AddText(text)
        self._textCtrl.SetReadOnly(True)


    def GetText(self):
        return self._textCtrl.GetText()


    def GetCurrentPos(self):
        return self._textCtrl.GetCurrentPos()


    def GetCurrLine(self):
        return self._textCtrl.GetCurLine()


    def GetBuildEventCount(self):
        return len(self._events)

    def AddBuildEvent(self, start, end, match):
        self._events.append({'start': start,
                             'end': end, 
                             'file': match.group('file'), 
                             'line': int(match.group('line')), 
                             'col': int(match.group('col')), 
                             'type': match.group('type'), 
                             'ref': match.group('ref')})
    
    def ClearBuildEvents(self):
        self.ClearLines()
        self._events = []
        self._curEvent = -1

    def OnDoubleClick(self, event):
        lineText, pos = self.GetCurrLine()
        match = self._re.search(lineText)
        if not match:
            return
        buildEvent = {}
        line = self._textCtrl.LineFromPosition(self.GetCurrentPos())
        buildEvent['start'] = self._textCtrl.PositionFromLine(line)
        buildEvent['end'] = self._textCtrl.GetLineEndPosition(line)
        buildEvent['file'] = match.group('file')
        buildEvent['line'] = int(match.group('line'))
        buildEvent['col'] = int(match.group('col'))
        buildEvent['type'] = match.group('type')
        buildEvent['ref'] = match.group('ref')
        self.JumpToBuildEvent(buildEvent)
        
    def JumpToBuildEvent(self, buildEvent):
        self._textCtrl.HideSelection(0)
        self._textCtrl.SetSelection(buildEvent['start'], buildEvent['end'])
        self._textCtrl.EnsureCaretVisible()
        filename = buildEvent['file']
        lineNum = buildEvent['line']
        colNum = buildEvent['col']

        foundView = None
        openDocs = wx.GetApp().GetDocumentManager().GetDocuments()
        for openDoc in openDocs:
            if os.path.normpath(openDoc.GetFilename()) == os.path.normpath(filename):
                foundView = openDoc.GetFirstView()
                break

        if not foundView and os.path.exists(filename):
            doc = wx.GetApp().GetDocumentManager().CreateDocument(filename, wx.lib.docview.DOC_SILENT|wx.lib.docview.DOC_OPEN_ONCE)
            if doc:
                foundView = doc.GetFirstView()

        if foundView:
            foundView.GetFrame().SetFocus()
            foundView.Activate()
            if hasattr(foundView, "GotoLine"):
                foundView.GotoLine(lineNum)
                startPos = foundView.PositionFromLine(lineNum)
                endPos = foundView.GetLineEndPosition(lineNum)
                # wxBug:  Need to select in reverse order, (end, start) to put cursor at head of line so positioning is correct
                #         Also, if we use the correct positioning order (start, end), somehow, when we open a edit window for the first
                #         time, we don't see the selection, it is scrolled off screen
                foundView.SetSelection(endPos, startPos)
                wx.GetApp().GetService(OutlineService.OutlineService).LoadOutline(foundView, position=startPos)

    def JumpToNextEvent(self):
        self._curEvent += 1
        if self._curEvent >= len(self._events):
            self._curEvent = -1
            self._textCtrl.HideSelection(1)
        else:
            self.JumpToBuildEvent(self._events[self._curEvent])


    def JumpToPrevEvent(self):
        self._curEvent -= 1
        if self._curEvent < 0:
            self._curEvent = len(self._events)
            self._textCtrl.HideSelection(1)
        else:
            self.JumpToBuildEvent(self._events[self._curEvent])


class BuildView(Service.ServiceView):
    
    #----------------------------------------------------------------------------
    # Overridden methods
    #----------------------------------------------------------------------------

    def __init__(self, service):
        Service.ServiceView.__init__(self, service)
        
    def _CreateControl(self, parent, id):
        #txtCtrl = STCTextEditor.TextCtrl(parent, id)
        #txtCtrl.SetMarginWidth(1, 0)  # hide line numbers
        ##txtCtrl.SetYCaretPolicy(wx.stc.STC_CARET_SLOP | wx.stc.STC_CARET_EVEN, 2)
        #txtCtrl.SetReadOnly(True)

        #if wx.Platform == '__WXMSW__':
            #font = "Courier New"
        #else:
            #font = "Courier"
        #txtCtrl.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL, faceName = font))
        #txtCtrl.SetFontColor(wx.BLACK)
        #txtCtrl.StyleClearAll()
        #txtCtrl.StyleSetSpec(1, 'bold')
        #txtCtrl.StyleSetSpec(2, 'fore:#FF0000')   # STDERR
        #txtCtrl.StyleSetSpec(3, 'fore:#0000FF,back:#FFFF00,bold')   # Lines
        #txtCtrl.UpdateStyles()

        self.Bind(DebuggerService.EVT_UPDATE_STDTEXT, self.AppendText)
        self.Bind(DebuggerService.EVT_UPDATE_ERRTEXT, self.AppendErrorText)
        #wx.stc.EVT_STC_DOUBLECLICK(txtCtrl, txtCtrl.GetId(), self.OnDoubleClick)
        #return txtCtrl
        ctrl = BuildCommandUI(parent, id)
        return ctrl

    def GetIcon(self):
        return getBuildIcon()

    def Activate(self):
        ctrl = self.GetControl()
        notebook = ctrl.GetParent()
        if isinstance(notebook, wx.Notebook):
            nb = notebook.GetPageCount()
            for index in range(nb):
                if notebook.GetPage(index) == ctrl:
                    notebook.SetSelection(index)
                    return
        
    #------------------------------------------------------------------------------
    # Event handling
    #-----------------------------------------------------------------------------
    
    def AppendText(self, event):
        self.GetControl().AppendText(event)

    def AppendErrorText(self, event):
        self.GetControl().AppendErrorText(event)

    def OnToolClicked(self, event):
        self.GetFrame().ProcessEvent(event)
    
    #----------------------------------------------------------------------------
    # Service specific methods
    #----------------------------------------------------------------------------

    #def BuildStarted(self):
        #self.GetControl().BuildStarted()
    
    #def BuildFinished(self):
        #self.GetControl().BuildFinished()

    def ClearBuildEvents(self):
        self.GetControl().ClearBuildEvents()
        
    def GetBuildEventCount(self):
        return self.GetControl().GetBuildEventCount()
    
    def JumpToNextEvent(self):
        self.GetControl().JumpToNextEvent()
        
    def JumpToPrevEvent(self):
        self.GetControl().JumpToPrevEvent()
        

        
class BuildService(Service.Service):
    availableBuilders = []

    #----------------------------------------------------------------------------
    # Constants
    #----------------------------------------------------------------------------
    SHOW_WINDOW = wx.NewId()  # keep this line for each subclass, need unique ID for each Service
    MAKE_ID = wx.NewId()
    MAKE_ALL_ID = wx.NewId()
    MAKE_MULTIPLE_ID = wx.NewId()
    CLEAN_ID = wx.NewId()
    COMPILE_ID = wx.NewId()
    LINK_ID = wx.NewId()
    NEXT_ERROR_ID = wx.NewId()
    PREV_ERROR_ID = wx.NewId()
    
            
    def ComparePaths(first, second):
        one = BuildService.ExpandPath(first)
        two = BuildService.ExpandPath(second)
        if _WINDOWS:
            return one.lower() == two.lower()
        else:
            return one == two
    ComparePaths = staticmethod(ComparePaths)
            
    # Make sure we're using an expanded path on windows.
    def ExpandPath(path):
        if _WINDOWS:
            try:
                return win32api.GetLongPathName(path)
            except:
                print "Cannot get long path for %s" % path
                
        return path
    ExpandPath = staticmethod(ExpandPath)
 
    #----------------------------------------------------------------------------
    # Overridden methods
    #----------------------------------------------------------------------------

    def __init__(self, serviceName, embeddedWindowLocation = wx.lib.pydocview.EMBEDDED_WINDOW_LEFT):
        Service.Service.__init__(self, serviceName, embeddedWindowLocation)
        self._curDocFilename = ''
    
    def _CreateView(self):
        return BuildView(self)


    #----------------------------------------------------------------------------
    # Service specific methods
    #----------------------------------------------------------------------------

    def InstallControls(self, frame, menuBar = None, toolBar = None, statusBar = None, document = None):
        Service.Service.InstallControls(self, frame, menuBar, toolBar, statusBar, document)

        config = wx.ConfigBase_Get()

        buildMenu = wx.Menu()
        if not menuBar.FindItemById(BuildService.MAKE_ID):
                    
            item = wx.MenuItem(buildMenu, BuildService.COMPILE_ID, _("C&ompile current file\tCtrl+F7"), _("Compile current file"))
            item.SetBitmap(getCompileBitmap())
            buildMenu.AppendItem(item)
            wx.EVT_MENU(frame, BuildService.COMPILE_ID, self.ProcessEvent)
            wx.EVT_UPDATE_UI(frame, BuildService.COMPILE_ID, self.ProcessUpdateUIEvent)

            item = wx.MenuItem(buildMenu, BuildService.MAKE_ID, _("&Make\tF7"), _("Build current project"))
            item.SetBitmap(getMakeBitmap())
            buildMenu.AppendItem(item)
            wx.EVT_MENU(frame, BuildService.MAKE_ID, frame.ProcessEvent)
            wx.EVT_UPDATE_UI(frame, BuildService.MAKE_ID, frame.ProcessUpdateUIEvent)

            #buildMenu.Append(BuildService.MAKE_ALL_ID, _("Make &All"), _("Clean and Build current project"))
            #wx.EVT_MENU(frame, BuildService.MAKE_ALL_ID, frame.ProcessEvent)
            #wx.EVT_UPDATE_UI(frame, BuildService.MAKE_ALL_ID, frame.ProcessUpdateUIEvent)
            
            #buildMenu.AppendSeparator()

            #buildMenu.Append(BuildService.CLEAN_ID, _("&Clean"), _("Clean current project"))
            #wx.EVT_MENU(frame, BuildService.CLEAN_ID, frame.ProcessEvent)
            #wx.EVT_UPDATE_UI(frame, BuildService.CLEAN_ID, frame.ProcessUpdateUIEvent)
            
            #buildMenu.Append(BuildService.LINK_ID, _("&Link\tAlt+F7"), _("Link current project"))
            #wx.EVT_MENU(frame, BuildService.LINK_ID, self.ProcessEvent)
            #wx.EVT_UPDATE_UI(frame, BuildService.LINK_ID, self.ProcessUpdateUIEvent)
            
##            buildMenu.AppendSeparator()
##
##            buildMenu.Append(BuildService.BREAK_ID, _("&Break"), _("Break current build"))
##            wx.EVT_MENU(frame, BuildService.BREAK_ID, frame.ProcessEvent)
##            wx.EVT_UPDATE_UI(frame, BuildService.BREAK_ID, frame.ProcessUpdateUIEvent)

            buildMenu.AppendSeparator()

            item = wx.MenuItem(buildMenu, BuildService.PREV_ERROR_ID, _("Goto &Previous Error\tShift+F4"), _("Goto &Previous Error"))
            item.SetBitmap(getUpBitmap())
            buildMenu.AppendItem(item)
            wx.EVT_MENU(frame, BuildService.PREV_ERROR_ID, frame.ProcessEvent)
            wx.EVT_UPDATE_UI(frame, BuildService.PREV_ERROR_ID, frame.ProcessUpdateUIEvent)

            item = wx.MenuItem(buildMenu, BuildService.NEXT_ERROR_ID, _("Goto &Next Error\tF4"), _("Goto &Next Error"))
            item.SetBitmap(getDownBitmap())
            buildMenu.AppendItem(item)
            wx.EVT_MENU(frame, BuildService.NEXT_ERROR_ID, frame.ProcessEvent)
            wx.EVT_UPDATE_UI(frame, BuildService.NEXT_ERROR_ID, frame.ProcessUpdateUIEvent)

            #BuildService.availableBuilders.append(AnubisBuilder)
        
        menuIndex = menuBar.FindMenu(_("&Run"))
        if menuIndex == wx.NOT_FOUND:
            menuIndex = menuBar.FindMenu(_("&Project")) + 1
        menuBar.Insert(menuIndex, buildMenu, _("&Build"))

        toolBar.AddSeparator()
        toolBar.AddTool(BuildService.COMPILE_ID, getCompileBitmap(), shortHelpString = _("Compile"), longHelpString = _("Compile current file"))
        toolBar.AddTool(BuildService.MAKE_ID, getMakeBitmap(), shortHelpString = _("Make"), longHelpString = _("Build current project"))
        toolBar.Realize()


        return True


            
    #----------------------------------------------------------------------------
    # Event Processing Methods
    #----------------------------------------------------------------------------

    def ProcessEventBeforeWindows(self, event):
        return False


    def ProcessEvent(self, event):
        if Service.Service.ProcessEvent(self, event):
            return True

        an_id = event.GetId()
        if an_id == BuildService.MAKE_ID:
            self.OnMake(event)
            return True
        elif an_id == BuildService.MAKE_ALL_ID:
            self.OnMakeAll(event)
            return True
        elif an_id == BuildService.CLEAN_ID:
            self.OnClean(event)
            return True
        elif an_id == BuildService.COMPILE_ID:
            self.OnCompile(event)
            return True
        elif an_id == BuildService.LINK_ID:
            self.OnLink(event)
            return True
        elif an_id == BuildService.NEXT_ERROR_ID:
            self.OnNextError(event)
            return True
        elif an_id == BuildService.PREV_ERROR_ID:
            self.OnPreviousError(event)
            return True
        return False
        
    def ProcessUpdateUIEvent(self, event):
        if Service.Service.ProcessUpdateUIEvent(self, event):
            return True

        enable = False
        curDoc = self.GetDocumentManager().GetCurrentDocument()
        if curDoc:
            builder = self.GetBuilderForFile(curDoc.GetFilename())
            enable = True
        an_id = event.GetId()
        #currentView = self.GetDocumentManager().GetCurrentView()
        #assert(isinstance(currentView, wx.lib.docview.View))
        if an_id == BuildService.MAKE_ID:
            event.Enable(enable)
            return True
        elif an_id == BuildService.MAKE_ALL_ID:
            event.Enable(enable)
            return True
        elif an_id == BuildService.CLEAN_ID:
            event.Enable(enable)
            return True
        elif an_id == BuildService.COMPILE_ID:
            event.Enable(enable)
            return True
        elif an_id == BuildService.LINK_ID:
            event.Enable(enable)
            return True
        elif an_id == BuildService.NEXT_ERROR_ID:
            event.Enable(enable and self.GetView() != None and self.GetView().GetBuildEventCount() > 0)
            return True
        elif an_id == BuildService.PREV_ERROR_ID:
            event.Enable(enable and self.GetView() != None and self.GetView().GetBuildEventCount() > 0)
            return True
        else:
            return False

    #----------------------------------------------------------------------------
    # Class Methods
    #----------------------------------------------------------------------------                    

    def OnMake(self, event):
        self.PromptToSaveFiles('making')
        projectService = wx.GetApp().GetService(ProjectEditor.ProjectService)
        if projectService:
            curDoc = projectService.GetSelectedProject()
            builder = self.GetBuilderForFile(curDoc.GetStartupFile())
            if builder:
                self.GetView().Activate()
                self.GetView().ClearBuildEvents()
                builder.setup(self.GetView(), self.EndOfBuildCallback)
                builder.Compile(curDoc.GetStartupFile())

    def OnCompile(self, event):
        self.PromptToSaveFiles('compiling')
        curDoc = self.GetDocumentManager().GetCurrentDocument()
        if curDoc:
            builder = self.GetBuilderForFile(curDoc.GetFilename())
            if builder:
                self.GetView().Activate()
                self.GetView().ClearBuildEvents()
                builder.setup(self.GetView(), self.EndOfBuildCallback)
                builder.Compile(curDoc.GetFilename())
   
    def OnClean(self, event):
        self.PromptToSaveFiles('cleaning')
        curDoc = self.GetDocumentManager().GetCurrentDocument()
        if curDoc:
            builder = self.GetBuilderForFile(curDoc.GetFilename())
            if builder:
                self.GetView().Activate()
                self.GetView().ClearBuildEvents()
                builder.setup(self.GetView(), self.EndOfBuildCallback)
                builder.Clean(curDoc.GetFilename())
   
    def OnLink(self, event):
        self.PromptToSaveFiles('linking')
        wx.LogError(_('Not yet implemented'))
        #projectService = wx.GetApp().GetService(ProjectEditor.ProjectService)
        #if projectService:
            #curDoc = projectService.GetSelectedProject()

    def OnNextError(self, event):
        self.GetView().JumpToNextEvent()
    
    def OnPreviousError(self, event):
        self.GetView().JumpToPrevEvent()
    
    def HasAnyFiles(self):
        docs = wx.GetApp().GetDocumentManager().GetDocuments()
        return len(docs) > 0
        
    def PromptToSaveFiles(self, actionTitle):
        filesModified = False
        docs = wx.GetApp().GetDocumentManager().GetDocuments()
        for doc in docs:
            if doc.IsModified():
                filesModified = True
                break
        if filesModified:
            frame = self.GetView().GetFrame()
            yesNoMsg = wx.MessageDialog(frame,
                      _("Files have been modified.\nWould you like to save all files before %s?" % actionTitle),
                      _("Run"),
                      wx.YES_NO|wx.ICON_QUESTION
                      )
            if yesNoMsg.ShowModal() == wx.ID_YES:
                docs = wx.GetApp().GetDocumentManager().GetDocuments()
                for doc in docs:
                    doc.Save()


    def SaveAllFiles(self):
        docs = wx.GetApp().GetDocumentManager().GetDocuments()
        for doc in docs:
            doc.Save()


    #----------------------------------------------------------------------------
    # Run / Debug executors
    #----------------------------------------------------------------------------                    
    def AddBuilder(self, builder):
        """Add an builder for a specific document type."""
        assert(isinstance(builder, Builder))
        BuildService.availableBuilders.append(builder)

    def RemoveBuilder(self, builder):
        """Remove a previously added builder."""
        BuildService.availableBuilders.remove(builder)

    def EndOfBuildCallback(self):
        evt = DebuggerService.UpdateTextEvent(value = _(u'\n\n*** Build finished! ***'))
        wx.PostEvent(self.GetView(), evt)
        
    def GetBuilderForFile(self, sourceFile):
        for builder in BuildService.availableBuilders:
            if builder.CanHandleFile(sourceFile):
                return builder
        

class BuildOptionsPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        SPACE = 10
        config = wx.ConfigBase_Get()
        # TODO : autosave check box

        parent.AddPage(self, _("Build"))
        
        
    def OnOK(self, optionsDialog):
        config = wx.ConfigBase_Get()

def getBuildData():
    return zlib.decompress(
'x\xda\x01\xeb\x01\x14\xfe\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\
\x10\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\
\x08\x08\x08\x08|\x08d\x88\x00\x00\x01\xa2IDAT8\x8d\xa5\x93\xbdKWa\x14\xc7?\
\xcfs\xc3E\xa8A\xc2?\xc2!\xd0&\x97\x1c\\Kqr\xb1\xe5\'\x8a`\x89\xb6\x884\xb4\
\xb4\xf4F\xb4\xd8\x1b-\xe2 (\xbe\x84\x10\xd9"\xbeBa\x8b\xa3\xb64\x08\x11\xb8\
\xe4\xaf{\x9f\xfb<\xe78\\\xee\xef\xd7\xf5Z\xbf\xc0\x03\x07\x9e\xf3\xc2\xf7|\
\xcf\xcbc\x8c\x8d\xb8\x88\\\xca\x1f\xdb\xef\xefj\xa3\xe4\xce\x9b/L\xc9il\x84\
\xb1\x11;\xab\xe3\x9a8\xffW\xdd_\x19\xd1\x9d\xd5q\xcd\xf3s\xb59\x90\xa8E\x15\
TA\x14T5S2\xbb\xa9\xb9\x85\xcb!fsy\xac\xc0\xb4\xd6B\x10\x8b(\xbcY\xf8L\x92\
\xa4T\x7f\'\xfc:\x89\xa9Vc\x9eL\xf6\xd2\xd4\xdc\x02\xc0\x95\x93\x9f\xac/M\
\xe8\x8d\xdeg\xa6\x00\xe0\xc5\x12D\xa9\xf4]\xaf\xc3\xab\x01\xa3\x88\xc0\xd5k\
w\xc8\xf8\xc0\x8f\x0f\xf7\xcb\x0c\xbcD\xa8\xc2\xbb\xc5/\xf8\x10p\xce\xe3\\J\
\x92x\x12\x97\xe2\\\x8as\x9e\xc7\x93}x\x89\xce\x01\x08\x11A\x94\xdb\xb7:\xfe\
\xb9\x89 \x8a\x97\xda\xe8\xce\x004\\d&i\xa83\xb0u\xa7ED\xd9\xd8\xfaJ[W\x85\
\xd9\xf95D\x94\xd9\xf95\xda\xba*\x1c\x1c|\xaf\xc5\x87\x1e}\xe3\xf9\xdb\x95\
\xac\\\xbe\xcf\xb9\x99\x87zt\x1ckk\xfb\x80\xee\xee\x1djk\xfb\x80\x1e\x1d\xc7\
\xb5\xf7\xee\xdea)^\xb8\x03\x1f"D\x8a\x87\xd6\xc8>\xd3BDP)\x0e\xac\x81]\x00\
\x08b\x111L\x8d\xf6\xd33\xf8\x80\xa9\xd1\xfes\x19\xfc\x19\x070\xf9o|5\xfdT\
\xbb{\x86K\x15\xcab\xf8\xb4\xfc\x92\xe1\x91{\xc5K\x145|\\z\xfd\x1f\x00\x19H.\
\xa7\x95\x99\x03\x1f|L?\x1d\x00\x00\x00\x00IEND\xaeB`\x82p\xaa\xe6m' )

def getBuildBitmap():
    return BitmapFromImage(getBuildImage())

def getBuildImage():
    stream = cStringIO.StringIO(getBuildData())
    return ImageFromStream(stream)

def getBuildIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getBuildBitmap())
    return icon


def getCompileData():
    return zlib.decompress(
'x\xda\x01\x96\x01i\xfe\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x01MIDAT8\x8d\x9d\x92=/DA\x18\x85\x9fw.\xeb\
\xa3\x10\xf1\x91\xac\x8eB\xd4\xa2Pn\xe1\x17\x88^\xa5\x97H$"\xc1\xa2\xa0\xd8\
\xf8/\x12\xca-4\x12\x95B$\xfc\x00"\xd6j|\x84\xdd\xb9s\x14\xbb\xec\xdd\xeb\
\xae\xe0$o&3g\xdeg\xceL\xc6\xccElmn\x88\x1f\xb4\xbd\xb3k\x1dMs\x11\xc5bQY\
\xda?8\xd2\xf2qEk\xa5S\x99\x8b\xc8*\x97\x84]\xde\xc5m\x050v}\xc6\xa0{d\xaft\
\x98\x99\xb2+9\xb9\xa8\xb4\']\x98\x9ferb$\xb1\xe2\x13\x90\x13\xcc\xcd\xd9\
\x17\xc0\x07\x98\xce\x83\x8f\x85$\x02\xe2-\x1a\xe2\xf69\xf0\xf4Paj\xfc\xaa\
\xb9\xb3\x90\x9d\xa0\x16\xc4\xf9\x1d\x805\xab\xa5\x99\xe1^V\xd7o(\xed/\x9a\
\x82W&\xe0\xa5\x16\x18\xc8\x81h\x14\x12\x02r\x91x~\xafS\xbd\xaf\xd2\xde\\@\
\xa1\xacV\x02\x1f\x93\xef\xf7\xc4\x01$\xe1%\x82D\x7f7\xd4\xdf\xc1\xd7=\xe6\
\xba\x12\tRo\xd0\xd3\x97c\x14\x88\x05q\x08\x8d$\x02s\xc6k\xd4:\xb7\x01)\xcb\
\xdc\x9c\xb5]ae\xe9\x80\xdf\xea\xb3\x19\xc0\xcc5\xf0\n\xb1\xccE_Fr\xfe\x93G\
\x13\xa0\xf4\x98\xb5\x96\xf6\xbe\xfd\xc4V\xc4\xa8\xe3\xdfO{\x99\x80\xbf\xc8}\
R\xd3\xf7L\x9e\xd8\xc9\x83\xc4#\xfeW\x1fz\xcc\xb6\xf9\x00\xdc\xb9t\x00\x00\
\x00\x00IEND\xaeB`\x82\x1b\xf3\xb8\xd1' )

def getCompileBitmap():
    return BitmapFromImage(getCompileImage())

def getCompileImage():
    stream = cStringIO.StringIO(getCompileData())
    return ImageFromStream(stream)

def getMakeData():
    return zlib.decompress(
'x\xda\x01\xe5\x01\x1a\xfe\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\
\x10\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\
\x08\x08\x08\x08|\x08d\x88\x00\x00\x01\x9cIDAT8\x8d\x9d\x93!l\xdb@\x14\x86\
\xbf\xb3;\xed\x05\xed\x0eT\x8a\xa1a\xc2\xda\xb2\xc0\xc1\x8d-l\x83\x83\x8d*U+\
\x1e\x98J\xa7H\xd5\x02\r7\x96\xb1\x14\x1a\x8e\xa5\x81\x86\x86\x9e\x14\x90\
\x1b\xf2ESz\x03v\x9c\xc4\x9d\xd4n?z\xfa\xef\xee{\xef\xdd\xbdS*\x08\x01\x98\
\xdf\x1c{Z:\xbbX\xaa\xb6\xd7\x96RA\xc8\xfc\xe6\xd8\xf7\x86#\xa4\xab\x01\x078\
l\x96\x93\xa7\xb3GAG\xdb@\xba\x02\xaeh\x16t\x1csr>\x02\xb7b\xce\xd7\xc3\xea\
\xc4\x80[qv\xb1TG;\xd7\x81t\xea\xb0\x04J\x16\x93\x04\x80x0D\xa2\x1e\xae\xb4H\
G#\xda\x90\xdf~\x01\x96\xec\x01\x04\xf73#\x9b\xce\x1a\':}I<\xfd\x06E\x82\x98\
\x9a\xbd\x82\xf2\xea\x1c\xe7\xf2\xc3\x16p+\xb2\xe9\x8cx\xf0\x0e\xb4Tm\x98\
\x88\xf2\xba\xdf\xb4f\x92\x04\xdd\x073\x99\xf0\xeb\xe3o\x05\x10l\xcf\xdb\xbc\
"\xea\x93\x18\x1d9\xb4\x01\xc8\xab\x94\x08 d\x83W\xb8\x02$\xde\xd5\xdd\x00\
\xf24\xa57\x1c\xd5\xd9\xa4\xba\x93\xbf\xc9\x81\x11\xcd\x8bO\xcf\xfc\x01 :}\
\xbd\xf7\x8cm\tH\xd5\x96\x11!\x8e"LW\x0e\xef@\xba\xf1n3\x9d\xfaEJ\xb0\x16WZ\
\x8a\xec\x8e~\xba\xe0\xc3\xfb7X,\xf4\xa1\xb8\\\xf8\x06\xa0\xa3\x1e\xb8;\x9c\
\xb3\x14?n\x9b\xdc6\xdf\xce\x86c=\x0e\xd5g\xbe\xfb\xeb\xab\xb7$I\xcaz\x1c\
\xaaf\x12\xf7\x0b~l\x84\x9f_n\xfcz\x1cV{T\x10R\xff\x07\xff?q\xd0\xa6\xfb\xfb\
\x8dWA\xa8\xfc\xfd\xc6?\xc5\x7f\x00\xf8W=\x00l\xb3\xa8 TO\xf1\xff\x00\xac\
\x8a\xa5\x12\xb1O\xbc\xb2\x00\x00\x00\x00IEND\xaeB`\x82I\x8b\xda\xa9' )

def getMakeBitmap():
    return BitmapFromImage(getMakeImage())

def getMakeImage():
    stream = cStringIO.StringIO(getMakeData())
    return ImageFromStream(stream)


def getDownData():
    return zlib.decompress(
'x\xda\x01N\x01\xb1\xfe\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x02\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\
\x08\xdb\xe1O\xe0\x00\x00\x01\x06IDAT(\x91\x95RAJ\x03A\x10\xac\x16?`p\xf5\t\
\xfe#/Po~BQ\xbc\x85\xa0\x08\x01A\xd6\\\xbc\xf8\x03c\xf4\xaa.B\xee\xe6\x07\
\xebU\x90xX\x84\rdu\x99\x9e\xf20\xa2\xce\xee\xac$}\xea\xe9\x9e\xea\xae\xa2ZH\
b\x91X\xae\x97\x1e\x8e"\xab\xf8,-\x15\xdbqVm\xd3\x8f\xe48\xb2\xb3T\xf3\xb1f\
\x89\x99\x0c\x86\xfb+\x95\x0fK\x0b\xf1\x01\x10\x02\x08\x80Fa\x01\r\xa4\x10\
\xe2\xd2\xf96\x80\xf2\x9b\xce\x01\x10\x91?\x8f\x06J\xa3\xde:\xed\xf78\x82n\t\
\x01\x10\xd7{-kIb\xe7\xe2\xdd\xd3\xd0\xee\xa4\x00\xa0\x05tJ\x08\x01@\xb6\x0e\
Oa\xa6\xd4bx\x1e{\x94\xda\x9d\xb7Qo\xc3\xd1\xe7\x8f\x0c!\x01\x027\xfd\xd8\
\x8d\x07|\xe3\x1eO\xd6l\xf9\xaa\xb3g\xcd\x9fL\x96\x98\xc9\x95y\xb9\x1c\xecz\
\xde\x05\x9c\xd6"\xd5|l\x1a\x9c\xae\x02H\xdewW\xddi\xdc\x1e\xb4\xea]\t^\xeb]\
7*?\xec\xe6Y\xed\xf2\x800\xe0\x9f\xf8\x02\xf3*\xf9\xca\xcd\xd0\xaf\xe5\x00\
\x00\x00\x00IEND\xaeB`\x82\xf2\x8c\x8fB' )

def getDownBitmap():
    return BitmapFromImage(getDownImage())

def getDownImage():
    stream = cStringIO.StringIO(getDownData())
    return ImageFromStream(stream)

def getUpData():
    return zlib.decompress(
'x\xda\x01Z\x01\xa5\xfe\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x02\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\
\x08\xdb\xe1O\xe0\x00\x00\x01\x12IDAT(\x91\x95\x92MJ\x03A\x10\x85\xdf\x93\\ \
\x9aI\x8e\xe0=\xa2\'\x08\xe2)\x8c\x90\x88(\x0c\xc1?\xb2\x8b\x8b,=\x81\t"\xba\
RG1+7\xe6\x06\xe3V\x90\xb8\x18\x17\xb30\x18\xba\xbb\\\xcc\xe8\x90\xe9fB\xde\
\xaa\xa9\xaa\xaf\x8b\xaaW\x14\x11,\xa3\x15g\xf4\xae\xe3\xdd\xee\xaf\xb9\t\
\xb1t\xdf\xa9\xe8x\xac\xa3\xe0\xba\xbdjg\xf3@p\xec\xe9i\xa8\xe3\xb1\x8a\x025\
\x19^\xb5\xcaE\xc0\xd3i\xd5\xcc>\xf4\xf7\x9b\x8e_U\x14\xa8\xc9@\xbd_\x0cw\
\xe7\x98l\x86\xe7\xb3\xda\x86\x1f\n\x01\n\x00\x02 \x84h\xb4\xfdA\xb3\x9c\x1f\
z\xd4\xad\xd5\xfd\x10\x00\x05\x04RHH\x80\xc0Vk\xefr\'eJ\xff\xe8\xa8\xbb.\x80\
1\xb2y\xf0\x02\x08\x01\x81\xdc\xf4\x0e\x8d\x811\xd9\xeaS\xa0\xee\x7f&\x8f\
\xc7\x93*AI:\x00 \xb6\xfb_\x0b|\x98\xb3\xd2r\xd5i\x1c\xb32\xe6s\xa5|\x00 %\
\xf9Y\xecrw\x87\xbf\xad:\xe5\xbe\xa5\x02\xd1\xbe\xd6\x87#\xcfh\xfc\xcc\x8ch4\
\xce\xa3\xc5@\xb1~\x01$\xeb\xc7\xbe\x87\x80\xf9\x9a\x00\x00\x00\x00IEND\xaeB\
`\x82s\x7f\x97\xb7' )

def getUpBitmap():
    return BitmapFromImage(getUpImage())

def getUpImage():
    stream = cStringIO.StringIO(getUpData())
    return ImageFromStream(stream)

