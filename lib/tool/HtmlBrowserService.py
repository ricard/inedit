#----------------------------------------------------------------------------
# Name:         HtmlBrowserService.py
# Purpose:      Service providing internal html browser
#
# Author:       Cedric RICARD
#
# Created:      20/02/2006
# Copyright:    (c) 2006 SoftArchi
#----------------------------------------------------------------------------

import wx
import wx.lib.docview
import wx.lib.pydocview
import wx.html
import Service
import lib.util.sysutils as sysutils
import os
import stat
if wx.Platform == '__WXMSW__':
    import  wx.lib.iewin    as  iewin

_ = wx.GetTranslation

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
HTML_BROWSER_ID = wx.NewId()

HTML_HOME_ID = wx.NewId()

class HtmlBrowserView(wx.lib.docview.View):


    def OnCreate(self, doc, flags):
        self.current = 'about:blank'
        
        frame = wx.GetApp().CreateDocumentFrame(self, doc, flags)
        sizer = wx.BoxSizer(wx.VERTICAL)        
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)

        btn = wx.Button(frame, -1, "Open", style=wx.BU_EXACTFIT)
        frame.Bind(wx.EVT_BUTTON, self.OnOpenButton, btn)
        btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

        btn = wx.Button(frame, HTML_HOME_ID, "Home", style=wx.BU_EXACTFIT)
        frame.Bind(wx.EVT_BUTTON, self.OnHomeButton, id = HTML_HOME_ID)
        btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

        btn = wx.Button(frame, -1, "<--", style=wx.BU_EXACTFIT)
        frame.Bind(wx.EVT_BUTTON, self.OnPrevPageButton, btn)
        btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

        btn = wx.Button(frame, -1, "-->", style=wx.BU_EXACTFIT)
        frame.Bind(wx.EVT_BUTTON, self.OnNextPageButton, btn)
        btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

        btn = wx.Button(frame, -1, "Stop", style=wx.BU_EXACTFIT)
        frame.Bind(wx.EVT_BUTTON, self.OnStopButton, btn)
        btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

        btn = wx.Button(frame, -1, "Search", style=wx.BU_EXACTFIT)
        frame.Bind(wx.EVT_BUTTON, self.OnSearchPageButton, btn)
        btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

        btn = wx.Button(frame, -1, "Refresh", style=wx.BU_EXACTFIT)
        frame.Bind(wx.EVT_BUTTON, self.OnRefreshPageButton, btn)
        btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

        txt = wx.StaticText(frame, -1, "Location:")
        btnSizer.Add(txt, 0, wx.CENTER|wx.ALL, 2)

        self.location = wx.ComboBox(
                            frame, -1, "", style=wx.CB_DROPDOWN|wx.PROCESS_ENTER
                            )
        
        frame.Bind(wx.EVT_COMBOBOX, self.OnLocationSelect, self.location)
        self.location.Bind(wx.EVT_KEY_UP, self.OnLocationKey)
        self.location.Bind(wx.EVT_CHAR, self.IgnoreReturn)
        btnSizer.Add(self.location, 1, wx.EXPAND|wx.ALL, 2)

        sizer.Add(btnSizer, 0, wx.EXPAND)

        if wx.Platform == '__WXMSW__':
            self._htmlView = iewin.IEHtmlWindow(frame, -1, style = wx.NO_FULL_REPAINT_ON_RESIZE)
            frame.Bind(iewin.EVT_DocumentComplete, self.OnDocumentComplete, self._htmlView)
            ##self.Bind(iewin.EVT_ProgressChange,  self.OnProgressChange, self._htmlView)
            frame.Bind(iewin.EVT_StatusTextChange, self.OnStatusTextChange, self._htmlView)
            frame.Bind(iewin.EVT_TitleChange, self.OnTitleChange, self._htmlView)
        else:
            self._htmlView = wx.html.HtmlWindow(frame, -1, style=wx.NO_FULL_REPAINT_ON_RESIZE)
            

        sizer.Add(self._htmlView, 1, wx.EXPAND, 0)
        frame.SetSizer(sizer)
        frame.Layout()
        self.SetFrame(frame)
        self.Activate()
        frame.Show()
        return True



    
    def Navigate(self, url):
        if isinstance(self._htmlView, wx.html.HtmlWindow):
            self._htmlView.LoadPage(url)
        else:
            self._htmlView.Navigate(url)

    def OnLocationSelect(self, evt):
        url = self.location.GetStringSelection()
        self._htmlView.Navigate(url)

    def OnLocationKey(self, evt):
        if evt.KeyCode() == wx.WXK_RETURN:
            URL = self.location.GetValue()
            self.location.Append(URL)
            self._htmlView.Navigate(URL)
        else:
            evt.Skip()


    def IgnoreReturn(self, evt):
        if evt.GetKeyCode() != wx.WXK_RETURN:
            evt.Skip()

    def OnOpenButton(self, event):
        dlg = wx.TextEntryDialog(self.GetFrame(), "Open Location",
                                "Enter a full URL or local path",
                                self.current, wx.OK|wx.CANCEL)
        dlg.CentreOnParent()

        if dlg.ShowModal() == wx.ID_OK:
            self.current = dlg.GetValue()
            self.Navigate(self.current)

        dlg.Destroy()

    def OnHomeButton(self, event):
        self._htmlView.GoHome()    ## ET Phone Home!

    def OnPrevPageButton(self, event):
        self._htmlView.GoBack()

    def OnNextPageButton(self, event):
        self._htmlView.GoForward()

    def OnStopButton(self, evt):
        self._htmlView.Stop()

    def OnSearchPageButton(self, evt):
        self._htmlView.GoSearch()

    def OnRefreshPageButton(self, evt):
        self._htmlView.Refresh(iewin.REFRESH_COMPLETELY)

    def OnDocumentComplete(self, evt):
        self.current = evt.URL
        if self.location.FindString(self.current) == wx.NOT_FOUND:
            self.location.Append(self.current)
        self.location.SetValue(self.current)

    def OnTitleChange(self, evt):
        #if self.GetFrame():
            #self.GetFrame().SetTitle(self.titleBase + ' -- ' + evt.Text)
        pass

    def OnStatusTextChange(self, evt):
        #if self.GetFrame():
            #self.GetFrame().SetStatusText(evt.Text)
        pass


    def ProcessEvent(self, event):
        return wx.lib.docview.View.ProcessEvent(self, event)


    def ProcessUpdateUIEvent(self, event):
        return wx.lib.docview.View.ProcessUpdateUIEvent(self, event)


    def OnClose(self, deleteWindow=True):
        if deleteWindow and self.GetFrame():
            self.GetFrame().Destroy()
        return True


class HtmlBrowserDocument(wx.lib.docview.Document):
    """ Generate Unique Doc Type """
    pass


class HtmlBrowserService(wx.lib.pydocview.DocService):

    def __init__(self):
        wx.lib.pydocview.DocService.__init__(self)
        docManager = wx.GetApp().GetDocumentManager()
        htmlBrowserTemplate = wx.lib.docview.DocTemplate(docManager,
                                          _("Internal HTML Browser"),
                                          "*.HTML_Foobar",
                                          "HTML_Foobar",
                                          ".HTML_Foobar",
                                          _("Internal HTML Browser Document"),
                                          _("Internal HTML Browser View"),
                                          HtmlBrowserDocument,
                                          HtmlBrowserView,
                                          flags = wx.lib.docview.TEMPLATE_INVISIBLE,
                                          icon = getHelpIcon())
        docManager.AssociateTemplate(htmlBrowserTemplate)

    

    def InstallControls(self, frame, menuBar = None, toolBar = None, statusBar = None, document = None):
        viewMenu = menuBar.GetMenu(menuBar.FindMenu(_("&View")))

        viewStatusBarItemPos = self.GetMenuItemPos(viewMenu, wx.lib.pydocview.VIEW_STATUSBAR_ID)
        viewMenu.InsertCheckItem(viewStatusBarItemPos + 1, HTML_BROWSER_ID, _("Internal &HTML Browser"), _("Shows or hides the internal HTML browser"))
        wx.EVT_MENU(frame, HTML_BROWSER_ID, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, HTML_BROWSER_ID, frame.ProcessUpdateUIEvent)

        return True



    #----------------------------------------------------------------------------
    # Event Processing Methods
    #----------------------------------------------------------------------------

    def ProcessEvent(self, event):
        id = event.GetId()
        if id == HTML_BROWSER_ID:
            self.OnViewHtmlBrowser(event)
            return True
        else:
            return wx.lib.pydocview.DocService.ProcessEvent(self, event)


    def ProcessUpdateUIEvent(self, event):
        id = event.GetId()
        if id == HTML_BROWSER_ID:
            event.Enable(True)
            docManager = wx.GetApp().GetDocumentManager()
            event.Check(False)
            for doc in docManager.GetDocuments():
                if isinstance(doc, HtmlBrowserDocument):
                    event.Check(True)
                    break
            return True
        else:
            return wx.lib.pydocview.DocService.ProcessUpdateUIEvent(self, event)
        

    #----------------------------------------------------------------------------
    # View Methods
    #----------------------------------------------------------------------------

    def GetDocument(self):
        for doc in wx.GetApp().GetDocumentManager().GetDocuments():
            if isinstance(doc, HtmlBrowserDocument):
                return doc
        return None
    
    def CreateDocument(self, title):
        for template in self.GetDocumentManager().GetTemplates():
            if template.GetDocumentType() == HtmlBrowserDocument:
                newDoc = template.CreateDocument('', wx.lib.docview.DOC_SILENT|wx.lib.docview.DOC_OPEN_ONCE)
                if newDoc:
                    newDoc.SetDocumentName(template.GetDocumentName())
                    newDoc.SetDocumentTemplate(template)
                    newDoc.OnNewDocument()
                    newDoc.SetWriteable(False)
                    newDoc.GetFirstView().GetFrame().SetTitle(title)
                    return newDoc

    def OnViewHtmlBrowser(self, event):
        doc = self.GetDocument()
        if doc:
            doc.DeleteAllViews()
            return
                
        self.CreateDocument(_("Internal HTML Browser"))

    #----------------------------------------------------------------------------
    # Service Methods
    #----------------------------------------------------------------------------

    def LoadUrl(self, url, title = None):
        if not title:
            title = _("Internal HTML Browser")
        doc = self.GetDocument()
        if not doc:
            doc = self.CreateDocument(title)
        view = doc.GetFirstView()
        view.GetFrame().SetTitle(title)
        view.Navigate(url)
        view.Activate()
        
#----------------------------------------------------------------------

from wx import ImageFromStream, BitmapFromImage
from wx import EmptyIcon
import cStringIO, zlib

def getHelpData():
    return zlib.decompress(
'x\xda\x01\x8f\x02p\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x02FIDAT8\x8d\xa5\x93=L\x13a\x18\xc7\x7fWXj\\\
\x9a\xd4^H\xbd\x96\x9e\x03\x87)\xc8Gm\x13\x11\xcb\x00\xa8\x89\x9f(\t\x83q\
\x13GcL\x18t\xd6M\x9c%\x91\xc10\x90h\xf0\x93\xc4P\x12m\x10\x12*\xd5JI\xee\
\x188z-\x95\\c\xe2$e\xea\xeb\xe0\xd1\x82\x0c&\xfa\x8c\xef\xf3\xfc\x7f\xcf\
\xe7+I\xae:\xfe\xc7\xea\xff|XL%\xc5\xaaU\xe6\xabn\x92+\xd8\x004*2\xc7\x9aU\
\x9a\x82nb\xd1\xb8\xb4;^\xda]\xc1\x9b\xc4\xbcx\xf7~\x89X\xbbFk\xb3JH\x91\x11\
\x02r\x1b6\xcb\xbaI\xea\x8b\xc1\x99\x9e\x08\xe7\xfa\xbb\xa4}\x80\xd73\xf3\
\xc2\xb46\xb9\xd8\x7f\x02\xc5\xef#k\x98d\xf5u\x00z\xbb;\x90\xbd\x1e\xf2\xc5\
\x12\xaff\x168\x12l\xe0\xbc\x03\x91$W\x1d\x8b\xa9\xa4x\xfa2\xc3\x9d\x9b\x83\
\x04\xfc>F\xc7\x9e3;\x97F\xf6z\x00\xb0\xbf\xff\xe0\xf6\xf0U\xfa\xba;\xc9\x17\
K<|\xfc\x8c\xeb\x97\xda\x88E\xe3\x92\x0b@\xcf\x95\x89\xb6k(~\x1fk\xd6&\xb3si\
Z\x9aU\xc6\x1f\x8d0>:\x82\xec\xf506\xf1\x16\x01(~\x1f\xc7\xdb5\xf4\\\xb96\
\xc4e\xdd\xe4\xda\x95>*\x02B\xc1\x06\x9e\x8c\x8e\x00P\x01\xb6\xb6\xb6\x11\
\xc0\xc1\x03n\x84\xd3w\xab\xa621\x95\xa8\x01\xac\r\x1b\xe5\xb0\x8c\x00\x84\
\x00\x9fS\xfa\x9a\xb5\xc9\xbd\x07c\x00\xdc\xba1\x88p\x08\x01E\xc6r6T\x8f#\
\x02\xaa\x19\x84\x93yG|\xff\xee0j\xa0\x81\n{\xe3\x00\\\x00AE&\x97\xb7\x11\
\xe27L\x00k\xd67~nms\xe1\xf4I\x1a\x1d\xf1\x8e/\x97\xb7\t(r\r\x10\xd6T\xb2\
\x86Y\xc3\n8\xe4\xf50t\xb9\x97\xa3Z\xa8*\x14\x8e/k\x98\x845u\x17 \xe4f)c`\
\x15KT\x9c\xe1}\xf8\xf8\x99\xc9\x17\xb3\xac\xe8\xebU\xa1\x00\xacb\x89O\x19\
\x83p\xc8]\x03\xc4\xa2q\xe9TDa:\xb1@\xa1X\x02\x01g\xfb\xbb\x00\xe8\xe9\xee\
\x00\x01\x15\x01\x85b\x89\xe9\xc4\x02\xf1\x88R=\xe9=\xa7<95%\x92K\x05:\xdb4\
\xc2M*A\xa7O\xab`\xb3\xb2j\x92\xce\x18\xc4#\nC\x03\x03\xfbOy\xc7\x16SI\x91]/\
\xb3b\x98\xe4\x9dU\x05\x14\x99\xb0\xa6\xd2\x12\xfa\xcbg\xfa\x17\xfb\x05]!\
\xf0\xce_g\xf8\x14\x00\x00\x00\x00IEND\xaeB`\x82\xfb,!\x1e' )

def getHelpBitmap():
    return BitmapFromImage(getHelpImage())

def getHelpImage():
    stream = cStringIO.StringIO(getHelpData())
    return ImageFromStream(stream)

def getHelpIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getHelpBitmap())
    return icon

