#----------------------------------------------------------------------------
# Name:         MessageService.py
# Purpose:      Message View Service for pydocview
#
# Author:       Morgan Hua
#
# Created:      9/2/04
# CVS-ID:       $Id: MessageService.py,v 1.1 2005/06/27 23:26:07 ricard Exp $
# Copyright:    (c) 2005 I-Team
# License:      wxWindows License
#----------------------------------------------------------------------------

import wx
import Service
import STCTextEditor
from wx import ImageFromStream, BitmapFromImage
from wx import EmptyIcon
import cStringIO, zlib

#----------------------------------------------------------------------------
# Utility
#----------------------------------------------------------------------------

def ClearMessages():
    messageService = wx.GetApp().GetService(MessageService)
    view = messageService.GetView()
    if view:
        view.ClearLines()


def ShowMessages(messages, clear=False):
    if ((messages != None) and (len(messages) > 0)):
        messageService = wx.GetApp().GetService(MessageService)
        messageService.ShowWindow(True)
        view = messageService.GetView()
        if view:
            if (clear):
                view.ClearLines()
            for message in messages:
                view.AddLines(message)
                view.AddLines("\n")


#----------------------------------------------------------------------------
# Classes
#----------------------------------------------------------------------------


class MessageView(Service.ServiceView):
    """ Reusable Message View for any document.
        When an item is selected, the document view is called back (with DoSelectCallback) to highlight and display the corresponding item in the document view.
    """

    #----------------------------------------------------------------------------
    # Overridden methods
    #----------------------------------------------------------------------------

    def _CreateControl(self, parent, id):
        txtCtrl = STCTextEditor.TextCtrl(parent, id)
        txtCtrl.SetMarginWidth(1, 0)  # hide line numbers
        txtCtrl.SetReadOnly(True)

        if wx.Platform == '__WXMSW__':
            font = "Courier New"
        else:
            font = "Courier"
        txtCtrl.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL, faceName = font))
        txtCtrl.SetFontColor(wx.BLACK)
        txtCtrl.StyleClearAll()
        txtCtrl.UpdateStyles()
        wx.EVT_SET_FOCUS(txtCtrl, self.OnFocus)

        return txtCtrl

    def GetIcon(self):
        return getInfoIcon()

    def GetDocument(self):
        return None

    def OnFocus(self, event):
        wx.GetApp().GetDocumentManager().ActivateView(self)
        event.Skip()

    def ProcessEvent(self, event):
        stcControl = self.GetControl()
        if not isinstance(stcControl, wx.stc.StyledTextCtrl):
            return wx.lib.docview.View.ProcessEvent(self, event)
        id = event.GetId()
        if id == wx.ID_COPY:
            stcControl.Copy()
            return True
        elif id == wx.ID_CLEAR:
            stcControl.Clear()
            return True
        elif id == wx.ID_SELECTALL:
            stcControl.SetSelection(0, -1)
            return True


    def ProcessUpdateUIEvent(self, event):
        stcControl = self.GetControl()
        if not isinstance(stcControl, wx.stc.StyledTextCtrl):
            return wx.lib.docview.View.ProcessUpdateUIEvent(self, event)
        id = event.GetId()
        if id == wx.ID_CUT or id == wx.ID_PASTE:
            # I don't think cut or paste makes sense from a message/log window.
            event.Enable(False)
            return True
        elif id == wx.ID_COPY:
            hasSelection = (stcControl.GetSelectionStart() != stcControl.GetSelectionEnd()) 
            event.Enable(hasSelection)
            return True
        elif id == wx.ID_CLEAR:
            event.Enable(True)  # wxBug: should be stcControl.CanCut()) but disabling clear item means del key doesn't work in control as expected
            return True
        elif id == wx.ID_SELECTALL:
            event.Enable(stcControl.GetTextLength() > 0)
            return True

        
    #----------------------------------------------------------------------------
    # Service specific methods
    #----------------------------------------------------------------------------

    def ClearLines(self):
        self.GetControl().SetReadOnly(False)
        self.GetControl().ClearAll()
        self.GetControl().SetReadOnly(True)


    def AddLines(self, text):
        self.GetControl().SetReadOnly(False)
        self.GetControl().AddText(text)
        self.GetControl().SetReadOnly(True)


    def GetText(self):
        return self.GetControl().GetText()


    def GetCurrentPos(self):
        return self.GetControl().GetCurrentPos()


    def GetCurrLine(self):
        return self.GetControl().GetCurLine()


    #----------------------------------------------------------------------------
    # Callback Methods
    #----------------------------------------------------------------------------

    def SetCallback(self, callback):
        """ Sets in the event table for a doubleclick to invoke the given callback.
            Additional calls to this method overwrites the previous entry and only the last set callback will be invoked.
        """
        wx.stc.EVT_STC_DOUBLECLICK(self.GetControl(), self.GetControl().GetId(), callback)



class MessageService(Service.Service):


    #----------------------------------------------------------------------------
    # Constants
    #----------------------------------------------------------------------------
    SHOW_WINDOW = wx.NewId()  # keep this line for each subclass, need unique ID for each Service


    #----------------------------------------------------------------------------
    # Overridden methods
    #----------------------------------------------------------------------------

    def _CreateView(self):
        return MessageView(self)


def getInfoData():
    return zlib.decompress(
"x\xda\xeb\x0c\xf0s\xe7\xe5\x92\xe2b``\xe0\xf5\xf4p\t\x02\xd2\x02 \xcc\xc1\
\x06$\xe5?\xffO\x04R,\xc5N\x9e!\x1c@P\xc3\x91\xd2\x01\xe4Wz\xba8\x86X\xf4&\
\xa7\xa4$\xa5-`1\x08\\2\xfb\x19\xdb\x95\xab\xef~\xfe\xff\x7f\x7f\xfe\x7fo\
\xff=\x7f\xfb\x83\x05\x95\x85\x8d\xfd\xce\x1c\xf8\xf0\xdf\x9e\xf9\xf0\xff\
\xff\xdf\xbe\xfd\xfe\xff8\xa4\xf9|C\xear\xbb?\xdb\xb7\xdf\xff_\x7fs%\xbf\x05\
o\xc2\x92H\x9d#G\x9d\xfe\x80\xf5\x95n\xad.hc\xf4c\x16\xd3-K\\r\x82\xf5H\xd3\
\xc3\x1c\x87\x18\x1d\xa5'\xfc2\rK\xbcr^eL\xe7\xeb`e\xd0\xd9\xaa\x1f\xfch\xb1\
\x98\x06\xd0\x11\x0c\x9e\xae~.\xeb\x9c\x12\x9a\x00\xce%N\xa8" )

def getInfoBitmap():
    return BitmapFromImage(getInfoImage())

def getInfoImage():
    stream = cStringIO.StringIO(getInfoData())
    return ImageFromStream(stream)

def getInfoIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getInfoBitmap())
    return icon

