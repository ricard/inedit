#----------------------------------------------------------------------------
# Name:         MarkerService.py
# Purpose:      Adding and removing line markers in text for easy searching
#
# Author:       Morgan Hua
#
# Created:      10/6/03
# CVS-ID:       $Id: MarkerService.py,v 1.1 2005/06/27 23:26:07 ricard Exp $
# Copyright:    (c) 2005 I-Team
# License:      wxWindows License
#----------------------------------------------------------------------------

import wx
import wx.stc
import wx.lib.docview
import wx.lib.pydocview
import STCTextEditor
import sys
import traceback
import pickle
_ = wx.GetTranslation

if wx.Platform == '__WXMSW__':
    try:
        import win32api
        _PYWIN32_INSTALLED = True
    except ImportError:
        _PYWIN32_INSTALLED = False
    _WINDOWS = True
else:
    _WINDOWS = False


class MarkerService(wx.lib.pydocview.DocService):
    MARKERTOGGLE_ID = wx.NewId()
    MARKERDELALL_ID = wx.NewId()
    MARKERNEXT_ID = wx.NewId()
    MARKERPREV_ID = wx.NewId()

            
    # Make sure we're using an expanded path on windows.
    def ExpandPath(path):
        if _WINDOWS:
            try:
                return win32api.GetLongPathName(path)
            except:
                print "Cannot get long path for %s" % path
                
        return path
        
    ExpandPath = staticmethod(ExpandPath)

    def __init__(self):
        self.MARKER_DICT_STRING = "MarkerDict"
        config = wx.ConfigBase_Get()
        pickledMarker = config.Read(self.MARKER_DICT_STRING)
        if pickledMarker:
            try:
                #self._markerDict = pickle.loads(pickledMarker.encode('ascii'))
                self._markerDict = eval(pickledMarker)
            except:
                tp, val, tb = sys.exc_info()
                traceback.print_exception(tp,val,tb)
                self._markerDict = {}
        else:
            self._markerDict = {}

    def OnCloseFrame(self, event):
        # IS THIS THE RIGHT PLACE?
        try:
            config = wx.ConfigBase_Get()
            #config.Write(self.MARKER_DICT_STRING, pickle.dumps(self._markerDict))
            config.Write(self.MARKER_DICT_STRING, repr(self._markerDict))
        except:
            tp,val,tb = sys.exc_info()
            traceback.print_exception(tp, val, tb)
        return True

    def InstallControls(self, frame, menuBar = None, toolBar = None, statusBar = None, document = None):
        if document and document.GetDocumentTemplate().GetDocumentType() != STCTextEditor.TextDocument:
            return
        if not document and wx.GetApp().GetDocumentManager().GetFlags() & wx.lib.docview.DOC_SDI:
            return

        editMenu = menuBar.GetMenu(menuBar.FindMenu(_("&Edit")))
        editMenu.AppendSeparator()
        editMenu.Append(MarkerService.MARKERTOGGLE_ID, _("Toggle &Marker\tCtrl+F2"), _("Toggles a jump marker to text line"))
        wx.EVT_MENU(frame, MarkerService.MARKERTOGGLE_ID, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, MarkerService.MARKERTOGGLE_ID, frame.ProcessUpdateUIEvent)
        editMenu.Append(MarkerService.MARKERDELALL_ID, _("Clear Markers"), _("Removes all jump markers from selected file"))
        wx.EVT_MENU(frame, MarkerService.MARKERDELALL_ID, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, MarkerService.MARKERDELALL_ID, frame.ProcessUpdateUIEvent)
        editMenu.Append(MarkerService.MARKERNEXT_ID, _("Marker Next\tF2"), _("Moves to next marker in selected file"))
        wx.EVT_MENU(frame, MarkerService.MARKERNEXT_ID, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, MarkerService.MARKERNEXT_ID, frame.ProcessUpdateUIEvent)
        editMenu.Append(MarkerService.MARKERPREV_ID, _("Marker Previous\tShift+F2"), _("Moves to previous marker in selected file"))
        wx.EVT_MENU(frame, MarkerService.MARKERPREV_ID, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, MarkerService.MARKERPREV_ID, frame.ProcessUpdateUIEvent)


    def ProcessEvent(self, event):
        id = event.GetId()
        if id == MarkerService.MARKERTOGGLE_ID:
##            wx.GetApp().GetDocumentManager().GetCurrentView().MarkerToggle()
            self.OnToggleMarker(event)
            return True
        elif id == MarkerService.MARKERDELALL_ID:
##            wx.GetApp().GetDocumentManager().GetCurrentView().MarkerDeleteAll()
            self.ClearAllMarkers(wx.GetApp().GetDocumentManager().GetCurrentView().GetDocument().GetFilename())
            return True
        elif id == MarkerService.MARKERNEXT_ID:
            wx.GetApp().GetDocumentManager().GetCurrentView().MarkerNext()
            return True
        elif id == MarkerService.MARKERPREV_ID:
            wx.GetApp().GetDocumentManager().GetCurrentView().MarkerPrevious()
            return True
        else:
            return False


    def ProcessUpdateUIEvent(self, event):
        id = event.GetId()
        if id == MarkerService.MARKERTOGGLE_ID:
            view = wx.GetApp().GetDocumentManager().GetCurrentView()
            event.Enable(hasattr(view, "MarkerToggle"))
            return True
        elif id == MarkerService.MARKERDELALL_ID:
            view = wx.GetApp().GetDocumentManager().GetCurrentView()
            event.Enable(hasattr(view, "MarkerDeleteAll") and view.GetMarkerCount())
            return True
        elif id == MarkerService.MARKERNEXT_ID:
            view = wx.GetApp().GetDocumentManager().GetCurrentView()
            event.Enable(hasattr(view, "MarkerNext") and view.GetMarkerCount())
            return True
        elif id == MarkerService.MARKERPREV_ID:
            view = wx.GetApp().GetDocumentManager().GetCurrentView()
            event.Enable(hasattr(view, "MarkerPrevious") and view.GetMarkerCount())
            return True
        else:
            return False

    def OnToggleMarker(self, event, line=-1, fileName=None):
        if not fileName:
            view = wx.GetApp().GetDocumentManager().GetCurrentView()
            # Test to make sure we aren't the project view.
            if not hasattr(view, 'MarkerExists'):
                return
            fileName = wx.GetApp().GetDocumentManager().GetCurrentDocument().GetFilename()
            if line < 0:
                line = view.GetCtrl().GetCurrentLine()
        if  self.IsMarkerSet(fileName, line + 1):
            self.ClearMarker(fileName, line + 1)
        else:
            self.SetMarker(fileName, line + 1)
        # Now refresh all the markers icons in all the open views.
##        self.ClearAllMarkers()
        self.SetAllMarkers()    
                 
    def IsMarkerSet(self, fileName, line):
        expandedName = MarkerService.ExpandPath(fileName)
        if not self._markerDict.has_key(expandedName):
            return False
        else:
            newList = []
            for number in self._markerDict[expandedName]:
                if(int(number) == int(line)):
                    return True
        return False
        
    def SetMarker(self, fileName, line):
        expandedName = MarkerService.ExpandPath(fileName)
        if not self._markerDict.has_key(expandedName):
            self._markerDict[expandedName] = [line]
        else:
            self._markerDict[expandedName] += [line]

    def ClearMarker(self, fileName, line):
        expandedName = MarkerService.ExpandPath(fileName)
        if not self._markerDict.has_key(expandedName):
            print "In ClearMarker: no key"
            return
        else:
            newList = []
            for number in self._markerDict[expandedName]:
                if(int(number) != int(line)):
                    newList.append(number)
            self._markerDict[expandedName] = newList
       
    def HasMarkersSet(self):
        for key, value in self._markerDict.items():
            if len(value) > 0:
                return True
        return False

    def ClearAllMarkers(self, fileName = None):
        if fileName:
            expandedName = MarkerService.ExpandPath(fileName)
            if self._markerDict.has_key(expandedName):
                del self._markerDict[expandedName]
            wx.GetApp().GetDocumentManager().GetCurrentView().MarkerDeleteAll()
        else:
            self._markerDict = {}
            openDocs = wx.GetApp().GetDocumentManager().GetDocuments()
            for openDoc in openDocs:  
                if(isinstance(openDoc.GetFirstView(), STCTextEditor.TextView)): 
                    openDoc.GetFirstView().MarkerDeleteAll(STCTextEditor.TextView.MARKER_NUM) 

    def GetMarkerList(self, fileName):
        expandedName = MarkerService.ExpandPath(fileName)
        if not self._markerDict.has_key(expandedName):
            return []
        else:
            return self._markerDict[expandedName] 

    def GetMarkerDict(self):
        return self._masterBPDict
        
    def SetAllMarkers(self):
        openDocs = wx.GetApp().GetDocumentManager().GetDocuments()
        for openDoc in openDocs:  
            if(isinstance(openDoc.GetFirstView(), STCTextEditor.TextView)): 
                self.SetCurrentMarkers(openDoc.GetFirstView())
            
    def SetCurrentMarkers(self, view):
        if isinstance(view, STCTextEditor.TextView) and hasattr(view, 'GetDocument'):
            view.MarkerDeleteAll(STCTextEditor.TextView.MARKER_NUM)
            for linenum in self.GetMarkerList(view.GetDocument().GetFilename()): 
                view.MarkerAdd(lineNum=int(linenum) - 1, marker_index=STCTextEditor.TextView.MARKER_NUM)
