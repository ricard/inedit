#----------------------------------------------------------------------------
# Name:         Service.py
# Purpose:      Basic Reusable Service View for wx.lib.pydocview
#
# Author:       Morgan Hua
#
# Created:      11/4/04
# CVS-ID:       $Id: Service.py,v 1.1 2005/06/27 23:26:08 ricard Exp $
# Copyright:    (c) 2005 I-Team
# License:      wxWindows License
#----------------------------------------------------------------------------

import wx
import wx.lib.docview
import wx.lib.pydocview
_ = wx.GetTranslation


FLOATING_MINIFRAME = -1
DOCUMENT_FRAME = -2

class ServiceViewMixIn:
    topTab = None
    bottomTab = None
    leftTab = None
    rightTab = None
    topLeftTab = None
    bottomLeftTab = None
    topRightTab = None
    bottomRightTab = None

    def __init__(self, service):
        wx.EvtHandler.__init__(self)
        self._viewFrame = None
        self._service = service
        self._control = None
        self._embeddedWindow = None


    def GetFrame(self):
        return self._viewFrame


    def SetFrame(self, frame):
        self._viewFrame = frame


    def _CreateControl(self, parent, id):
        return None
        
    
    def GetControl(self):
        return self._control


    def SetControl(self, control):
        self._control = control


    def GetIcon(self):
        return None
    
    
    def OnCreate(self, doc, flags):
        config = wx.ConfigBase_Get()
        windowLoc = self._service.GetEmbeddedWindowLocation()
        if windowLoc == FLOATING_MINIFRAME:
            pos = config.ReadInt(self._service.GetServiceName() + "FrameXLoc", -1), config.ReadInt(self._service.GetServiceName() + "FrameYLoc", -1)
            # make sure frame is visible
            screenWidth = wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            screenHeight = wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if pos[0] < 0 or pos[0] >= screenWidth or pos[1] < 0 or pos[1] >= screenHeight:
                pos = wx.DefaultPosition

            size = wx.Size(config.ReadInt(self._service.GetServiceName() + "FrameXSize", -1), config.ReadInt(self._service.GetServiceName() + "FrameYSize", -1))
            title = _(self._service.GetServiceName())
            if wx.GetApp().GetDocumentManager().GetFlags() & wx.lib.docview.DOC_SDI and wx.GetApp().GetAppName():
                title =  title + " - " + wx.GetApp().GetAppName()
            frame = wx.MiniFrame(wx.GetApp().GetTopWindow(), -1, title, pos = pos, size = size, style = wx.CLOSE_BOX|wx.CAPTION|wx.SYSTEM_MENU)
            wx.EVT_CLOSE(frame, self.OnCloseWindow)
        elif wx.GetApp().IsMDI():
            if windowLoc == DOCUMENT_FRAME:
                title = _(self._service.GetServiceName())
                class FakeDocChildPanel(wx.Panel):
                    def __init__(self, view):
                        wx.Panel.__init__(self, docNb, -1)
                        self.view = view
                    def GetView(self):
                        return self.view
                    def IsMaximized(self):
                        return True
                    
                if wx.GetApp().GetUseTabbedMDI():
                    docNb = wx.GetApp().GetTopWindow().GetNotebook()
                    assert(isinstance(docNb, wx.Notebook))
                    frame = FakeDocChildPanel(self)
                    docNb.AddPage(frame, title, True)
                else:
                    frame = wx.MDIChildFrame(wx.GetApp().GetTopWindow(), -1, title)
            else:
                self._embeddedWindow = wx.GetApp().GetTopWindow().GetEmbeddedWindow(windowLoc)
                frame = self._embeddedWindow
        else:
            pos = config.ReadInt(self._service.GetServiceName() + "FrameXLoc", -1), config.ReadInt(self._service.GetServiceName() + "FrameYLoc", -1)
            # make sure frame is visible
            screenWidth = wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            screenHeight = wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if pos[0] < 0 or pos[0] >= screenWidth or pos[1] < 0 or pos[1] >= screenHeight:
                pos = wx.DefaultPosition

            size = wx.Size(config.ReadInt(self._service.GetServiceName() + "FrameXSize", -1), config.ReadInt(self._service.GetServiceName() + "FrameYSize", -1))
            title = _(self._service.GetServiceName())
            if wx.GetApp().GetDocumentManager().GetFlags() & wx.lib.docview.DOC_SDI and wx.GetApp().GetAppName():
                title =  title + " - " + wx.GetApp().GetAppName()
            frame = wx.GetApp().CreateDocumentFrame(self, doc, flags, pos = pos, size = size)
            frame.SetTitle(title)
            if config.ReadInt(self._service.GetServiceName() + "FrameMaximized", False):
                frame.Maximize(True)
            wx.EVT_CLOSE(frame, self.OnCloseWindow)

        self.SetFrame(frame)
        sizer = frame.GetSizer()
        if not sizer:
            sizer = wx.BoxSizer(wx.VERTICAL)
            frame.SetSizer(sizer)
        
        windowLoc = self._service.GetEmbeddedWindowLocation()
        if self._embeddedWindow or windowLoc == FLOATING_MINIFRAME:
            if (windowLoc > 0):
                if (self._service.GetEmbeddedWindowLocation() == wx.lib.pydocview.EMBEDDED_WINDOW_BOTTOM):
                    if ServiceView.bottomTab == None:
                        ServiceView.bottomTab = self.MakeNotebook(frame, sizer, "Bottom Tab")
                    nb = ServiceView.bottomTab
                elif (self._service.GetEmbeddedWindowLocation() == wx.lib.pydocview.EMBEDDED_WINDOW_TOPLEFT):
                    if ServiceView.topLeftTab == None:
                        ServiceView.topLeftTab = self.MakeNotebook(frame, sizer, "Top Left Tab")
                    nb = ServiceView.topLeftTab
                elif (self._service.GetEmbeddedWindowLocation() == wx.lib.pydocview.EMBEDDED_WINDOW_LEFT):
                    if ServiceView.leftTab == None:
                        ServiceView.leftTab = self.MakeNotebook(frame, sizer, "Left Tab")
                    nb = ServiceView.leftTab
                elif (self._service.GetEmbeddedWindowLocation() == wx.lib.pydocview.EMBEDDED_WINDOW_BOTTOMLEFT):
                    if ServiceView.bottomLeftTab == None:
                        ServiceView.bottomLeftTab = self.MakeNotebook(frame, sizer, "Bottom Left Tab")
                    nb = ServiceView.bottomLeftTab
                else:
                    assert(False)
                # Factor this out.
                self._control = self._CreateControl(nb, wx.NewId())
                icon = self.GetIcon()
                if icon:
                    imgList = nb.GetImageList()
                    if not imgList:
                        imgList = wx.ImageList(16, 16)
                        nb.AssignImageList(imgList)
                    iconIndex = imgList.AddIcon(icon)
                else:
                    iconIndex = -1
                if self._control != None:
                    nb.AddPage(self._control, self._service.GetServiceName(), False, iconIndex)
                nb.Layout()
            else:
                # Factor this out.
                self._control = self._CreateControl(frame, wx.NewId())
                sizer.Add(self._control)
        else:
            # Factor this out.
            self._control = self._CreateControl(frame, wx.NewId())
            sizer.Add(self._control, 1, wx.EXPAND, 0)
        frame.Layout()
        self.Activate()
        return True

    def MakeNotebook(self, frame, sizer, name):
        notebook = wx.Notebook(frame, wx.NewId(), (0,0), (100,100), wx.LB_DEFAULT, name)
        wx.EVT_RIGHT_DOWN(notebook, self.OnNotebookRightClick)
        wx.EVT_MIDDLE_DOWN(notebook, self.OnNotebookMiddleClick)
        sizer.Add(notebook, 1, wx.TOP|wx.EXPAND, 4)
        #def OnFrameResize(event):
            #notebook.SetSize(notebook.GetParent().GetSize())
        #frame.Bind(wx.EVT_SIZE, OnFrameResize)
        return notebook

    def OnNotebookMiddleClick(self, event):
        notebook = event.GetEventObject()
        index, type = ServiceView.bottomTab.HitTest(event.GetPosition())
        # 0 tab is always message. This code assumes the rest are run/debug windows
        if index > 0:
            page = notebook.GetPage(index)
            if hasattr(page, 'StopAndRemoveUI'):
                page.StopAndRemoveUI(event)
 
       
    def OnNotebookRightClick(self, event):
        notebook = event.GetEventObject()
        index, type = notebook.HitTest(event.GetPosition())
        menu = wx.Menu()
        x, y = event.GetX(), event.GetY()
        # 0 tab is always message. This code assumes the rest are run/debug windows
        #if index > 0:
        page = notebook.GetPage(index)
        if hasattr(page, 'StopAndRemoveUI'):
            id = wx.NewId()
            menu.Append(id, _("Close"))
            def OnRightMenuSelect(event):
                if hasattr(page, 'StopAndRemoveUI'):
                    page.StopAndRemoveUI(event)
            wx.EVT_MENU(notebook, id, OnRightMenuSelect)
        if notebook.GetPageCount() > 1:
            for i in range(notebook.GetPageCount()-1, 0, -1): # Go from len-1 to 1
                page = notebook.GetPage(i)
                if hasattr(page, 'StopAndRemoveUI'):
                    id = wx.NewId()
                    menu.Append(id, _("Close all [Run] or [Debug] tabs"))
                    def OnRightMenuSelect(event):
                        for i in range(notebook.GetPageCount()-1, 0, -1): # Go from len-1 to 1
                            page = notebook.GetPage(i)
                            if hasattr(page, 'StopAndRemoveUI'):
                                page.StopAndRemoveUI(event)
                    wx.EVT_MENU(notebook, id, OnRightMenuSelect)
                    break
        
        notebook.PopupMenu(menu, wx.Point(x, y))
        menu.Destroy()

        
    def OnCloseWindow(self, event):
        frame = self.GetFrame()
        config = wx.ConfigBase_Get()
        if frame and not self._embeddedWindow:
            if not frame.IsMaximized():
                config.WriteInt(self._service.GetServiceName() + "FrameXLoc", frame.GetPositionTuple()[0])
                config.WriteInt(self._service.GetServiceName() + "FrameYLoc", frame.GetPositionTuple()[1])
                config.WriteInt(self._service.GetServiceName() + "FrameXSize", frame.GetSizeTuple()[0])
                config.WriteInt(self._service.GetServiceName() + "FrameYSize", frame.GetSizeTuple()[1])
            config.WriteInt(self._service.GetServiceName() + "FrameMaximized", frame.IsMaximized())

        if not self._embeddedWindow:
            windowLoc = self._service.GetEmbeddedWindowLocation()
            if windowLoc == FLOATING_MINIFRAME:
                # don't destroy it, just hide it
                frame.Hide()
            elif windowLoc == DOCUMENT_FRAME:
                parent = frame.GetParent()
                if isinstance(parent, wx.Notebook):
                    pageIndex = wx.NOT_FOUND
                    for i in range(parent.GetPageCount()):
                        if self.GetControl() == parent.GetPage(i):
                            parent.RemovePage(i)
                            break
                else:
                    frame.Hide()
            else:
                # Call the original OnCloseWindow, could have subclassed SDIDocFrame and MDIDocFrame but this is easier since it will work for both SDI and MDI frames without subclassing both
                frame.OnCloseWindow(event)


    #----------------------------------------------------------------------------
    # Callback Methods
    #----------------------------------------------------------------------------

    def SetCallback(self, callback):
        """ Sets in the event table for a doubleclick to invoke the given callback.
            Additional calls to this method overwrites the previous entry and only the last set callback will be invoked.
        """
        wx.stc.EVT_STC_DOUBLECLICK(self.GetControl(), self.GetControl().GetId(), callback)


    #----------------------------------------------------------------------------
    # Display Methods
    #----------------------------------------------------------------------------

    def IsShown(self):
        frame = self.GetFrame()
        if self._service.GetEmbeddedWindowLocation() == DOCUMENT_FRAME:
            parent = frame.GetParent()
        elif self.GetControl():
            parent = self.GetControl().GetParent()
        else:
            parent = None
        if isinstance(parent, wx.Notebook):
            pageIndex = wx.NOT_FOUND
            for i in range(parent.GetPageCount()):
                if self.GetControl() == parent.GetPage(i):
                    pageIndex = i
                    break
            return pageIndex != wx.NOT_FOUND
        elif not self.GetFrame():
            return False
        return self.GetFrame().IsShown()


    def Hide(self):
        self.Show(False)


    def Show(self, show = True):
        frame = self.GetFrame()
        if self._service.GetEmbeddedWindowLocation() == DOCUMENT_FRAME:
            parent = frame.GetParent()
        elif self.GetControl():
            parent = self.GetControl().GetParent()
        else:
            parent = None
        if isinstance(parent, wx.Notebook):
            pageIndex = wx.NOT_FOUND
            for i in range(parent.GetPageCount()):
                if self.GetControl() == parent.GetPage(i):
                    pageIndex = i
                    break
            if show:
                if not frame.IsShown():
                    self.ShowEmbedded(True)
                if pageIndex != wx.NOT_FOUND:
                    return
                parent.AddPage(self.GetControl(), self._service.GetServiceName())
            else:
                if pageIndex != wx.NOT_FOUND:
                    parent.RemovePage(pageIndex)
                    if parent.GetPageCount() == 0:
                        self.ShowEmbedded(False)
        else:
            self.ShowEmbedded(show)


    def ShowEmbedded(self, show = True):
        if self.GetFrame():
            self.GetFrame().Show(show)
            if self._embeddedWindow:
                mdiParentFrame = wx.GetApp().GetTopWindow()
                mdiParentFrame.ShowEmbeddedWindow(self.GetFrame(), show)



class ServiceView(ServiceViewMixIn, wx.EvtHandler):
    """ Basic Service View.
    """
    
    #----------------------------------------------------------------------------
    # Overridden methods
    #----------------------------------------------------------------------------

    def __init__(self, service):
        wx.EvtHandler.__init__(self)
        ServiceViewMixIn.__init__(self, service)


    def Destroy(self):
        wx.EvtHandler.Destroy(self)


    def Activate(self, activate = True):
        """ Dummy function for SDI mode """
        pass


    def Close(self, deleteWindow = True):
        """
        Closes the view by calling OnClose. If deleteWindow is true, this
        function should delete the window associated with the view.
        """
        if deleteWindow:
            self.Destroy()

        return True




class Service(wx.lib.pydocview.DocService):


    #----------------------------------------------------------------------------
    # Constants
    #----------------------------------------------------------------------------
    SHOW_WINDOW = wx.NewId()  # keep this line for each subclass, need unique ID for each Service


    def __init__(self, serviceName, embeddedWindowLocation = wx.lib.pydocview.EMBEDDED_WINDOW_LEFT):
        self._serviceName = serviceName
        self._embeddedWindowLocation = embeddedWindowLocation
        self._view = None


    def GetEmbeddedWindowLocation(self):
        return self._embeddedWindowLocation


    def SetEmbeddedWindowLocation(self, embeddedWindowLocation):
        self._embeddedWindowLocation = embeddedWindowLocation


    def InstallControls(self, frame, menuBar = None, toolBar = None, statusBar = None, document = None):
        viewMenu = menuBar.GetMenu(menuBar.FindMenu(_("&View")))
        menuItemPos = self.GetMenuItemPos(viewMenu, viewMenu.FindItem(_("&Status Bar"))) + 1

        viewMenu.InsertCheckItem(menuItemPos, self.SHOW_WINDOW, self.GetMenuString(), self.GetMenuDescr())
        wx.EVT_MENU(frame, self.SHOW_WINDOW, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, self.SHOW_WINDOW, frame.ProcessUpdateUIEvent)

        return True


    def GetServiceName(self):
        """ String used to save out Service View configuration information """
        return self._serviceName


    def GetMenuString(self):
        """ Need to override this method to provide menu item for showing Service View """
        return _(self.GetServiceName())


    def GetMenuDescr(self):
        """ Need to override this method to provide menu item for showing Service View """
        return _("Show or hides the %s window") % self.GetMenuString()


##    def OnViewChanged(self, view):
##        """Can be overloaded to make some GUI changes when changing active document."""
##        pass

    #----------------------------------------------------------------------------
    # Event Processing Methods
    #----------------------------------------------------------------------------

    def ProcessEvent(self, event):
        id = event.GetId()
        if id == self.SHOW_WINDOW:
            self.ToggleWindow(event)
            return True
        else:
            return False


    def ProcessUpdateUIEvent(self, event):
        id = event.GetId()
        if id == self.SHOW_WINDOW:
            event.Check(self._view != None and self._view.IsShown())
            event.Enable(True)
            return True
        else:
            return False


    #----------------------------------------------------------------------------
    # View Methods
    #----------------------------------------------------------------------------

    def _CreateView(self):
        """ This method needs to be overridden with corresponding ServiceView """
        return ServiceView(self)


    def GetView(self):
        # Window Menu Service Method
        return self._view


    def SetView(self, view):
        self._view = view


    def ShowWindow(self, show = True):
        if show:
            if self._view:
                if not self._view.IsShown():
                    self._view.Show()
            else:
                view = self._CreateView()
                view.OnCreate(self, flags = 0)  # hack: give self in place of wx.Document
                self.SetView(view)
        else:
            if self._view:
                if self._view.IsShown():
                    self._view.Hide()


    def HideWindow(self):
        self.ShowWindow(False)


    def ToggleWindow(self, event):
        show = event.IsChecked()
        wx.ConfigBase_Get().WriteInt(self.GetServiceName()+"Shown", show)
        self.ShowWindow(show)


    def OnCloseFrame(self, event):
        if not self._view:
            return True

        if wx.GetApp().IsMDI():
            self._view.OnCloseWindow(event)
        # This is called when any SDI frame is closed, so need to check if message window is closing or some other window
        elif self._view == event.GetEventObject().GetView():
            self.SetView(None)
        
        return True
        
    #----------------------------------------------------------------------------
    # Document Methods
    #----------------------------------------------------------------------------
    def GetPrintableName(self):
        return _('Help system')
    
    def GetDocumentTemplate(self):
        return None