#----------------------------------------------------------------------------
# Name:         HelpService.py
# Purpose:      Service providing help to the user
#
# Author:       Cedric RICARD
#
# Created:      20/02/2006
# Copyright:    (c) 2006 I-Team
#----------------------------------------------------------------------------

import wx
import wx.lib.docview
import wx.lib.pydocview
import wx.lib.filebrowsebutton as filebrowse
import Service
import lib.util.sysutils as sysutils
import os
import stat
import HtmlBrowserService

_ = wx.GetTranslation


class HelpService(wx.lib.pydocview.DocService):

    _trees = {}
    def __init__(self):
        wx.lib.pydocview.DocService.__init__(self)
        #self.htmlHelp = wx.html.HtmlHelpController()
        #self.htmlHelp.AddBook(os.path.join(sysutils.getAppPath(), 'doc', 'ApplicationHelp', 'fr', 'in-edit fr.hhp'))

    
    #----------------------------------------------------------------------------
    # Constants
    #----------------------------------------------------------------------------
    SHOW_WINDOW = wx.NewId()  # keep this line for each subclass, need unique ID for each Service


    def InstallControls(self, frame, menuBar = None, toolBar = None, statusBar = None, document = None):
        helpMenu = menuBar.GetMenu(menuBar.FindMenu(_("&Help")))

        helpMenu.Insert(0, wx.ID_HELP, _("Help Contents & Index\tF1"), _("Help Contents & Index"))
        wx.EVT_MENU(frame, wx.ID_HELP, self.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, wx.ID_HELP, self.ProcessUpdateUIEvent)
        helpMenu.InsertSeparator(1)

        toolBar.AddSeparator()
        toolBar.AddTool(wx.ID_HELP, getHelpBitmap(), shortHelpString = _("Help Contents & Index"), longHelpString = _("Shows Help Contents & Index"))
        toolBar.Realize()

        return True



    #----------------------------------------------------------------------------
    # Event Processing Methods
    #----------------------------------------------------------------------------

    def ProcessEvent(self, event):
        id = event.GetId()
        if id == wx.ID_HELP:
            self.OnHelpIndex(event)
            return True
        else:
            return wx.lib.pydocview.DocService.ProcessEvent(self, event)


    def ProcessUpdateUIEvent(self, event):
        id = event.GetId()
        if id == wx.ID_HELP:
            #event.Check(self._view != None and self._view.IsShown())
            event.Enable(True)
            return True
        else:
            return wx.lib.pydocview.DocService.ProcessUpdateUIEvent(self, event)

    #----------------------------------------------------------------------------
    # View Methods
    #----------------------------------------------------------------------------

    #def _CreateView(self):
        #""" This method needs to be overridden with corresponding ServiceView """
        #return HelpView(self)

    def OnHelpIndex(self, event):
        htmlBrowserService = wx.GetApp().GetService(HtmlBrowserService.HtmlBrowserService)
        #self.htmlHelp.DisplayContents()
        url = os.path.join(sysutils.getAppPath(), '..', 'doc', 'fr', 'ApplicationHelp.htm')
        if not os.path.exists(url):
            url = os.path.join(sysutils.getAppPath(), 'doc', 'fr', 'ApplicationHelp.htm')
        config = wx.ConfigBase_Get()
        if htmlBrowserService and config.ReadBool("Help/InternalBrowser", True):
            htmlBrowserService.LoadUrl(url, _("Online Help"))
        else:
            browser = config.Read("Help/ExternalBrowser", "")
            if not browser:
                if not wx.LaunchDefaultBrowser(url):
                    wx.MessageBox('Default browser can\'t be found.\n\nPleasy verify your operating system configuration or select a specific location into In-Edit preferences.\n\nOn Linux systems, environment variable "BROWSER" sould be set.',
                                  'Help browser error.',
                                  wx.ICON_ERROR)
            elif os.path.exists(browser):
                os.spawnl(os.P_NOWAIT, browser, url);
            else:
                wx.MessageBox('Can\'t found browser on location \'%s\'.\n\nPleasy verify your preferences.' % browser,
                              'Help browser error.',
                              wx.ICON_ERROR)
                
                
                

    #----------------------------------------------------------------------------
    # Service Methods
    #----------------------------------------------------------------------------

class HelpOptionsPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        config = wx.ConfigBase_Get()
        self.browserPath = config.Read("Help/ExternalBrowser", "")
        self.useInternal = config.ReadBool("Help/InternalBrowser", True)
        self._useDefault = not self.browserPath

        mainSizer = wx.BoxSizer(wx.VERTICAL)                
        helpBrowserSizer = wx.StaticBoxSizer(wx.StaticBox(self, -1, _('Help Browser')), wx.VERTICAL)

        self._internalBrowserCheck = wx.RadioButton(self, -1, _('Internal browser'), style = wx.RB_SINGLE)
        helpBrowserSizer.Add(self._internalBrowserCheck, 0, wx.ALL, 4)
        
        self._externalBrowserCheck = wx.RadioButton(self, -1, _('External browser'), style = wx.RB_SINGLE)
        helpBrowserSizer.Add(self._externalBrowserCheck, 0, wx.ALL, 4)
        
        gridSizer = wx.FlexGridSizer(2, 2)
        gridSizer.AddGrowableCol(1)
        gridSizer.Add(wx.Size(30, 0))
        self._defaultExtBrowserCheck = wx.RadioButton(self, -1, _('Operating system default browser'), style = wx.RB_SINGLE)
        gridSizer.Add(self._defaultExtBrowserCheck, 0, wx.ALL, 4)
        
        gridSizer.Add(wx.Size(30, 0))
        tmpSizer = wx.BoxSizer(wx.HORIZONTAL)
        self._selectExtBrowserCheck = wx.RadioButton(self, -1, _('Specified:'), style = wx.RB_SINGLE)
        tmpSizer.Add(self._selectExtBrowserCheck, 0, wx.ALIGN_CENTER_VERTICAL)

        self._fbb = filebrowse.FileBrowseButton(
            self, -1, size=(450, -1), labelText = '', buttonText = _('Browse'),
            dialogTitle = _('Select the external browser location...')
            )
        tmpSizer.Add(self._fbb, 1, wx.GROW)
        gridSizer.Add(tmpSizer, 0, wx.GROW | wx.ALL, 4)
        
        helpBrowserSizer.Add(gridSizer, 0, wx.GROW)
        mainSizer.Add(helpBrowserSizer, 0, wx.GROW)

        self.SetSizer(mainSizer)

        self.Bind(wx.EVT_RADIOBUTTON, self.OnRadioButton)
        
        self.SetControlStates()
        self._fbb.SetValue(self.browserPath)
        parent.AddPage(self, _("Help"))
        
    def OnRadioButton(self, event):
        obj = event.GetEventObject()
        if not event.IsChecked():
            return
        if obj == self._internalBrowserCheck:
            self.useInternal = True
        elif obj == self._externalBrowserCheck:
            self.useInternal = False
        elif obj == self._defaultExtBrowserCheck:
            self._useDefault = True
            self.browserPath = ''
        elif obj == self._selectExtBrowserCheck:
            self._useDefault = False
            self.browserPath = self._fbb.GetValue()
        self.SetControlStates()
            
    def SetControlStates(self):
        self._internalBrowserCheck.SetValue(self.useInternal)
        self._externalBrowserCheck.SetValue(not self.useInternal)
        self._defaultExtBrowserCheck.Enable(not self.useInternal)
        self._selectExtBrowserCheck.Enable(not self.useInternal)
        self._fbb.Enable(not self.useInternal and not self._useDefault)
        self._defaultExtBrowserCheck.SetValue(self._useDefault)
        self._selectExtBrowserCheck.SetValue(not self._useDefault)

    def OnOK(self, optionsDialog):
        config = wx.ConfigBase_Get()
        config.Write("Help/ExternalBrowser", self.browserPath)
        config.WriteBool("Help/InternalBrowser", self.useInternal)

#----------------------------------------------------------------------------

from wx import ImageFromStream, BitmapFromImage
from wx import EmptyIcon
import cStringIO, zlib

def getHelpData():
    return zlib.decompress(
'x\xda\x01\x8f\x02p\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x02FIDAT8\x8d\xa5\x93=L\x13a\x18\xc7\x7fWXj\\\
\x9a\xd4^H\xbd\x96\x9e\x03\x87)\xc8Gm\x13\x11\xcb\x00\xa8\x89\x9f(\t\x83q\
\x13GcL\x18t\xd6M\x9c%\x91\xc10\x90h\xf0\x93\xc4P\x12m\x10\x12*\xd5JI\xee\
\x188z-\x95\\c\xe2$e\xea\xeb\xe0\xd1\x82\x0c&\xfa\x8c\xef\xf3\xfc\x7f\xcf\
\xe7+I\xae:\xfe\xc7\xea\xff|XL%\xc5\xaaU\xe6\xabn\x92+\xd8\x004*2\xc7\x9aU\
\x9a\x82nb\xd1\xb8\xb4;^\xda]\xc1\x9b\xc4\xbcx\xf7~\x89X\xbbFk\xb3JH\x91\x11\
\x02r\x1b6\xcb\xbaI\xea\x8b\xc1\x99\x9e\x08\xe7\xfa\xbb\xa4}\x80\xd73\xf3\
\xc2\xb46\xb9\xd8\x7f\x02\xc5\xef#k\x98d\xf5u\x00z\xbb;\x90\xbd\x1e\xf2\xc5\
\x12\xaff\x168\x12l\xe0\xbc\x03\x91$W\x1d\x8b\xa9\xa4x\xfa2\xc3\x9d\x9b\x83\
\x04\xfc>F\xc7\x9e3;\x97F\xf6z\x00\xb0\xbf\xff\xe0\xf6\xf0U\xfa\xba;\xc9\x17\
K<|\xfc\x8c\xeb\x97\xda\x88E\xe3\x92\x0b@\xcf\x95\x89\xb6k(~\x1fk\xd6&\xb3si\
Z\x9aU\xc6\x1f\x8d0>:\x82\xec\xf506\xf1\x16\x01(~\x1f\xc7\xdb5\xf4\\\xb96\
\xc4e\xdd\xe4\xda\x95>*\x02B\xc1\x06\x9e\x8c\x8e\x00P\x01\xb6\xb6\xb6\x11\
\xc0\xc1\x03n\x84\xd3w\xab\xa621\x95\xa8\x01\xac\r\x1b\xe5\xb0\x8c\x00\x84\
\x00\x9fS\xfa\x9a\xb5\xc9\xbd\x07c\x00\xdc\xba1\x88p\x08\x01E\xc6r6T\x8f#\
\x02\xaa\x19\x84\x93yG|\xff\xee0j\xa0\x81\n{\xe3\x00\\\x00AE&\x97\xb7\x11\
\xe27L\x00k\xd67~nms\xe1\xf4I\x1a\x1d\xf1\x8e/\x97\xb7\t(r\r\x10\xd6T\xb2\
\x86Y\xc3\n8\xe4\xf50t\xb9\x97\xa3Z\xa8*\x14\x8e/k\x98\x845u\x17 \xe4f)c`\
\x15KT\x9c\xe1}\xf8\xf8\x99\xc9\x17\xb3\xac\xe8\xebU\xa1\x00\xacb\x89O\x19\
\x83p\xc8]\x03\xc4\xa2q\xe9TDa:\xb1@\xa1X\x02\x01g\xfb\xbb\x00\xe8\xe9\xee\
\x00\x01\x15\x01\x85b\x89\xe9\xc4\x02\xf1\x88R=\xe9=\xa7<95%\x92K\x05:\xdb4\
\xc2M*A\xa7O\xab`\xb3\xb2j\x92\xce\x18\xc4#\nC\x03\x03\xfbOy\xc7\x16SI\x91]/\
\xb3b\x98\xe4\x9dU\x05\x14\x99\xb0\xa6\xd2\x12\xfa\xcbg\xfa\x17\xfb\x05]!\
\xf0\xce_g\xf8\x14\x00\x00\x00\x00IEND\xaeB`\x82\xfb,!\x1e' )

def getHelpBitmap():
    return BitmapFromImage(getHelpImage())

def getHelpImage():
    stream = cStringIO.StringIO(getHelpData())
    return ImageFromStream(stream)

def getHelpIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getHelpBitmap())
    return icon

