#----------------------------------------------------------------------------
# Name:         AnubisEditor.py
# Purpose:      AnubisEditor for wx.lib.pydocview tbat uses the Styled Text Control
#
# Author:       Peter Yared
#
# Created:      8/15/03
# CVS-ID:       $Id: AnubisEditor.py,v 1.3 2005/07/06 17:18:49 ricard Exp $
# Copyright:    (c) 2005 I-Team.
# License:      wxWindows License
#----------------------------------------------------------------------------

import CodeEditor
import wx
import wx.lib.docview
import wx.lib.pydocview
import string
import wx.py  # For the Anubis interpreter
import wx.stc # For the Anubis interpreter
import cStringIO  # For indent
import OutlineService
import STCTextEditor
import STCStyleEditor
import os
import sys # for GetAutoCompleteKeywordList
import MessageService # for OnCheckCode
import OutlineService
import CodeAssistService
#import pyggy
import BuildService
import time
import anubis
import ConfigParser
from bison import BisonParser, BisonNode, BisonError

_ = wx.GetTranslation

if wx.Platform == '__WXMSW__':
    _WINDOWS = True
else:
    _WINDOWS = False

anubisKeywords = 'alert delegate else if is lock protect terminal then with define global public read type'


##VIEW_ANUBIS_INTERPRETER_ID = wx.NewId()


class AnubisDocument(CodeEditor.CodeDocument):
    pass


class AnubisView(CodeEditor.CodeView):

    def ProcessUpdateUIEvent(self, event):
        if not self.GetCtrl():
            return False
            
##        id = event.GetId()
##        if id == CodeEditor.CHECK_CODE_ID:
##            hasText = self.GetCtrl().GetTextLength() > 0
##            event.Enable(hasText)
##            return True
            
        return CodeEditor.CodeView.ProcessUpdateUIEvent(self, event)


    def GetCtrlClass(self):
        """ Used in split window to instantiate new instances """
        return AnubisCtrl


    def OnActivateView(self, activate, activeView, deactiveView):
        STCTextEditor.TextView.OnActivateView(self, activate, activeView, deactiveView)
        if activate:
            wx.CallAfter(self.LoadOutline)  # need CallAfter because document isn't loaded yet
        

    def OnClose(self, deleteWindow = True):
        status = STCTextEditor.TextView.OnClose(self, deleteWindow)
        wx.CallAfter(self.ClearOutline)  # need CallAfter because when closing the document, it is Activated and then Close, so need to match OnActivateView's CallAfter
        return status
       

    def GetAutoCompleteDefaultKeywords(self):
        return anubisKeywords.split()


    #def GetAutoCompleteKeywordList(self, context, hint):
       #return anubisKeywords


    def OnCheckCode(self):
        pass

    def OnJumpToFoundLine(self, event):
        messageService = wx.GetApp().GetService(MessageService.MessageService)
        lineText, pos = messageService.GetView().GetCurrLine()
        
        lineEnd = lineText.find(".py:")
        if lineEnd == -1:
            return

        lineStart = lineEnd + len(".py:")
        lineEnd = lineText.find(":", lineStart)
        lineNum = int(lineText[lineStart:lineEnd])

        filename = lineText[0:lineStart - 1]

        foundView = None
        openDocs = wx.GetApp().GetDocumentManager().GetDocuments()
        for openDoc in openDocs:
            if openDoc.GetFilename() == filename:
                foundView = openDoc.GetFirstView()
                break

        if not foundView:
            doc = wx.GetApp().GetDocumentManager().CreateDocument(filename, wx.lib.docview.DOC_SILENT)
            foundView = doc.GetFirstView()

        if foundView:
            foundView.GetFrame().SetFocus()
            foundView.Activate()
            foundView.GotoLine(lineNum)
            startPos = foundView.PositionFromLine(lineNum)
            endPos = foundView.GetLineEndPosition(lineNum)
            # wxBug:  Need to select in reverse order, (end, start) to put cursor at head of line so positioning is correct
            #         Also, if we use the correct positioning order (start, end), somehow, when we open a edit window for the first
            #         time, we don't see the selection, it is scrolled off screen
            foundView.SetSelection(endPos, startPos)
            wx.GetApp().GetService(OutlineService.OutlineService).LoadOutline(foundView, position=startPos)


    def DoLoadOutlineCallback(self, force=False):
        outlineService = wx.GetApp().GetService(OutlineService.OutlineService)
        if not outlineService:
            return False

        outlineView = outlineService.GetView()
        if not outlineView:
            return False

        treeCtrl = outlineView.GetTreeCtrl()
        if not treeCtrl:
            return False

        view = treeCtrl.GetCallbackView()
        
        newCheckSum = self.GenCheckSum()
        if view is not self:
            treeCtrl.DeleteAllItems()
            rootItem = treeCtrl.AddRoot(_(u'Computing...'))
            treeCtrl.SetDoSelectCallback(rootItem, self, None)
            force = True
        if not hasattr(self, '_outlineCheckSum'):
            self._outlineCheckSum = -1
        if self._outlineCheckSum != newCheckSum or force:
            self._outlineCheckSum = newCheckSum
            self._outlineDateLastModification = time.time()
            self._outlineUpToDate = False
            if view is self and not force:
                return False
        
        if self._outlineUpToDate or time.time() - self._outlineDateLastModification < 2.0:
            return False

        self._outlineUpToDate = True
        treeCtrl.DeleteAllItems()

        document = self.GetDocument()
        assert(document)
        if not document:
            return True

        filename = document.GetFilename()
        if filename:
            treeCtrl.DeleteAllItems()
            rootItem = treeCtrl.AddRoot(_(u'Computing...'))
            treeCtrl.SetDoSelectCallback(rootItem, self, None)
        else:
            return True

        codeAssistService = wx.GetApp().GetService(CodeAssistService.CodeAssistService)
        sourceParser = codeAssistService.GetSourceParser(filename)
        paragraphes = sourceParser.GetParagraphs()
        
        treeCtrl.DeleteAllItems()
        rootItem = treeCtrl.AddRoot(os.path.basename(filename), OutlineService.OUTLINE_ICON)
        treeCtrl.SetDoSelectCallback(rootItem, self, None)

        for par in paragraphes:
            assert(isinstance(par, BisonNode))
            icon = OutlineService.UNKNOWN_ICON
            if isinstance(par[0], BisonNode):
                if par[0].target == 'OperationDefinition' or par[0].target == 'OperationDeclaration':
                    start, end, itemStr = sourceParser.RecursivelyExtractOperationDescription(par[0], endToken = 'yy__equals')
                    if par[0][0].names[0] == 'yy__operation':
                        icon = OutlineService.PRIVATE_FUNCTION_ICON
                    elif par[0][0].names[0] == 'yy__p_operation':
                        icon = OutlineService.PUBLIC_FUNCTION_ICON
                    elif par[0][0].names[0] == 'yy__g_operation':
                        icon = OutlineService.GLOBAL_FUNCTION_ICON
                elif par[0].target == 'VariableDeclaration':
                    start, end, itemStr = sourceParser.RecursivelyExtractOperationDescription(par[0], endToken = 'yy__equals')
                    if par[0][0].names[0] == 'yy__variable':
                        icon = OutlineService.PRIVATE_VARIABLE_ICON
                    elif par[0][0].names[0] == 'yy__p_varialbe':
                        icon = OutlineService.PUBLIC_VARIABLE_ICON
                elif par[0].target == 'TypeDefinition':
                    start, end, itemStr = sourceParser.RecursivelyExtractOperationDescription(par[0], endToken = 'yy__colon')
                    if par[0][0].names[0] == 'yy__type':
                        icon = OutlineService.PRIVATE_TYPE_ICON
                    elif par[0][0].names[0] == 'yy__p_type':
                        icon = OutlineService.PUBLIC_TYPE_ICON
                else:
                    start, end, itemStr = (par[0].pos[4], par[0].pos[5], par[0].target)
            else:
                start, end, itemStr = sourceParser.RecursivelyExtractOperationDescription(par, endToken = None)
            item = treeCtrl.AppendItem(rootItem, itemStr, icon)
            treeCtrl.SetDoSelectCallback(item, view, (end, start))  # select in reverse order because we want the cursor to be at the start of the line so it wouldn't scroll to the right
            
        treeCtrl.Expand(rootItem)

        del paragraphes
        del sourceParser
        return True


    def GetLanguageTitle(self):
        '''Overide to set your language title'''
        return _(u'Anubis')

    
    def GetLanguageName(self):
        '''Overide to set your language name'''
        return u'anubis'



class AnubisService(CodeEditor.CodeService):


    def __init__(self):
        CodeEditor.CodeService.__init__(self)
        config = wx.ConfigBase_Get()
        anubisConfig = ConfigParser.RawConfigParser()
        if not config.Read("/Anubis/Path", '') or not config.Read("/Anubis/MyAnubis", ''):
            configFile = os.path.expanduser('~/my_anubis/anubis.conf')
            if not os.path.exists(configFile):
                for path in os.environ['PATH'].split(os.pathsep):
                    configFile = os.path.join(path, 'anubis.conf')
                    if os.path.exists(configFile):
                        break
            if os.path.exists(configFile):
                anubisConfig.read(configFile)
                config.Write("/Anubis/Path", anubisConfig.get('PATH', 'ANUBIS'))
                config.Write("/Anubis/MyAnubis", anubisConfig.get('PATH', 'MY_ANUBIS'))

        if not config.Read("/Anubis/CompilerLocation", ''):
            config.Write("/Anubis/CompilerLocation", os.path.join(anubisConfig.get('PATH', 'ANUBIS'), 'anubis.exe'))
        if not config.Read("/Anubis/InterpreterLocation", ''):
            config.Write("/Anubis/InterpreterLocation", os.path.join(anubisConfig.get('PATH', 'ANUBIS'), 'anbexec.exe'))
        if not config.Read("/Anubis/CompilerCommandLine", ''):
            config.Write("/Anubis/CompilerCommandLine", '"%(file)s"')


    def InstallControls(self, frame, menuBar = None, toolBar = None, statusBar = None, document = None):
        CodeEditor.CodeService.InstallControls(self, frame, menuBar, toolBar, statusBar, document)

        if document and document.GetDocumentTemplate().GetDocumentType() != AnubisDocument:
            return
        if not document and wx.GetApp().GetDocumentManager().GetFlags() & wx.lib.docview.DOC_SDI:
            return
        
        buildService = wx.GetApp().GetService(BuildService.BuildService)
        if buildService == None:
            wx.LogWarning(_('BuildService not found. Anubis sources won\'t be able to be compiled to executable object code.'))
        else:
            buildService.AddBuilder(AnubisBuilder())

        DebuggerService.DebuggerService.availableExecutors.append(AnubisExecutor)


    def ProcessEvent(self, event):
        id = event.GetId()
##        if id == VIEW_ANUBIS_INTERPRETER_ID:
##            self.OnViewAnubisInterpreter(event)
##            return True
##        else:
        return CodeEditor.CodeService.ProcessEvent(self, event)


    def ProcessUpdateUIEvent(self, event):
        id = event.GetId()
##        if id == VIEW_ANUBIS_INTERPRETER_ID:
##            event.Enable(True)
##            docManager = wx.GetApp().GetDocumentManager()
##            event.Check(False)
##            for doc in docManager.GetDocuments():
##                if isinstance(doc.GetFirstView(), AnubisInterpreterView):
##                    event.Check(True)
##                    break
##            return True
##        else:
        return CodeEditor.CodeService.ProcessUpdateUIEvent(self, event)




class AnubisCtrl(CodeEditor.CodeCtrl):

    # Character styles
    ANUBIS_DEFAULT = 0
    ANUBIS_COMMENT = 1
    ANUBIS_NUMBER = 4
    ANUBIS_STRING = 5
    ANUBIS_CHARACTER = 6
    ANUBIS_PREDEF_TYPE = 7
    ANUBIS_DEFNAME = 8
    ANUBIS_OPERATOR = 9
    ANUBIS_STRINGEOL = 10
    ANUBIS_KEYWORD = 11
    ANUBIS_IDENTIFIER = 12
    ANUBIS_ERROR = 14
    
    styles = [[ANUBIS_DEFAULT, []],
              [ANUBIS_COMMENT, []],
              [ANUBIS_NUMBER, ['yy__integer', 'yy__utvar', 'yy__float', ]],
              [ANUBIS_STRING, ['yy__anb_string', ]],
              [ANUBIS_CHARACTER, ['yy__char', ]],
              [ANUBIS_PREDEF_TYPE, ['yy__type_String', 'yy__type_ByteArray', 'yy__type_Int32', 'yy__type_Float', 
                                  'yy__type_Listener', 'yy__RAddr', 'yy__WAddr', 'yy__RWAddr', 'yy__GAddr', 
                                  'yy__Var', 'yy__type_Omega', 'yy__StructPtr', ]],
              [ANUBIS_DEFNAME, ['yy__operation', 'yy__g_operation', 'yy__p_operation', 'yy__p_type', 'yy__p_variable', ]],
              [ANUBIS_OPERATOR, ['yy__lbracket', 'yy__dot', 'yy__rbracket', 'yy__comma', 'yy__semicolon', 'yy__plus', 
                                 'yy__minus', 'yy__star', 'yy__carret', 'yy__ampersand', 
                                 'yy__vbar', 'yy__equals', 'yy__exchange', 'yy__lpar', 'yy__percent', 'yy__rbrace', 
                                 'yy__lbrace', 'yy__rpar', 'yy__colon', 'yy__implies',
                                 'yy__arrow', 'yy__ndarrow', 'yy__left_shift', 'yy__right_shift', 'yy__arroww', 
                                 'yy__non_equal', 'yy__tilde', 'yy__rpar_arrow', 'yy__rpar_ndarrow', 'yy__dots', 
                                 'yy__less', 'yy__greater', 'yy__lessoreq', 'yy__greateroreq', 'yy__slash',
                                 'yy__exists', 'yy__exists_unique', 'yy__description', 'yy__mapsto', 'yy__rec_mapsto',
                                 'yy__write', 'yy__mod', ]],
              [ANUBIS_STRINGEOL, []],
              [ANUBIS_KEYWORD, ['yy__if', 'yy__is', 'yy__then', 'yy__type', 'yy__read', 'yy__else',
                                'yy__read', 'yy__forall', 'yy__with', 'yy__wait_for', 'yy__delegate', 
                                'yy__succeeds', 'yy__succeeds_as', 'yy__protect', 'yy__alert', 'yy__MVar',
                                'yy__avm', 'yy__terminal', 'yy__checking_every', 'yy__connect_to_file', 'yy__connect_to_IP', 
                                'yy__load_module', 'yy__bit_width', 'yy__vcopy', 'yy__serialize', 'yy__unserialize', ]],
              [ANUBIS_IDENTIFIER, ['yy__variable', 'yy__symbol', 'yy__Symbol', ]],
              [ANUBIS_ERROR, []],
              ]
       #'yy__theorem', 'yy__p_theorem', 'yy__proof', 
#'yy__decimal_digit', 'yy__indirect', 
#'yy__type_Proof', 
#'yy__C_constr_for',
#'yy__debug_avm', 
#'yy__since', 'yy__lock', 'yy__alt_number', 'yy__config_file', 
#'yy__verbose', 'yy__stop_after', 'yy__language', 'yy__mapstoo', 'yy__rec_mapstoo', 'yy__we_have', 
#'yy__enough', 'yy__let', 'yy__assume', 'yy__indeed', 'yy__hence', ]

    _parser = None
    __checksum = None
    _tree = None

    def __init__(self, parent, ID = -1, style = wx.NO_FULL_REPAINT_ON_RESIZE):
        CodeEditor.CodeCtrl.__init__(self, parent, ID, style)
#        self.SetProperty("tab.timmy.whinge.level", "1")
#        self.SetProperty("fold.comment.python", "1")
#        self.SetProperty("fold.quotes.python", "1")
        #self.lex, self.ltab = pyggy.getlexer("lexer.pyl", debug=1)
        #self._parser = anubis.Parser(verbose=0, debug = 1, keepfiles = 0, file = self)
        #self.currentReadLine = 0
        self.SetLexer(wx.stc.STC_LEX_CONTAINER)
        self.SetKeyWords(0, anubisKeywords)
        self.Bind(wx.stc.EVT_STC_STYLENEEDED, self.OnStyleNeeded)
        #self.SetModEventMask(wx.stc.STC_MOD_INSERTTEXT | wx.stc.STC_MOD_DELETETEXT
                             #| wx.stc.STC_PERFORMED_REDO | wx.stc.STC_PERFORMED_UNDO
                             #| wx.stc.STC_PERFORMED_USER)
        #self.Bind(wx.stc.EVT_STC_MODIFIED, self.OnStcModified)
        #self.SetMarginWidth(0, 80)
        self.SetFoldFlags(16)


    def GetView(self):
        if hasattr(self, "_dynSash"):
            return self._dynSash._view
        return None


    def GenCheckSum(stc):
        """ Poor man's checksum.  We'll assume most changes will change the length of the file.
        """
        return stc.GetTextLength()
    GenCheckSum = staticmethod(GenCheckSum)


    def SetViewDefaults(self):
        CodeEditor.CodeCtrl.SetViewDefaults(self, configPrefix = "Anubis/", hasWordWrap = False, hasTabs = True)


    def GetFontAndColorFromConfig(self):
        return CodeEditor.CodeCtrl.GetFontAndColorFromConfig(self, configPrefix = "Anubis/")


    def UpdateStyles(self):
        CodeEditor.CodeCtrl.UpdateStyles(self)

        STCStyleEditor.initSTC(self, None, 'anubis')
        event = wx.stc.StyledTextEvent(wx.stc.wxEVT_STC_STYLENEEDED, self.GetId())
        event.SetEventObject(self)
        event.SetPosition(self.GetTextLength())
        self.AddPendingEvent(event)


    STYLE_MASK = 0x1F
        
        
    def OnStyleNeeded(self, event):
        AnubisCtrl.DoStyleNeeded(event)


    def GetStyleFromToken(tokenName):
        for style, tokens in AnubisCtrl.styles:
            if tokenName in tokens:
                return style
        return AnubisCtrl.ANUBIS_ERROR
    GetStyleFromToken = staticmethod(GetStyleFromToken)
        
        
    def RecurseStyling(node, stc):
        assert(isinstance(stc, wx.stc.StyledTextCtrl))
        for name, val in zip(node.names, node.values):
            if isinstance(val, BisonNode):
                AnubisCtrl.RecurseStyling(val, stc)
            elif name == 'error':
                if len(val.split(':', 2)) == 3:
                    tokenStart, tokenEnd, value = val.split(':', 2)
                else:
                    tokenStart, tokenEnd, value = ('?', '?', val)
                try:
                    print 'Parsing error at pos %s: token = "%s"' % (tokenStart, value)
                except:
                    pass
            else:
                #print "  %s=%s" % (name, val)
                tokenStart, tokenEnd, value = val.split(':', 2)
                if tokenStart > stc.GetEndStyled():
                    stc.SetStyling(int(tokenStart) - stc.GetEndStyled(), AnubisCtrl.ANUBIS_COMMENT)
                stc.SetStyling(int(tokenEnd) - stc.GetEndStyled(), AnubisCtrl.GetStyleFromToken(name))
    RecurseStyling = staticmethod(RecurseStyling)
    
    
    def DoStyleNeeded(event):
        t0 = time.clock()
        stc = event.GetEventObject()
        assert(isinstance(stc, wx.stc.StyledTextCtrl))
            
        lineNumber = stc.LineFromPosition(stc.GetEndStyled())
        startPos = stc.PositionFromLine(lineNumber)
        endPos = event.GetPosition()
        lineEnd = stc.LineFromPosition(endPos)
        lineLength = stc.LineLength(lineNumber)

        ##print 'DoStyleNeeded %d ==> %d' % (startPos, endPos)
        try:
            codeAssistService = wx.GetApp().GetService(CodeAssistService.CodeAssistService)
            view = None
            if hasattr(stc, 'GetView'):
                view = stc.GetView()
            if view:
                doc = view.GetDocument()
                
                sourceParser = codeAssistService.GetSourceParser(doc.GetFilename())
            else:
                ## this is in case of STCStyleEditor dialog
                sourceParser = CodeAssistService.SourceParser(codeAssistService._parser, 'No File')
                sourceParser.SetTextCtrl(stc)
                
            tokens = sourceParser.UpdateTokens(startPos, endPos)
            if len(tokens) > 0:
                posFirstToken = tokens[0][1][4]
                if posFirstToken < startPos:
                    stc.StartStyling(tokens[0][1][4], 0x1F)
                else:
                    stc.StartStyling(startPos, 0x1F)
            else:
                stc.StartStyling(startPos, 0x1F)
            for token, pos in tokens:
                tokenStart, tokenEnd = pos[4:6]
                if tokenStart >= endPos:
                    break
                if tokenStart > stc.GetEndStyled():
                    stc.SetStyling(int(tokenStart) - stc.GetEndStyled(), AnubisCtrl.ANUBIS_COMMENT)
                tokenEnd = min(int(tokenEnd), stc.GetTextLength())
                stc.SetStyling(tokenEnd - stc.GetEndStyled(), AnubisCtrl.GetStyleFromToken(token))
            ## Now, update folder status
            if lineNumber > 0:
                level = stc.GetFoldLevel(lineNumber - 1)
                prevStyle = stc.GetStyleAt(stc.PositionFromLine(lineNumber - 1))
            else:
                level = wx.stc.STC_FOLDLEVELBASE
                prevStyle = AnubisCtrl.ANUBIS_DEFAULT
            prevNonWhiteLine = -1
            for line in range(lineNumber, lineEnd + 1):
                text = stc.GetLine(line)
                newStyle = stc.GetStyleAt(stc.PositionFromLine(line) + stc.GetLineIndentation(line))
                newLevel = wx.stc.STC_FOLDLEVELBASE
                if stc.GetLineIndentation(line) > 0:
                    newLevel += 1
                if len(text.strip()) == 0:
                    stc.SetFoldLevel(line, level | wx.stc.STC_FOLDLEVELWHITEFLAG)
                    continue
                elif newLevel > level and prevNonWhiteLine >= 0:
                    stc.SetFoldLevel(prevNonWhiteLine, level | wx.stc.STC_FOLDLEVELHEADERFLAG)
                if newStyle == AnubisCtrl.ANUBIS_COMMENT and newStyle != prevStyle:
                    #print line, stc.PositionFromLine(line), text
                    stc.SetFoldLevel(line, wx.stc.STC_FOLDLEVELBASE  | wx.stc.STC_FOLDLEVELHEADERFLAG)
                else:
                    stc.SetFoldLevel(line, newLevel)
                level = newLevel
                prevNonWhiteLine = line
                prevStyle = newStyle
        except:
            excep = sys.exc_info()
            print sys.excepthook(excep[0], excep[1], excep[2])
            stc.StartStyling(0, 0x1F)
        if endPos > stc.GetEndStyled():
            stc.SetStyling(endPos - stc.GetEndStyled(), AnubisCtrl.ANUBIS_COMMENT)

        #print 'DoStyleNeeded ==> %f' % (time.clock() - t0)
    DoStyleNeeded = staticmethod(DoStyleNeeded)


    def OnUpdateUI(self, evt):
        braces = self.GetMatchingBraces()
        
        # check for matching braces
        braceAtCaret = -1
        braceOpposite = -1
        charBefore = None
        caretPos = self.GetCurrentPos()
        if caretPos > 0:
            charBefore = self.GetCharAt(caretPos - 1)
            styleBefore = self.GetStyleAt(caretPos - 1)

        # check before
        if charBefore and chr(charBefore) in braces and styleBefore == AnubisCtrl.ANUBIS_OPERATOR:
            braceAtCaret = caretPos - 1

        # check after
        if braceAtCaret < 0:
            charAfter = self.GetCharAt(caretPos)
            styleAfter = self.GetStyleAt(caretPos)
            if charAfter and chr(charAfter) in braces and styleAfter == AnubisCtrl.ANUBIS_OPERATOR:
                braceAtCaret = caretPos

        if braceAtCaret >= 0:
            braceOpposite = self.BraceMatch(braceAtCaret)

        if braceAtCaret != -1  and braceOpposite == -1:
            self.BraceBadLight(braceAtCaret)
        else:
            self.BraceHighlight(braceAtCaret, braceOpposite)

        evt.Skip()


    def OnStcModified(self, event):
        assert(isinstance(event, wx.stc.StyledTextEvent))
        pos = event.GetPosition()
        oldLen = newLen = 0
        if event.GetModificationType() & wx.stc.STC_MOD_DELETETEXT:
            print 'Deleted %d char at %d' % (event.GetLength(), pos)
            oldLen = event.GetLength()
        elif event.GetModificationType() & wx.stc.STC_MOD_INSERTTEXT:
            print 'Inserted %d char at %d' % (event.GetLength(), pos)
            newLen = event.GetLength()
        else:
            return
        if hasattr(self, 'GetView'):
            view = self.GetView()
            if view:
                doc = view.GetDocument()
                codeAssistService = wx.GetApp().GetService(CodeAssistService.CodeAssistService)
                sourceParser = codeAssistService.GetSourceParser(doc.GetFilename())
                sourceParser.UpdateRegion(pos, oldLen, newLen)
        
##    def DoIndent(self):
##        pass


class AnubisOptionsPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)

        mainSizer = wx.BoxSizer(wx.VERTICAL)                

        pathLabel = wx.StaticText(self, -1, _("Anubis compiler path:"))
        config = wx.ConfigBase_Get()
        path = config.Read("Anubis/CompilerLocation", 'anubis.exe')
        self._compilerPathTextCtrl = wx.TextCtrl(self, -1, path, size = (150, -1))
        self._compilerPathTextCtrl.SetToolTipString(self._compilerPathTextCtrl.GetValue())
        self._compilerPathTextCtrl.SetInsertionPointEnd()
        choosePathButton = wx.Button(self, -1, _("Browse..."))
        pathSizer = wx.BoxSizer(wx.HORIZONTAL)
        HALF_SPACE = 5
        pathSizer.Add(pathLabel, 0, wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT | wx.TOP, HALF_SPACE)
        pathSizer.Add(self._compilerPathTextCtrl, 0, wx.ALIGN_LEFT | wx.EXPAND | wx.RIGHT, HALF_SPACE)
        pathSizer.Add(choosePathButton, 0, wx.ALIGN_RIGHT | wx.LEFT, HALF_SPACE)
        wx.EVT_BUTTON(self, choosePathButton.GetId(), self.OnChooseCompilerPath)

        mainSizer.Add(pathSizer, 0, wx.LEFT | wx.RIGHT | wx.TOP, 10)

        argsLabel = wx.StaticText(self, -1, _("Anubis compiler args:"))
        args = config.Read("Anubis/CompilerCommandLine", '"%(file)s"')
        self._compilerArgsTextCtrl = wx.TextCtrl(self, -1, args, size = (150, -1))
        self._compilerArgsTextCtrl.SetToolTipString(self._compilerArgsTextCtrl.GetValue())
        self._compilerArgsTextCtrl.SetInsertionPointEnd()
        pathSizer = wx.BoxSizer(wx.HORIZONTAL)
        pathSizer.Add(argsLabel, 0, wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT | wx.TOP, HALF_SPACE)
        pathSizer.Add(self._compilerArgsTextCtrl, 0, wx.ALIGN_LEFT | wx.EXPAND | wx.RIGHT, HALF_SPACE)

        mainSizer.Add(pathSizer, 0, wx.LEFT | wx.RIGHT | wx.TOP, 10)

        pathLabel = wx.StaticText(self, -1, _("Anubis interpreter path:"))
        path = config.Read("Anubis/InterpreterLocation", 'anbexec.exe')
        self._interpreterPathTextCtrl = wx.TextCtrl(self, -1, path, size = (150, -1))
        self._interpreterPathTextCtrl.SetToolTipString(self._interpreterPathTextCtrl.GetValue())
        self._interpreterPathTextCtrl.SetInsertionPointEnd()
        choosePathButton = wx.Button(self, -1, _("Browse..."))
        pathSizer = wx.BoxSizer(wx.HORIZONTAL)
        pathSizer.Add(pathLabel, 0, wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT | wx.TOP, HALF_SPACE)
        pathSizer.Add(self._interpreterPathTextCtrl, 0, wx.ALIGN_LEFT | wx.EXPAND | wx.RIGHT, HALF_SPACE)
        pathSizer.Add(choosePathButton, 0, wx.ALIGN_RIGHT | wx.LEFT, HALF_SPACE)
        wx.EVT_BUTTON(self, choosePathButton.GetId(), self.OnChooseInterpreterPath)

        mainSizer.Add(pathSizer, 0, wx.LEFT | wx.RIGHT | wx.TOP, 10)

        self._otherOptions = STCTextEditor.TextOptionsPanel(self, -1, configPrefix = "Anubis/", label = "Anubis", hasWordWrap = False, hasTabs = True, addPage=False)
        mainSizer.Add(self._otherOptions)
        self.SetSizer(mainSizer)
        parent.AddPage(self, _("Anubis"))
        
    def OnChooseCompilerPath(self, event):
        if _WINDOWS:
            wildcard = _("*.exe")
        else:
            wildcard = _("*")
        path, file = os.path.split(self._compilerPathTextCtrl.GetValue())
        fullpath = wx.FileSelector(_("Please select Anubis compiler"),
                               path,
                               file,
                               wildcard = wildcard ,
                               flags = wx.HIDE_READONLY,
                               parent = wx.GetApp().GetTopWindow())
        if fullpath:  
            self._compilerPathTextCtrl.SetValue(fullpath)
            self._compilerPathTextCtrl.SetToolTipString(self._compilerPathTextCtrl.GetValue())
            self._compilerPathTextCtrl.SetInsertionPointEnd()

    def OnChooseInterpreterPath(self, event):
        if _WINDOWS:
            wildcard = _("*.exe")
        else:
            wildcard = _("*")
        path, file = os.path.split(self._interpreterPathTextCtrl.GetValue())
        fullpath = wx.FileSelector(_("Please select Anubis interpreter"),
                               path,
                               file,
                               wildcard = wildcard ,
                               flags = wx.HIDE_READONLY,
                               parent = wx.GetApp().GetTopWindow())
        if fullpath:  
            self._interpreterPathTextCtrl.SetValue(fullpath)
            self._interpreterPathTextCtrl.SetToolTipString(self._interpreterPathTextCtrl.GetValue())
            self._interpreterPathTextCtrl.SetInsertionPointEnd()

    def OnOK(self, optionsDialog):
        config = wx.ConfigBase_Get()
        if len(self._compilerPathTextCtrl.GetValue()) > 0:
            config.Write("Anubis/CompilerLocation", self._compilerPathTextCtrl.GetValue())
        if len(self._compilerArgsTextCtrl.GetValue()) > 0:
            config.Write("Anubis/CompilerCommandLine", self._compilerArgsTextCtrl.GetValue())
        if len(self._interpreterPathTextCtrl.GetValue()) > 0:
            config.Write("Anubis/InterpreterLocation", self._interpreterPathTextCtrl.GetValue())

        self._otherOptions.OnOK(optionsDialog)
#----------------------------------------------------------------------------
import BuildService
import codecs
import re

class AnubisBuilder(BuildService.Builder):
    """Anubis language builder."""

    def CanHandleFile(self, file):
        return file.endswith('.anubis')

    def ExtractTargetFiles(file):
        try:
            docFile = codecs.open(file, 'r', 'utf-8')
            data = docFile.read()
        except:
            try:
                docFile = codecs.open(file, 'r', 'latin-1')
                data = docFile.read()
            except:
                docFile = file(file, 'r')
                data = docFile.read()
        regexpr = re.compile('^[Gg]lobal[ \r\n]+define[ \r\n]+([Oo]ne)?[ \r\n]+(?P<name>[A-Za-z_][A-Za-z0-9_]*)', re.M)
        pos = 0
        length = len(data)
        modules = []
        while pos < len:
            match = regexpr.search(data, pos)
            if match == None:
                break
            pos = match.end()
            modules.append(match.group('name') + '.adm')
        return modules
    ExtractTargetFiles = staticmethod(ExtractTargetFiles)

    def __init__(self):
        BuildService.Builder.__init__(self)
        
    def Clean(self, filename):
        config = wx.ConfigBase_Get()
        anubisSourcesDir = config.Read("Anubis/MyAnubis", '')
        modules = AnubisBuilder.ExtractTargetFiles(filename)
        for file in modules:
            os.remove(os.path.join(anubisSourcesDir, 'modules', file))
        
    def Compile(self, filename):
        config = wx.ConfigBase_Get()
        compiler = config.Read("Anubis/CompilerLocation", 'anubis.exe')
        cmdLine = config.Read("Anubis/CompilerCommandLine", '"%(file)s"')
        path, leafname = os.path.split(filename)
        args = cmdLine % {'file': leafname}
        evt = DebuggerService.UpdateTextEvent(value = _(u"*** Calling '%s %s' ***\n" % (compiler, args)), bold = True)
        wx.PostEvent(self._wxComponent, evt)
        self.Execute(compiler, args, path)
    
    def Link(self, project):
        pass
    
    def Make(self, project, makeall = False):
        pass


import DebuggerService
import process

class AnubisExecutor(DebuggerService.Executor):
    
    def GetInterpreterPath():
        config = wx.ConfigBase_Get()
        path = config.Read("Anubis/InterpreterLocation")
        if path:
            return path
        wx.MessageBox(_("To proceed I need to know the location of the Anubis Interpreter you would like to use.\nTo set this, go to Tools-->Options and use the 'Anubis' tab to enter a value.\n"), _("Anubis Interpreter Location Unknown"))
        return None
    GetInterpreterPath = staticmethod(GetInterpreterPath)   
     
    def __init__(self, fileName, wxComponent, callbackOnExit=None):
        DebuggerService.Executor.__init__(self, fileName, wxComponent, callbackOnExit = callbackOnExit)
        path = AnubisExecutor.GetInterpreterPath()
        self._cmd = '"' + path + '"' # + '" -u \"' + fileName + '\"'
        
        self._stdOutReader = None
        self._stdErrReader = None
        self._process = None
                    
    def CanHandleFile(file):
        return file.lower().endswith('.adm') or file.lower().endswith('.anubis')
    CanHandleFile = staticmethod(CanHandleFile)   

    def DoExecute(self, arguments, startIn=None, environment=None):
        if not startIn:
            startIn = str(os.getcwd())
        startIn = os.path.abspath(startIn)
        command = self._cmd + ' ' + arguments
        #stdinput = process.IOBuffer()
        #self._process = process.ProcessProxy(command, mode='b', cwd=startIn, stdin=stdinput)
        self._process = process.ProcessOpen(command, mode='b', cwd=startIn, env=environment)
        # Kick off threads to read stdout and stderr and write them
        # to our text control. 
        self._stdOutReader = DebuggerService.OutputReaderThread(self._process.stdout, self._stdOutCallback, callbackOnExit=self._callbackOnExit)
        self._stdOutReader.start()
        self._stdErrReader = DebuggerService.OutputReaderThread(self._process.stderr, self._stdErrCallback, accumulate=False)
        self._stdErrReader.start()
                
    
    def DoStopExecution(self):
        if(self._process != None):
            self._process.kill()
            self._process.close()
            self._process = None
        if(self._stdOutReader != None):
            self._stdOutReader.AskToStop()
        if(self._stdErrReader != None):
            self._stdErrReader.AskToStop()

#----------------------------------------------------------------------------
# Icon Bitmaps - generated by encode_bitmaps.py
#----------------------------------------------------------------------------
from wx import ImageFromStream, BitmapFromImage
from wx import EmptyIcon
import cStringIO, zlib

#----------------------------------------------------------------------
def getAnubisData():
    return zlib.decompress(
'x\xda\x01\xc7\x018\xfe\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x01~IDAT8\x8d\x8d\x93\xbfK\x02a\x18\xc7?\x9a\
\x104U\x93\xd4\xd4\xd0\xde\xe6\xd6\x90MI\rB\xf5\x07\x14\xd1\x96\xd0PK\x10E4\
\xb6E\x834\x04\xad\x05I\x04\xa1\xd1\x0f\xa4\x1a\xac\x86\x90D2,\xd3\xc44R\xcf\
\xdf\xdd5xz\xe7]\x1e}\xe1\xe1y\xdf\xe7y\x9f\xef\xf7\xfb\x1e\xf7\x9a|\x81\x88\
\x84\x06\xe1\xd7$\x00\xa9\xb4\xc0\xca\xec\xa8I\xdbo\x81/\x10\x91^\nR3\x1e\
\xb3\xf5\xd89\xbc\x96N\x13\x92\xb4\xee\xf6\xea\x04\xd40\xffU\xccW\x94\xb5\
\xcdagnu\xbf-I\x0b\x81P\xab\xe7r\xa9H4\x9a\xe0\xf6\xd8\xc7\xcc\xf0\x10\xd9L\
\xd2\xd8\x81PS\x86\xf3\x15\xb8\xf7_\xb0\xe9rbs\xd8\xe9\xed\xef3\xba\x01\xe6\
\xc6\x07S[\x17~\xea9%\xefc\xb1\x84\xb1\x83|E\x19\xceV\x95\xe6\x9d\xd7C\xec9d\
\xe8\xc0\x92J\x0b\xcdMc\xf8\xbb\n\xd6\x81A\xf6\xd6\x16I\xbfEx\x12\x8f\xda\
\x13hU\x1b\xb6?^\xc2\xcd\xda\xe7{\x08\xe0\x00p\xea\xae\x90+\xc6\x11\x01\x11\
\x88\x15D\xca\x95\x1a\xc2W\x8eN\xbd\xd8\x04\x90\x01\xb6\x80\x9eF\x98\x01\xe2\
\x05\x91xA\x04\xa0\xcb\xe7a\xd25\xc5\x12\xd0\xddJ\xd0!\x0f\xb9d\xa2\x0cpi\
\xd1\xcaX\xe7\xeb.G\x80\x12\xe0\x96O\xcaH\x00W\xaa\xe3\x0b\x16\x7f\xb0\x8c?\
\xb8\xdb\xacl\xa8\xbacr\xde\x06\xc6\x818<\\\xc0\xb4ZP\xf7P\xceA\xf7\xdb\x9e\
\xc9\x8en\xe0dY\xe1\x05\xda\xbc\x05-F\x0cz\xff"0\xc2/.\xdf\xa4i)_\nP\x00\x00\
\x00\x00IEND\xaeB`\x82\x7f\x9e\xce\xce' )

def getAnubisBitmap():
    return BitmapFromImage(getAnubisImage())

def getAnubisImage():
    stream = cStringIO.StringIO(getAnubisData())
    return ImageFromStream(stream)

def getAnubisIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getAnubisBitmap())
    return icon

