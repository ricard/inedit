##----------------------------------------------------------------------------
# Name:         CodeAssistService.py
# Purpose:      Service that parses Anubis code and help developper
#
# Author:       Cedric RICARD
#
# Created:      16/01/2006
# Copyright:    (c) 2006 I-Team
#----------------------------------------------------------------------------

import wx
import wx.lib.docview
import wx.lib.pydocview
import wx.html
import Service
import lib.util.sysutils as sysutils
import os
import stat
import anubis
from bison import BisonParser, BisonNode, BisonError

_ = wx.GetTranslation


FLOATING_MINIFRAME = -1
startParTokens = ['yy__type', 'yy__p_type',
                  'yy__C_constr_for',
                  'yy__operation', 'yy__p_operation', 'yy__g_operation', 
                  'yy__theorem', 'yy__p_theorem',
                  'yy__variable', 'yy__p_variable',
                  'yy__read', 'yy__replaced_by', 'error']

class FileLike:
    def __init__(self, stc, start = 0, length = None):
        self.stc = stc
        self.pos = 0
        self.start = start
        self.currentReadLine = 0
        self.startLine = stc.LineFromPosition(start)
        if length != None:
            self.length = length
        else:
            self.length = self.stc.GetTextLength()
        ##print '%s Init' % repr(self)

    def readline(self, bytes):
        if self.pos >= self.length or self.currentReadLine > self.stc.GetLineCount():
            #print '%s %d : <EOF>' % (repr(self), self.currentReadLine)
            return ''
        line = self.stc.GetLine(self.currentReadLine)
        #print '%s %d : %s' % (repr(self), self.currentReadLine, line)
        self.currentReadLine += 1
        self.pos += len(line)
        if self.currentReadLine < self.startLine:
            return ' ' * (len(line.encode('utf-8')) - 1) + '\n'
        else:
            return line.encode('utf-8')

    def close(self):
        self.length = 0
        
    def getLength(self):
        return self.stc.GetTextLength()

class FileSample:
    """Read only a sample of a file."""
    def __init__(self, filename, start = 0, length = None):
        self.file = open(filename, 'rt')
        self.file.seek(start)
        if length != None:
            self.length = length
        else:
            self.length = os.fstat(self.file.fileno())[stat.ST_SIZE]
    
    def readline(self, bytes):
        if bytes > self.length:
            bytes = self.length
        if bytes <= 0:
            return ''
        line = self.file.readline(bytes)
        self.length -= len(line)
        return line
        
    def close(self):
        self.file.close()
        self.length = 0
    
    def getLength(self):
        return os.fstat(self.file.fileno())[stat.ST_SIZE]


class SourceParser:
    """Class that handles the source tree and dependancies.
    """
    def __init__(self, parser, sourcefile):
        """Constructor. Receive the full path of the source.
           - parser: global anubis parser object
           - sourcefile: filename of the source file
        """
        assert(isinstance(parser, anubis.Parser))
        self._parser = parser
        self._paragraphs = []
        self._tokens = []
        self._source = sourcefile
        self._checksum = 0
        self._modificationDate = 0
        self._isTreeUpToDate = False
        self._areTokensUpToDate = False
        self._stc = None
        
    def SetTextCtrl(self, stc):
        self._stc = stc
        
    def isModified(self):
        newModificationDate = self.GetModificationDate(self._source)
        if self._modificationDate != newModificationDate:
            self._isTreeUpToDate = False
            self._areTokensUpToDate = False
            return True

        f = self.MakeFileObject()
        newChecksum = self.GenCheckSum(f)
        f.close()
        if newChecksum != self._checksum:
            self._isTreeUpToDate = False
            self._areTokensUpToDate = False
            return True
        return False

    def GetModificationDate(self, filename):
        if os.path.exists(filename):
            return os.path.getmtime(filename)
        else:
            return 0.0
        
    def MakeFileObject(self, start = 0, length = None):
        docManager = wx.GetApp().GetDocumentManager()
        f = None
        if self._stc:
            f = FileLike(self._stc, start, length)
            return f

        for doc in docManager.GetDocuments():
            if doc.GetFilename() == self._source:
                f = FileLike(doc.GetFirstView().GetCtrl(), start, length)
                break
        if not f:
            try:
                f = FileSample(self._source, start, length)
            except IOError:
                wx.LogWarning(_('File "%s" doesn\'t exist.') % self._file)
                f = None
        return f
    

    def GenCheckSum(self, f):
        """ Poor man's checksum.  We'll assume most changes will change the length of the file.
        """
        if isinstance(f, file):
            return os.fstat(f.fileno)[stat.ST_SIZE]
        else:
            return f.getLength()

    def GetTokens(self):
        if not self._tokens or not self._areTokensUpToDate or self.isModified():
            self.UpdateAllTokens()
        return self._tokens
        
    def GetParagraphs(self):
        if not self._paragraphs or not self._isTreeUpToDate or self.isModified():
            self.UpdateAllParagraphs()
        return self._paragraphs
    
    def GetGlobalParagraphs(self):
        if not self._paragraphs or not self._isTreeUpToDate or self.isModified():
            self.UpdateAllParagraphs()
        gPars = []
        for par in self._paragraphs:
            if par[0].target == 'OperationDefinition' and par[0][0].names[0] == 'yy__g_operation':
                gPars.append(par)
        return gPars
    
    def GetGlobalParagraphNames(self):
        gPars = self.GetGlobalParagraphs()
        names = []
        for par in gPars:
            names.append(self.RecursivelyExtractOperationDescription(par, nameOnly = True)[2].strip())
        return names
        
    def UpdateAllTokens(self):
        self._modificationDate = self.GetModificationDate(self._source)
        f = self.MakeFileObject()
        self._checksum = self.GenCheckSum(f)
        self._tokens = self.DoReadTokens(f)
        f.close()
        self._areTokensUpToDate = True

    def UpdateAllParagraphs(self):
        self._modificationDate = self.GetModificationDate(self._source)
        f = self.MakeFileObject()
        self._checksum = self.GenCheckSum(f)
        self._paragraphs = self.DoReadParagraphes(f)
        f.close()
        self._isTreeUpToDate = True
        
    def RecurseLookForParagraphs(self, node):
        allPars = []
        for name, val in zip(node.names, node.values):
            if name == 'Par':
                #self._paragraphs.append(val)
                allPars.append(val)
            elif isinstance(val, anubis.BisonNode):
                allPars.extend(self.RecurseLookForParagraphs(val))
            elif name == 'error':
                print 'Parsing error at line %s, col %s : token = "%s"' % (node.pos[0], node.pos[1], val)
            else:
                print 'Parsing error at line %s, col %s : token = "%s"' % (node.pos[0], node.pos[1], val)
        return allPars
        
    def UpdateTokens(self, pos, endPos):
        """ Returns list of new token beetween boundaries.
            Current token array is updated with this list, but the end of the array
            is deleted and not re-computed.
        """
        start = pos
        end = endPos
        startTokenIndex = 0
        lastTokenPos = None
        for index, (token, tokenPos) in enumerate(self._tokens):
            if tokenPos[4] > endPos:
                break
            if token in startParTokens:
                if tokenPos[4] <= pos:
                    start = tokenPos[4]
                    startTokenIndex = index
                elif lastTokenPos and lastTokenPos[5] < pos:
                    startTokenIndex = index
            if tokenPos[5] >= endPos:
                end = tokenPos[5]
                break
            lastTokenPos = tokenPos
            
        ##self._modificationDate = self.GetModificationDate(self._source)
        # Now the tokens
        f = self.MakeFileObject(start, end)
        newTokens = self.DoReadTokens(f)
        self._tokens[startTokenIndex:] = newTokens
        f.close()
        self._areTokensUpToDate = False  ## because ending tokens may have been deleted
        return newTokens
    
    def DoReadTokens(self, file):
        tokens = []
        token = self._parser.getToken(reset = True, file = file, debug = 0, verbose = 0)
        while token:
            pos = self._parser.getTokenPos()
            tokens.append((token, pos))
            token = self._parser.getToken(file = file)
        return tokens

    def DoReadParagraphes(self, file):
        try:
            tree = self._parser.run(verbose = 0, debug = 0, file = file)
        except:
            tree = self._parser.last
        paragraphs = self.RecurseLookForParagraphs(tree)
        return paragraphs
            
    def RecursivelyExtractOperationDescription(self, tree, endToken = 'yy__equals', nameOnly = False):
        """Parse the tree to extract a readable description of this paragraph.
           endToken should be set to a token that ends the description.
        """
        assert(isinstance(tree, BisonNode))
        start = -1
        end = 0
        text = ''
        for name, val in zip(tree.names, tree.values):
            if isinstance(val, BisonNode):
                tmpStart, end, tmpText = self.RecursivelyExtractOperationDescription(val, endToken = endToken, nameOnly = nameOnly)
                if start == -1:
                    start = tmpStart
                if name not in ('OpKW', 'TypeKW', 'Type') and tmpText:
                    text = text + tmpText + ' '
            elif name == 'error':
                text = text + '*** ERROR *** '
            elif name == endToken:
                break
            else:
                #tmpStart, end = tree.pos[4:6]
                tmpStart, end, tmpText = val.split(':', 2)
                if start == -1:
                    start = int(tmpStart)
                end = int(end)
                if not nameOnly or name == 'yy__symbol':
                    text = text + tmpText.decode('utf-8') + ' '
            if text and nameOnly:
                break
        return [start, end, text]


class CodeAssistView(Service.ServiceView):
    """ Basic Service View.
    """
    
    #----------------------------------------------------------------------------
    # Overridden methods
    #----------------------------------------------------------------------------
    def _CreateControl(self, parent, id):
        return wx.html.HtmlWindow(parent, id, style=wx.NO_FULL_REPAINT_ON_RESIZE)
        
    def GetIcon(self):
        return getHelpIcon()

    


class CodeAssistService(Service.Service):

    _trees = {}
    def __init__(self, serviceName, embeddedWindowLocation = wx.lib.pydocview.EMBEDDED_WINDOW_BOTTOMLEFT):
        Service.Service.__init__(self, serviceName, embeddedWindowLocation)
        self._cachePath = sysutils.getAppPath()
        self._parser = anubis.Parser(verbose = 0, keepfiles = 0, debug = 0)

    
    #----------------------------------------------------------------------------
    # Constants
    #----------------------------------------------------------------------------
    SHOW_WINDOW = wx.NewId()  # keep this line for each subclass, need unique ID for each Service


    def InstallControls(self, frame, menuBar = None, toolBar = None, statusBar = None, document = None):
        Service.Service.InstallControls(self, frame, menuBar, toolBar, statusBar, document)
        #viewMenu = menuBar.GetMenu(menuBar.FindMenu(_("&View")))
        #menuItemPos = self.GetMenuItemPos(viewMenu, viewMenu.FindItem(_("&Status Bar"))) + 1

        #viewMenu.InsertCheckItem(menuItemPos, self.SHOW_WINDOW, self.GetMenuString(), self.GetMenuDescr())
        #wx.EVT_MENU(frame, self.SHOW_WINDOW, frame.ProcessEvent)
        #wx.EVT_UPDATE_UI(frame, self.SHOW_WINDOW, frame.ProcessUpdateUIEvent)

        return True



    #----------------------------------------------------------------------------
    # Event Processing Methods
    #----------------------------------------------------------------------------

    def ProcessEvent(self, event):
        if Service.Service.ProcessEvent(self, event):
            return True
        #id = event.GetId()
        #if id == self.SHOW_WINDOW:
            #self.ToggleWindow(event)
            #return True
        #else:
            #return False
        return False


    def ProcessUpdateUIEvent(self, event):
        if Service.Service.ProcessUpdateUIEvent(self, event):
            return True
        #id = event.GetId()
        #if id == self.SHOW_WINDOW:
            #event.Check(self._view != None and self._view.IsShown())
            #event.Enable(True)
            #return True
        #else:
            #return False
        return False

    #----------------------------------------------------------------------------
    # View Methods
    #----------------------------------------------------------------------------

    def _CreateView(self):
        """ This method needs to be overridden with corresponding ServiceView """
        return CodeAssistView(self)


    #----------------------------------------------------------------------------
    # Service Methods
    #----------------------------------------------------------------------------

    def GetSourceParser(self, filename):
        try:
            sourceParser = self._trees[filename]
        except KeyError:
            sourceParser = SourceParser(self._parser, filename)
            self._trees[filename] = sourceParser
        return sourceParser
        
#----------------------------------------------------------------------

from wx import ImageFromStream, BitmapFromImage
from wx import EmptyIcon
import cStringIO, zlib

def getHelpData():
    return zlib.decompress(
'x\xda\x01\x8f\x02p\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x02FIDAT8\x8d\xa5\x93=L\x13a\x18\xc7\x7fWXj\\\
\x9a\xd4^H\xbd\x96\x9e\x03\x87)\xc8Gm\x13\x11\xcb\x00\xa8\x89\x9f(\t\x83q\
\x13GcL\x18t\xd6M\x9c%\x91\xc10\x90h\xf0\x93\xc4P\x12m\x10\x12*\xd5JI\xee\
\x188z-\x95\\c\xe2$e\xea\xeb\xe0\xd1\x82\x0c&\xfa\x8c\xef\xf3\xfc\x7f\xcf\
\xe7+I\xae:\xfe\xc7\xea\xff|XL%\xc5\xaaU\xe6\xabn\x92+\xd8\x004*2\xc7\x9aU\
\x9a\x82nb\xd1\xb8\xb4;^\xda]\xc1\x9b\xc4\xbcx\xf7~\x89X\xbbFk\xb3JH\x91\x11\
\x02r\x1b6\xcb\xbaI\xea\x8b\xc1\x99\x9e\x08\xe7\xfa\xbb\xa4}\x80\xd73\xf3\
\xc2\xb46\xb9\xd8\x7f\x02\xc5\xef#k\x98d\xf5u\x00z\xbb;\x90\xbd\x1e\xf2\xc5\
\x12\xaff\x168\x12l\xe0\xbc\x03\x91$W\x1d\x8b\xa9\xa4x\xfa2\xc3\x9d\x9b\x83\
\x04\xfc>F\xc7\x9e3;\x97F\xf6z\x00\xb0\xbf\xff\xe0\xf6\xf0U\xfa\xba;\xc9\x17\
K<|\xfc\x8c\xeb\x97\xda\x88E\xe3\x92\x0b@\xcf\x95\x89\xb6k(~\x1fk\xd6&\xb3si\
Z\x9aU\xc6\x1f\x8d0>:\x82\xec\xf506\xf1\x16\x01(~\x1f\xc7\xdb5\xf4\\\xb96\
\xc4e\xdd\xe4\xda\x95>*\x02B\xc1\x06\x9e\x8c\x8e\x00P\x01\xb6\xb6\xb6\x11\
\xc0\xc1\x03n\x84\xd3w\xab\xa621\x95\xa8\x01\xac\r\x1b\xe5\xb0\x8c\x00\x84\
\x00\x9fS\xfa\x9a\xb5\xc9\xbd\x07c\x00\xdc\xba1\x88p\x08\x01E\xc6r6T\x8f#\
\x02\xaa\x19\x84\x93yG|\xff\xee0j\xa0\x81\n{\xe3\x00\\\x00AE&\x97\xb7\x11\
\xe27L\x00k\xd67~nms\xe1\xf4I\x1a\x1d\xf1\x8e/\x97\xb7\t(r\r\x10\xd6T\xb2\
\x86Y\xc3\n8\xe4\xf50t\xb9\x97\xa3Z\xa8*\x14\x8e/k\x98\x845u\x17 \xe4f)c`\
\x15KT\x9c\xe1}\xf8\xf8\x99\xc9\x17\xb3\xac\xe8\xebU\xa1\x00\xacb\x89O\x19\
\x83p\xc8]\x03\xc4\xa2q\xe9TDa:\xb1@\xa1X\x02\x01g\xfb\xbb\x00\xe8\xe9\xee\
\x00\x01\x15\x01\x85b\x89\xe9\xc4\x02\xf1\x88R=\xe9=\xa7<95%\x92K\x05:\xdb4\
\xc2M*A\xa7O\xab`\xb3\xb2j\x92\xce\x18\xc4#\nC\x03\x03\xfbOy\xc7\x16SI\x91]/\
\xb3b\x98\xe4\x9dU\x05\x14\x99\xb0\xa6\xd2\x12\xfa\xcbg\xfa\x17\xfb\x05]!\
\xf0\xce_g\xf8\x14\x00\x00\x00\x00IEND\xaeB`\x82\xfb,!\x1e' )

def getHelpBitmap():
    return BitmapFromImage(getHelpImage())

def getHelpImage():
    stream = cStringIO.StringIO(getHelpData())
    return ImageFromStream(stream)

def getHelpIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getHelpBitmap())
    return icon

