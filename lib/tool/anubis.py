#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
PyBison file automatically generated from grammar file anubis.y
You can edit this module, or import it and subclass the Parser class
"""

import sys, os

from bison import BisonParser, BisonNode, BisonError

bisonFile = "anubis.y"  # original bison file
lexFile = "anubis.l"    # original flex file

class Parser(BisonParser):
    """
    bison Parser class generated automatically by bison2py from the
    grammar file "anubis.y" and lex file "anubis.l"

    You may (and probably should) edit the methods in this class.
    You can freely edit the rules (in the method docstrings), the
    tokens list, the start symbol, and the precedences.

    Each time this class is instantiated, a hashing technique in the
    base class detects if you have altered any of the rules. If any
    changes are detected, a new dynamic lib for the parser engine
    will be generated automatically.
    """

    # -------------------------------------------------
    # Default class to use for creating new parse nodes
    # -------------------------------------------------
    defaultNodeClass = BisonNode

    # --------------------------------------------
    # basename of binary parser engine dynamic lib
    # --------------------------------------------
    bisonEngineLibName = "anubis-engine"

    # ----------------------------------------------------------------
    # lexer tokens - these must match those in your lex script (below)
    # ----------------------------------------------------------------
    tokens = ['yy__if', 'yy__is', 'yy__then', 'yy__type', 'yy__operation', 'yy__variable', 'yy__utvar', 
              'yy__float', 'yy__alert', 'yy__theorem', 'yy__p_theorem', 'yy__proof', 'yy__read', 'yy__replaced_by', 
              'yy__exchange', 'yy__symbol', 'yy__Symbol', 'yy__lpar', 'yy__lbracket', 'yy__dot', 
              'yy__rbracket', 'yy__comma', 'yy__bit_width', 'yy__semicolon', 'yy__plus', 'yy__minus',
              'yy__star', 'yy__carret', 'yy__ampersand', 'yy__vbar', 'yy__equals', 'yy__write',
              'yy__percent', 'yy__rbrace', 'yy__lbrace', 'yy__rpar', 'yy__colon', 'yy__implies', 
              'yy__forall', 'yy__exists', 'yy__exists_unique', 'yy__decimal_digit', 'yy__anb_string', 
              'yy__type_String', 'yy__type_ByteArray', 'yy__description', 'yy__arrow', 'yy__ndarrow', 
              'yy__with', 'yy__type_Int32', 'yy__type_Float', 'yy__type_Listener', 'yy__indirect', 
              'yy__serialize', 'yy__unserialize', 'yy__vcopy', 'yy__non_equal', 'yy__load_module', 
              'yy__wait_for', 'yy__delegate', 'yy__tilde', 'yy__type_Omega', 'yy__type_Proof',
              'yy__checking_every', 'yy__rpar_arrow', 'yy__rpar_ndarrow', 'yy__dots', 'yy__g_operation', 
              'yy__less', 'yy__greater', 'yy__lessoreq', 'yy__greateroreq', 'yy__mod', 'yy__slash', 
              'yy__else', 'yy__RAddr', 'yy__WAddr', 'yy__RWAddr', 'yy__succeeds', 'yy__succeeds_as', 
              'yy__connect_to_file', 'yy__C_constr_for', 'yy__connect_to_IP', 'yy__GAddr', 'yy__Var',
              'yy__MVar', 'yy__StructPtr', 'yy__debug_avm', 'yy__terminal', 'yy__avm', 'yy__left_shift',
              'yy__right_shift', 'yy__since', 'yy__p_operation', 'yy__p_type', 'yy__p_variable', 
              'yy__protect', 'yy__lock', 'yy__alt_number', 'yy__config_file', 'yy__verbose', 
              'yy__stop_after', 'yy__mapsto', 'yy__rec_mapsto', 'yy__language', 'yy__mapstoo', 
              'yy__rec_mapstoo', 'yy__arroww', 'yy__we_have', 'yy__enough', 'yy__let', 'yy__assume',
              'yy__indeed', 'yy__hence', 'yy__integer', 'yy__char', 'yy__comment']

    # ------------------------------
    # precedences
    # ------------------------------
    precedences = (
        ("right", ['yy__protect', 'yy__lock'],),
        ("right", ['yy__debug_avm', 'yy__terminal'],),
        ("right", ['yy__assume', 'yy__let', 'yy__enough', 'yy__we_have', 'yy__hence'],),
        ("right", ['yy__comma'],),
        ("right", ['yy__mapsto', 'yy__rec_mapsto'],),
        ("right", ['yy__colon'],),
        ("right", ['yy__is', 'yy__with'],),
        ("right", ['yy__then', 'yy__else'],),
        ("right", ['yy__semicolon'],),
        ("right", ['prec_symbol', 'yy__symbol'],),
        ("right", ['yy__rpar'],),
        ("right", ['prec_par_term'],),
        ("right", ['prec_of_type'],),
        ("right", ['prec_sym_type'],),
        ("right", ['yy__vbar'],),
        ("right", ['yy__ampersand'],),
        ("right", ['yy__implies', 'yy__left_shift', 'yy__right_shift'],),
        ("right", ['yy__tilde'],),
        ("right", ['yy__less', 'yy__greater', 'yy__lessoreq', 'yy__greateroreq', 'yy__equals', 'yy__write', 'yy__non_equal', 'yy__exchange'],),
        ("right", ['yy__connect_to_file', 'yy__connect_to_IP'],),
        ("right", ['yy__mod'],),
        ("right", ['yy__plus'],),
        ("left", ['yy__minus'],),
        ("right", ['yy__star'],),
        ("right", ['yy__percent'],),
        ("left", ['yy__slash', 'yy__dot'],),
        ("right", ['yy__carret'],),
        ("right", ['unaryminus'],),
        ("right", ['yy__arrow', 'yy__ndarrow', 'yy__rpar_arrow', 'yy__rpar_ndarrow'],),
        ("right", ['yy__lpar', 'yy__lbrace', 'yy__lbracket'],),
        )

    # ---------------------------------------------------------------
    # Declare the start target here (by name)
    # ---------------------------------------------------------------
    start = "Start"

    # ---------------------------------------------------------------
    # These methods are the python handlers for the bison targets.
    # (which get called by the bison code each time the corresponding
    # parse target is unambiguously reached)
    #
    # WARNING - don't touch the method docstrings unless you know what
    # you are doing - they are in bison rule syntax, and are passed
    # verbatim to bison to build the parser engine library.
    # ---------------------------------------------------------------

    def __init__(self, **kw):
        oldCwd = os.getcwd()
        if os.path.isabs(sys.argv[0]):
            os.chdir(os.path.dirname(sys.argv[0]))
        else:
            os.chdir(os.environ['PATH'].split(';')[0])
        BisonParser.__init__(self, **kw)
        os.chdir(oldCwd)

    def on_Start(self, target, option, pos, names, values):
        """
        Start
            : Text
        """
        return self.defaultNodeClass(
            target="Start",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Text(self, target, option, pos, names, values):
        """
        Text
            : Paragraphs
        """
        return self.defaultNodeClass(
            target="Text",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Paragraphs(self, target, option, pos, names, values):
        """
        Paragraphs
            : | Paragraphs Paragraph
        """
        return self.defaultNodeClass(
            target="Paragraphs",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Paragraph(self, target, option, pos, names, values):
        """
        Paragraph
            : Par
        """
        return self.defaultNodeClass(
            target="Paragraph",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Par(self, target, option, pos, names, values):
        """
        Par
            : TypeDefinition
            | OperationDefinition
            | OperationDeclaration
            | VariableDeclaration
            | Theorem
            | C_constr
            | yy__read yy__anb_string
            | yy__replaced_by yy__anb_string
            | error yy__dot
        """
        return self.defaultNodeClass(
            target="Par",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_C_constr(self, target, option, pos, names, values):
        """
        C_constr
            : yy__C_constr_for yy__Symbol yy__equals Type yy__dot
        """
        return self.defaultNodeClass(
            target="C_constr",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_OpKW(self, target, option, pos, names, values):
        """
        OpKW
            : yy__operation
            | yy__p_operation
            | yy__g_operation
        """
        return self.defaultNodeClass(
            target="OpKW",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_ThKW(self, target, option, pos, names, values):
        """
        ThKW
            : yy__theorem
            | yy__p_theorem
        """
        return self.defaultNodeClass(
            target="ThKW",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_VarKW(self, target, option, pos, names, values):
        """
        VarKW
            : yy__variable
            | yy__p_variable
        """
        return self.defaultNodeClass(
            target="VarKW",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_TypeKW(self, target, option, pos, names, values):
        """
        TypeKW
            : yy__type
            | yy__p_type
        """
        return self.defaultNodeClass(
            target="TypeKW",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_TypeDefinition(self, target, option, pos, names, values):
        """
        TypeDefinition
            : TypeKW yy__Symbol yy__equals Type yy__dot
            | TypeKW yy__Symbol yy__colon Alternatives
            | TypeKW yy__Symbol yy__lpar TypeVars1 yy__rpar yy__colon Alternatives
        """
        return self.defaultNodeClass(
            target="TypeDefinition",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_TypeVars1(self, target, option, pos, names, values):
        """
        TypeVars1
            : yy__utvar
            | yy__utvar yy__comma TypeVars1
        """
        return self.defaultNodeClass(
            target="TypeVars1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_SymbolOrDecimalDigit(self, target, option, pos, names, values):
        """
        SymbolOrDecimalDigit
            : yy__symbol
            | yy__decimal_digit
        """
        return self.defaultNodeClass(
            target="SymbolOrDecimalDigit",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Alternatives(self, target, option, pos, names, values):
        """
        Alternatives
            : yy__dot
            | yy__dots
            | Alternatives1
        """
        return self.defaultNodeClass(
            target="Alternatives",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Alternatives1(self, target, option, pos, names, values):
        """
        Alternatives1
            : Alternative yy__dot
            | Alternative yy__comma yy__dots
            | Alternative yy__comma Alternatives1
        """
        return self.defaultNodeClass(
            target="Alternatives1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Alternative(self, target, option, pos, names, values):
        """
        Alternative
            : yy__symbol
            | yy__decimal_digit
            | yy__lbracket yy__rbracket
            | yy__symbol yy__lpar AltOperands1 yy__rpar
            | yy__lbracket AltOperand yy__dot AltOperand yy__rbracket
            | AltOperand yy__plus AltOperand
            | AltOperand yy__star AltOperand
            | AltOperand yy__percent AltOperand
            | AltOperand yy__carret AltOperand
            | AltOperand yy__vbar AltOperand
            | AltOperand yy__ampersand AltOperand
            | AltOperand yy__arrow AltOperand
            | AltOperand yy__equals AltOperand
            | AltOperand yy__implies AltOperand
            | AltOperand yy__left_shift AltOperand
            | AltOperand yy__right_shift AltOperand
            | AltOperand yy__minus AltOperand
            | AltOperand yy__slash AltOperand
            | AltOperand yy__mod AltOperand yy__rpar
            | AltOperand yy__less AltOperand
            | AltOperand yy__non_equal AltOperand
            | AltOperand yy__lessoreq AltOperand
            | yy__tilde AltOperand
            | yy__star AltOperand
            | error
        """
        return self.defaultNodeClass(
            target="Alternative",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_AltOperands1(self, target, option, pos, names, values):
        """
        AltOperands1
            : AltOperand
            | AltOperand yy__comma AltOperands1
        """
        return self.defaultNodeClass(
            target="AltOperands1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_AltOperand(self, target, option, pos, names, values):
        """
        AltOperand
            : Type yy__symbol
            | Type %prec yy__comma
        """
        return self.defaultNodeClass(
            target="AltOperand",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Type(self, target, option, pos, names, values):
        """
        Type
            : yy__lpar Type yy__rpar
            | yy__Symbol %prec prec_sym_type
            | yy__utvar
            | yy__type_String
            | yy__type_ByteArray
            | yy__type_Int32
            | yy__type_Proof yy__lpar Term yy__rpar
            | yy__type_Float
            | yy__type_Listener
            | yy__RAddr yy__lpar Type yy__rpar
            | yy__WAddr yy__lpar Type yy__rpar
            | yy__RWAddr yy__lpar Type yy__rpar
            | yy__GAddr yy__lpar Type yy__rpar
            | yy__Var yy__lpar Type yy__rpar
            | yy__MVar yy__lpar Type yy__rpar
            | yy__Symbol yy__lpar Types1 yy__rpar
            | yy__StructPtr yy__lpar yy__Symbol yy__rpar
            | Type yy__arrow Type
            | yy__Symbol yy__lpar Types1 yy__rpar_arrow Type
            | yy__lpar TypesArgs1 yy__rpar_arrow Type
            | yy__lpar Types2 yy__rpar
            | yy__lbrace Type yy__rbrace
        """
        return self.defaultNodeClass(
            target="Type",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Types1(self, target, option, pos, names, values):
        """
        Types1
            : Type
            | Type yy__comma Types1
        """
        return self.defaultNodeClass(
            target="Types1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_TypesArgs1(self, target, option, pos, names, values):
        """
        TypesArgs1
            : Type
            | Type yy__symbol
            | Type yy__comma TypesArgs1
            | Type yy__symbol yy__comma TypesArgs1
        """
        return self.defaultNodeClass(
            target="TypesArgs1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Types2(self, target, option, pos, names, values):
        """
        Types2
            : Type yy__comma Type
            | Type yy__comma Types2
        """
        return self.defaultNodeClass(
            target="Types2",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_VariableDeclaration(self, target, option, pos, names, values):
        """
        VariableDeclaration
            : VarKW Type yy__symbol yy__equals Term yy__dot
        """
        return self.defaultNodeClass(
            target="VariableDeclaration",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_OperationDefinition(self, target, option, pos, names, values):
        """
        OperationDefinition
            : OpKW Type SymbolOrDecimalDigit yy__equals Term yy__dot
            | OpKW Type yy__symbol yy__lpar OpArgs yy__rpar yy__equals Term yy__dot
            | OpKW Type OpArg SimpleBinaryOp OpArg yy__equals Term yy__dot
            | OpKW Type SimpleUnaryOp OpArg yy__equals Term yy__dot
            | OpKW Type OpArg yy__mod OpArg yy__rpar yy__equals Term yy__dot
            | OpKW Type OpArg SimpleBinaryOp OpArg yy__dot
            | OpKW Type SimpleUnaryOp OpArg yy__dot
            | OpKW Type OpArg yy__mod OpArg yy__rpar yy__dot
        """
        return self.defaultNodeClass(
            target="OperationDefinition",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_OperationDeclaration(self, target, option, pos, names, values):
        """
        OperationDeclaration
            : OpKW Type yy__symbol yy__lpar OpArgs yy__rpar yy__dot
            | OpKW Type yy__symbol yy__dot
        """
        return self.defaultNodeClass(
            target="OperationDeclaration",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_SimpleBinaryOp(self, target, option, pos, names, values):
        """
        SimpleBinaryOp
            : yy__plus
            | yy__star
            | yy__percent
            | yy__carret
            | yy__vbar
            | yy__ampersand
            | yy__arrow
            | yy__equals
            | yy__implies
            | yy__left_shift
            | yy__right_shift
            | yy__minus
            | yy__slash
            | yy__less
            | yy__non_equal
            | yy__lessoreq
        """
        return self.defaultNodeClass(
            target="SimpleBinaryOp",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_SimpleUnaryOp(self, target, option, pos, names, values):
        """
        SimpleUnaryOp
            : yy__minus
            | yy__tilde
        """
        return self.defaultNodeClass(
            target="SimpleUnaryOp",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_OpArgs(self, target, option, pos, names, values):
        """
        OpArgs
            : | OpArgs1
        """
        return self.defaultNodeClass(
            target="OpArgs",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_OpArgs1(self, target, option, pos, names, values):
        """
        OpArgs1
            : OpArg
            | OpArg yy__comma OpArgs1
        """
        return self.defaultNodeClass(
            target="OpArgs1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_OpArg(self, target, option, pos, names, values):
        """
        OpArg
            : Type yy__symbol
            | error
        """
        return self.defaultNodeClass(
            target="OpArg",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_FArgs1(self, target, option, pos, names, values):
        """
        FArgs1
            : FArg
            | FArg yy__comma FArgs1
        """
        return self.defaultNodeClass(
            target="FArgs1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_FArg(self, target, option, pos, names, values):
        """
        FArg
            : Type yy__symbol
        """
        return self.defaultNodeClass(
            target="FArg",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Term(self, target, option, pos, names, values):
        """
        Term
            : yy__alert
            | yy__alt_number yy__lpar Term yy__rpar
            | yy__protect Term
            | yy__lock Term yy__comma Term
            | yy__debug_avm Term
            | yy__avm yy__lbrace AVM yy__rbrace
            | yy__terminal Term
            | yy__symbol %prec prec_symbol
            | yy__lpar yy__rpar %prec prec_symbol
            | yy__integer %prec prec_symbol
            | yy__char %prec prec_symbol
            | yy__float %prec prec_symbol
            | yy__lpar Term yy__rpar %prec prec_par_term
            | yy__lpar Terms2 yy__rpar %prec prec_par_term
            | yy__lpar Type yy__rpar Term %prec prec_of_type
            | yy__lpar yy__colon Term yy__rpar Term %prec prec_of_type
            | yy__lpar yy__colon Type yy__rpar Term %prec prec_of_type
            | AppTerm
            | Conditional
            | yy__lbracket List
            | yy__star Term
            | Term yy__write Term
            | Term yy__exchange Term
            | Term yy__semicolon Term
            | yy__load_module yy__lpar Term yy__rpar
            | yy__serialize yy__lpar Term yy__rpar
            | yy__unserialize yy__lpar Term yy__rpar
            | yy__bit_width yy__lpar Type yy__rpar
            | yy__indirect yy__lpar Type yy__rpar
            | yy__vcopy yy__lpar Term yy__comma Term yy__rpar
            | yy__lpar Type yy__rpar yy__connect_to_file Term
            | yy__lpar Type yy__rpar yy__connect_to_IP Term yy__colon Term
            | yy__anb_string
            | yy__with yy__symbol yy__equals Term yy__comma WithTerm
            | yy__checking_every Term yy__milliseconds yy__comma yy__wait_for Term yy__then Term
            | yy__delegate Term yy__comma Term
            | yy__lbrace yy__symbol yy__colon Type yy__comma Term yy__rbrace
            | yy__lpar FArgs1 yy__rpar yy__mapsto Term
            | yy__lpar FArgs1 yy__rpar yy__rec_mapsto Term
            | yy__forall yy__symbol yy__colon Type yy__comma Term
            | yy__forall yy__symbol yy__colon Term yy__comma Term
            | yy__exists yy__symbol yy__colon Type yy__comma Term
            | yy__exists_unique yy__symbol yy__colon Type yy__comma Term
            | yy__description yy__symbol yy__colon Type yy__comma Term
            | yy__we_have Term yy__dot Justif Term %prec yy__we_have
            | yy__enough Term yy__dot Justif Term %prec yy__enough
            | yy__assume Term yy__dot Term %prec yy__assume
            | yy__let yy__symbol yy__colon Term yy__dot Term %prec yy__let
            | Hence Term %prec yy__hence
            | error
        """
        return self.defaultNodeClass(
            target="Term",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Terms2(self, target, option, pos, names, values):
        """
        Terms2
            : Term yy__comma Term
            | Term yy__comma Terms2
        """
        return self.defaultNodeClass(
            target="Terms2",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_WithTerm(self, target, option, pos, names, values):
        """
        WithTerm
            : Term %prec yy__comma
            | yy__symbol yy__equals Term yy__comma WithTerm
        """
        return self.defaultNodeClass(
            target="WithTerm",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_List(self, target, option, pos, names, values):
        """
        List
            : yy__rbracket
            | Term yy__rbracket
            | Term yy__comma List
            | Term yy__dot Term yy__rbracket
        """
        return self.defaultNodeClass(
            target="List",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_AppTerm(self, target, option, pos, names, values):
        """
        AppTerm
            : Term yy__lpar Terms yy__rpar
            | Term yy__lbracket List
            | Term yy__plus Term
            | Term yy__star Term
            | Term yy__percent Term
            | Term yy__carret Term
            | Term yy__vbar Term
            | Term yy__ampersand Term
            | Term yy__arrow Term
            | Term yy__equals Term
            | Term yy__implies Term
            | Term yy__left_shift Term
            | Term yy__right_shift Term
            | Term yy__minus Term
            | Term yy__slash Term
            | Term yy__less Term
            | Term yy__non_equal Term
            | Term yy__greater Term
            | Term yy__lessoreq Term
            | Term yy__greateroreq Term
            | Term yy__mod Term yy__rpar
            | yy__minus Term %prec unaryminus
            | yy__tilde Term
        """
        return self.defaultNodeClass(
            target="AppTerm",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Terms(self, target, option, pos, names, values):
        """
        Terms
            : | Terms1
        """
        return self.defaultNodeClass(
            target="Terms",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Terms1(self, target, option, pos, names, values):
        """
        Terms1
            : Term
            | Term yy__comma Terms1
        """
        return self.defaultNodeClass(
            target="Terms1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Conditional(self, target, option, pos, names, values):
        """
        Conditional
            : yy__if Term yy__is yy__lbrace Clauses yy__rbrace
            | yy__if Term yy__succeeds_as yy__symbol yy__then Term
            | yy__if Term yy__succeeds yy__then Term
            | yy__if Term yy__is Clause
            | yy__since Term yy__is Head yy__comma Term
            | yy__if Term yy__then Term yy__else Term
            | yy__if Term yy__is Clause yy__else Term
        """
        return self.defaultNodeClass(
            target="Conditional",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Clauses(self, target, option, pos, names, values):
        """
        Clauses
            : | Clause Clauses
            | Clause yy__comma Clauses
        """
        return self.defaultNodeClass(
            target="Clauses",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Clause(self, target, option, pos, names, values):
        """
        Clause
            : Head yy__then Term
        """
        return self.defaultNodeClass(
            target="Clause",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Head(self, target, option, pos, names, values):
        """
        Head
            : yy__symbol
            | yy__decimal_digit
            | yy__lbracket yy__rbracket
            | yy__symbol yy__lpar yy__rpar
            | yy__symbol yy__lpar ResurSym1 yy__rpar
            | yy__lbracket ResurSym yy__dot ResurSym yy__rbracket
            | ResurSym yy__plus ResurSym
            | ResurSym yy__star ResurSym
            | ResurSym yy__percent ResurSym
            | ResurSym yy__carret ResurSym
            | ResurSym yy__vbar ResurSym
            | ResurSym yy__ampersand ResurSym
            | ResurSym yy__arrow ResurSym
            | ResurSym yy__equals ResurSym
            | ResurSym yy__implies ResurSym
            | ResurSym yy__left_shift ResurSym
            | ResurSym yy__right_shift ResurSym
            | ResurSym yy__minus ResurSym
            | ResurSym yy__slash ResurSym
            | ResurSym yy__mod ResurSym yy__rpar
            | ResurSym yy__less ResurSym
            | ResurSym yy__non_equal ResurSym
            | ResurSym yy__lessoreq ResurSym
            | yy__tilde ResurSym
            | yy__lpar ResurSym2 yy__rpar
        """
        return self.defaultNodeClass(
            target="Head",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_ResurSym(self, target, option, pos, names, values):
        """
        ResurSym
            : yy__symbol
            | Type yy__symbol
        """
        return self.defaultNodeClass(
            target="ResurSym",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_ResurSym1(self, target, option, pos, names, values):
        """
        ResurSym1
            : ResurSym
            | ResurSym yy__comma ResurSym1
        """
        return self.defaultNodeClass(
            target="ResurSym1",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_ResurSym2(self, target, option, pos, names, values):
        """
        ResurSym2
            : ResurSym yy__comma ResurSym
            | ResurSym yy__comma ResurSym2
        """
        return self.defaultNodeClass(
            target="ResurSym2",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_AVM(self, target, option, pos, names, values):
        """
        AVM
            : | AVM_Instr AVM
        """
        return self.defaultNodeClass(
            target="AVM",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_AVM_Instr(self, target, option, pos, names, values):
        """
        AVM_Instr
            : yy__symbol
            | yy__lpar yy__symbol IntMCons
        """
        return self.defaultNodeClass(
            target="AVM_Instr",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_IntMCons(self, target, option, pos, names, values):
        """
        IntMCons
            : yy__rpar
            | yy__dot yy__integer yy__rpar
            | yy__integer IntMCons
            | yy__anb_string IntMCons
        """
        return self.defaultNodeClass(
            target="IntMCons",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_yy__milliseconds(self, target, option, pos, names, values):
        """
        yy__milliseconds
            : yy__symbol
        """
        return self.defaultNodeClass(
            target="yy__milliseconds",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Theorem(self, target, option, pos, names, values):
        """
        Theorem
            : ThKW yy__symbol yy__colon Term yy__dot yy__proof yy__colon Term yy__dot
        """
        return self.defaultNodeClass(
            target="Theorem",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Hence(self, target, option, pos, names, values):
        """
        Hence
            : yy__hence
            | yy__hence yy__comma
        """
        return self.defaultNodeClass(
            target="Hence",
            option=option,
            pos=pos,
            names=names,
            values=values)

    def on_Justif(self, target, option, pos, names, values):
        """
        Justif
            : | yy__indeed Term yy__dot
        """
        return self.defaultNodeClass(
            target="Justif",
            option=option,
            pos=pos,
            names=names,
            values=values)

    # -----------------------------------------
    # raw lex script, verbatim here
    # -----------------------------------------
    lexscript = r"""
/* lexer.y *******************************************************************************

                                             Anubis 1. 
                                    The lexer of the compiler. 

*****************************************************************************************/

/*
    This LEX/FLEX file is the lexer for the Anubis compiler. 
*/ 


%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Python.h"
typedef struct _YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
  int start_pos;
  int end_pos;
} _YYLTYPE;
#define YYLTYPE _YYLTYPE
#define YYSTYPE void *
#include "tokens.h"
extern void *py_parser;
extern void (*py_input)(PyObject *parser, char *buf, int *result, int max_size);
#define returntoken(tok) \
    *pyylval = PyString_FromFormat("%d:%d:%s", tokenStart, yyoffset, yytext); \
    pyylloc->first_line = linenoStart; \
    pyylloc->first_column = colnoStart; \
    pyylloc->last_line = lineno; \
    pyylloc->last_column = colno; \
    pyylloc->start_pos = tokenStart; \
    pyylloc->end_pos = yyoffset; \
    tokenStart = yyoffset; \
    return (tok)

#define YY_INPUT(buf,result,max_size) {(*py_input)(py_parser, buf, &result, max_size);}


//#include "compil.h"

#define line_in(x)       (((x)>>9)&0x7FFF)
#define col_in(x)        (((x)>>1)&0xFF)
#define new_integer(n)           ((n)<<1)

#define errfile stderr

int current_par_line = 0;      /* line number of first line of paragraph currently read */ 
int linenoStart = 0;
int colnoStart = 0;
extern int yyoffset;
extern int tokenStart;
extern int lineno;
extern int colno; 

   
/* Constructing the lexer */   
//int yylex(void);        /* this  is the  function called  by yyparse  (it is  defined just
//                           below) */ 

int linecol(void);
void updateTokenStart();
   
   
   
/* YY_PROTO(( void )) */

   
   
/* Managing character  strings. We  use the  buffer 'str_buf' for  reading the  content of
   character strings. */ 
#define str_buf_size (10000)
int str_index = 0; 
char str_buf[str_buf_size]; 

/* This function is  used for storing one  character into the buffer. It  is defined after
   the lexer proper, because it uses the definition of the state 'STRTL'. */ 
void store_str_char(char);

   
   

   
/* When we read a new file, we must push into stacks a lot of informations. First of all a
   YY_BUFFER_STATE, because FLEX  needs to be able  to return to the previous  file at the
   right place. We  must push the path of  the file currently being read,  and the current
   line number in the previous file. The  current directory is managed by another stack in
   'mallocz.c'.  */ 
   
   
/* the number of nested includes is limited to 100. */    
//#define                  max_include      (100)
      
//YY_BUFFER_STATE      include_stack[max_include];   /* buffer states for FLEX */ 
//char *               include_names[max_include];   /* absolute paths of files */ 
//int                  include_lines[max_include];   /* current line number in files */ 
//char *               include_dir[max_include];     /* current directory */ 

//int                  include_stack_ptr = 0;        /* common stack pointer */

/* The 4 stacks: 'include_stack'
                 'include_lines'
                 'include_names'
                 'include_dir' 
   
   are growing in parallel. The common stack pointer is 'include_stack_ptr'. */    
   
//YY_BUFFER_STATE new_yy_buffer; 



 

void err_line_col(int lc)
{
	fprintf(errfile, "ERROR in Line %d, Col %d, token '%s'\n", line_in(lc), col_in(lc), yytext); 
}



   
   
   
//#define YY_NO_UNPUT


/* counting lines and columns -------------------------------------------------*/
int comlevel = 0;
int bracelevel = 0;
int bracketlevel = 0;
int tab_seen = 0; 
int tab_width = 4; 


/* updating line and column counters ---------------------------------------------*/
void update_lc(void)
{
  int i = 0;
  linenoStart = lineno;
  colnoStart = colno;
  //fprintf(errfile, "STATE %d %d %d --> '%s'\n", YY_START, bracelevel, bracketlevel, yytext);
  while (yytext[i] != 0)
  {
    if (yytext[i] == '\t') { tab_seen = 1; colno += tab_width; }
    if (yytext[i] == '\n') { lineno++; colno = 1; }
    else                   colno++; 
    i++; 
  }
  yyoffset += i;
}

#define YY_USER_ACTION      	update_lc(); 


#define YY_DECL int yylex ( YYSTYPE * pyylval, YYLTYPE * pyylloc )

/* return current line, column and file id into an expression integer -------------*/ 
int linecol(void)
{
  return new_integer((lineno<<8)|colno); 
}

void lexical_error(void)
{
  err_line_col(linecol()); 
}

void lexical_kwread_error(void)
{
	lexical_error();
}


%}

%option noyywrap



/* states ------------------------------------------------------------------------------*/
%x COM
%x PAR
%x INCL
%x STR
%x STRTL
   
/* state STRTL is used when a string is too long. */ 

/* the lexer ----------------------------------------------------------------------------*/
%%
<PAR>"/*"                         { comlevel = 1; BEGIN COM; }
<COM>"/*"                         { comlevel++; }
<COM>"*/"                         { comlevel--; if (!comlevel) { BEGIN PAR; } }
<COM>\n                           { }
<COM>.                            { }
<PAR>\/\/.*$                      { }
<PAR>\"                           { BEGIN STR; updateTokenStart(); tokenStart -= 1; colno -= 1; str_index = 0; }
<STR>\"                           { BEGIN PAR; 
                                    str_buf[str_index] = 0; 
                                    yytext = str_buf;
                                    returntoken( yy__anb_string ); }
<STR>\\\"                         { store_str_char('\"'); }
<STR>\\n                          { store_str_char('\n'); }
<STR>\\r                          { store_str_char('\r'); }
<STR>\\t                          { store_str_char('\t'); }
<STR>\\\\                         { store_str_char('\\'); }
<STR>\n                           { store_str_char('\n'); }
<STR>.                            { store_str_char(yytext[0]); }
<STRTL>\"                         { BEGIN PAR; returntoken( yy__anb_string );}
<STRTL>\n                         { }
<STRTL>.                          { }
<PAR>\'[^\'\n\t\r\"]\'            { returntoken( yy__char ); }
<PAR>\'\\n\'                      { returntoken( yy__char ); }
<PAR>\'\\r\'                      { returntoken( yy__char ); }
<PAR>\'\\t\'                      { returntoken( yy__char ); }
<PAR>\'\\\"\'                     { returntoken( yy__char ); }
<PAR>\'\\\\\'                     { returntoken( yy__char ); }
<PAR>\'\\\'\'                     { returntoken( yy__char ); }
<PAR>"("                               { bracketlevel++; returntoken( yy__lpar ); }
<PAR>")"                               { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rpar ); }
                                         else lexical_error(); }
<PAR>\,[\ \t\r\n]*\)                   { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rpar ); }
                                         else lexical_error(); }
<PAR>"{"                               { bracelevel++; returntoken( yy__lbrace ); }
<PAR>"}"                               { if(bracelevel > 0) { bracelevel--; returntoken( yy__rbrace ); } 
                                         else lexical_error(); }
<PAR>\,[\ \t\r\n]*\}                   { if(bracelevel > 0) { bracelevel--; returntoken( yy__rbrace ); }
                                         else lexical_error(); }
<PAR>"["                               { bracketlevel++; returntoken( yy__lbracket ); }
<PAR>"]"                               { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rbracket ); }
                                         else lexical_error(); }
<PAR>\,[\t\n\r\ ]*\]                   { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rbracket ); }
                                         else lexical_error(); }
<PAR>":"                               { returntoken( yy__colon ); }
<PAR>";"                               { returntoken( yy__semicolon ); }
<PAR>"-"                               { returntoken( yy__minus ); }
<PAR>"."                               { if(bracketlevel == 0 && bracelevel == 0) BEGIN(INITIAL); 
                                         returntoken( yy__dot ); }
<PAR>\%                                { returntoken( yy__percent ); }
<PAR>","                               { returntoken( yy__comma ); }
<PAR>"~"                               { returntoken( yy__tilde ); }
<PAR>"="                               { returntoken( yy__equals ); }
<PAR>"/="                              { returntoken( yy__non_equal ); }
<PAR>"+"                               { returntoken( yy__plus ); }
<PAR>"*"                               { returntoken( yy__star ); }
<PAR>"^"                               { returntoken( yy__carret ); }
<PAR>"\&"                              { returntoken( yy__ampersand ); }
<PAR>"|"                               { returntoken( yy__vbar ); }
<PAR>">>"                              { returntoken( yy__right_shift ); }
<PAR>"<<"                              { returntoken( yy__left_shift ); }
<PAR>"=>"                              { returntoken( yy__implies ); }
<PAR>"?"                               { returntoken( yy__forall ); }
<PAR>"!"                               { returntoken( yy__exists ); }
<PAR>"!!"                              { returntoken( yy__exists_unique ); }
<PAR>"#!"                              { returntoken( yy__description ); }
<PAR>"/"                               { returntoken( yy__slash ); }
<PAR>\([\t ]*mod[\t ]                  { bracketlevel++; returntoken( yy__mod ); }
<PAR>"<"                               { returntoken( yy__less ); }
<PAR>">"                               { returntoken( yy__greater ); }
<PAR>"=<"                              { returntoken( yy__lessoreq ); }
<PAR>">="                              { returntoken( yy__greateroreq ); }
<PAR>"<-"                              { returntoken( yy__write ); }
<PAR>"<->"                             { returntoken( yy__exchange ); }
<PAR>"->"                              { returntoken( yy__arrow ); }
<PAR>"->>"                             { returntoken( yy__arroww ); }
<PAR>"|->"                             { returntoken( yy__mapsto ); }
<PAR>"|->>"                            { returntoken( yy__mapstoo ); }
<PAR>"..."                             { if(bracketlevel == 0 && bracelevel == 0) BEGIN(INITIAL); 
                                         returntoken( yy__dots ); }
<PAR>\)[ \t\n]*\-\>                    { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rpar_arrow ); }
                                         else lexical_error(); }
<PAR>String                            { returntoken( yy__type_String ); }
<PAR>ByteArray                         { returntoken( yy__type_ByteArray ); }
<PAR>Int32                             { returntoken( yy__type_Int32 ); }
<PAR>Omega                             { returntoken( yy__type_Omega ); }
<PAR>bit_width                         { returntoken( yy__bit_width ); }
<PAR>is_indirect_type                  { returntoken( yy__indirect ); }
<PAR>\£vcopy                           { returntoken( yy__vcopy ); }
<PAR>\£load_module                     { returntoken( yy__load_module ); }
<PAR>\£serialize                       { returntoken( yy__serialize ); }
<PAR>\£unserialize                     { returntoken( yy__unserialize ); }
<PAR>Float                             { returntoken( yy__type_Float ); }
<PAR>\£Listener                        { returntoken( yy__type_Listener ); }
<PAR>\£StructPtr                       { returntoken( yy__StructPtr ); }
<PAR>\£avm                             { returntoken( yy__avm ); }
<PAR>if                                { returntoken( yy__if ); }
<PAR>^[Pp]roof                         { returntoken( yy__proof ); }
<PAR>Proof                             { returntoken( yy__type_Proof ); }
<PAR>since                             { returntoken( yy__since ); }
<PAR>is                                { returntoken( yy__is ); }
<PAR>then                              { returntoken( yy__then ); }
<PAR>alert                             { returntoken( yy__alert ); }
<PAR>protect                           { returntoken( yy__protect ); }
<PAR>lock                              { returntoken( yy__lock ); }
<PAR>succeeds                          { returntoken( yy__succeeds ); }
<PAR>succeeds[\ \t\n]+as               { returntoken( yy__succeeds_as ); }
<PAR>wait[\ \t\n]+for                  { returntoken( yy__wait_for ); }
<PAR>checking[\ \t\n]+every            { returntoken( yy__checking_every ); }
<PAR>delegate                          { returntoken( yy__delegate ); }
<PAR>else                              { returntoken( yy__else ); }
<PAR>with                              { returntoken( yy__with ); }
<PAR>alternative_number                { returntoken( yy__alt_number ); }
<PAR>RAddr                             { returntoken( yy__RAddr ); }
<PAR>WAddr                             { returntoken( yy__WAddr ); }
<PAR>RWAddr                            { returntoken( yy__RWAddr ); }
<PAR>GAddr                             { returntoken( yy__GAddr ); }
<PAR>Var                               { returntoken( yy__Var ); }
<PAR>MVar                              { returntoken( yy__MVar ); }
<PAR>connect[\ \t\n]+to[\ \t\n]+file    { returntoken( yy__connect_to_file ); }
<PAR>connect[\ \t\n]+to[\ \t\n]+network { returntoken( yy__connect_to_IP ); }
<PAR>debug                             { returntoken( yy__debug_avm ); }
<PAR>terminal                          { returntoken( yy__terminal ); }
<PAR>[Ww]e[\ \t\n]+have                { returntoken( yy__we_have ); }
<PAR>[Ii]t[\ \t\n]+is[\ \t\n]+enough[\ \t\n]+to[\ \t\n]+prove([\ \t\n]+that)? { 
                                         returntoken( yy__enough ); } 
<PAR>[Ll]et                            { returntoken( yy__let ); } 
<PAR>[Aa]ssume([\ \t\n]+that)?         { returntoken( yy__assume ); }
<PAR>[Hh]ence                          { returntoken( yy__hence ); } 
<PAR>[Ii]ndeed                         { returntoken( yy__indeed ); } 
^to[\ \t]+do\:.*\n             { printf(yytext); fflush(stdout); }
^[Tt]ype                       { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__type ); }
^[Pp]ublic[\ \t\n]+type        { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__p_type ); }
^[Vv]ariable                   { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__variable ); }
^[Pp]ublic[\ \t\n]+variable    { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__p_variable ); }
^[Tt]heorem                    { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__theorem ); }
^[Pp]ublic[\ \t\n]+theorem     { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__p_theorem ); }
^[Oo]peration                  { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__operation ); }
^[Gg]lobal[\ \t\n]+operation   { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__g_operation ); }
^[Dd]efine                     { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__operation ); }
^[Gg]lobal[\ \t\n]+define      { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__g_operation ); }
^[Pp]ublic[\ \t\n]+define      { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__p_operation ); }
^[Rr]ead[\ \t]+                { BEGIN INCL; returntoken( yy__read ); }
^[Rr]eplaced[\ \t]+by[\ \t]+   { BEGIN INCL; returntoken( yy__replaced_by ); }
^C[\ \t]+constructors[\ \t]+for { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                  returntoken( yy__C_constr_for ); }
<INCL>[^\ \t\n\r]+    { 
													BEGIN INITIAL; 
													returntoken( yy__anb_string ); 
											}
<INCL>\n                { lexical_kwread_error(); }
<INCL>.                 { lexical_kwread_error(); }
<<EOF>>                 {     yyterminate(); }
<PAR>[0-9]+                            { returntoken( yy__integer ); }
<PAR>[0-9]+\.[0-9]+                    { returntoken( yy__float ); }
<PAR>\$[A-Z][A-Za-z0-9\_]*             { returntoken( yy__utvar ); }
<PAR>[A-Z][A-Za-z0-9\_]*               { returntoken( yy__Symbol ); }
<PAR>[a-z\_][A-Za-z0-9\_]*             { returntoken( yy__symbol ); }
<PAR>\|\-[a-z\_][A-Za-z0-9\_]*\-\>     { returntoken( yy__rec_mapsto ); }
<PAR>\|\-[a-z\_][A-Za-z0-9\_]*\-\>\>   { returntoken( yy__rec_mapstoo ); }
<PAR>\£[A-Z][A-Za-z0-9\_]*             { returntoken( yy__Symbol ); }
<PAR>\£[a-z\_][A-Za-z0-9\_]*           { returntoken( yy__symbol ); }
<PAR>\£                                { returntoken( yy__symbol ); }
<PAR>[\t \r]                           { updateTokenStart(); }
<PAR>\n                                { updateTokenStart(); }
<PAR>.                                 { lexical_error(); }
\n                                 { updateTokenStart(); }
.                                  { updateTokenStart(); }
%%

//bool yywrap() { return true; }

void updateTokenStart()
{
  tokenStart = yyoffset;
  linenoStart = lineno;
  colnoStart = colno;
}

/* Definition of functions using the definitions lexer of states. */ 

/* storing a character into the string buffer 'str_buf'. */ 
void store_str_char(char c)
{
	if (str_index >= str_buf_size-1)
	{
		str_buf[str_buf_size-1] = 0; 
		BEGIN STRTL;
	}
	else
		str_buf[str_index++] = c; 
}


/* end of lexer -----------------------------------------------------------------------*/


    """
    # -----------------------------------------
    # end raw lex script
    # -----------------------------------------

def usage():
    print "%s: PyBison parser derived from %s and %s" % (sys.argv[0], bisonFile, lexFile)
    print "Usage: %s [-k] [-v] [-d] [filename]" % sys.argv[0]
    print "  -k       Keep temporary files used in building parse engine lib"
    print "  -v       Enable verbose messages while parser is running"
    print "  -d       Enable garrulous debug messages from parser engine"
    print "  filename path of a file to parse, defaults to stdin"

def main(*argsList):
    """
    Unit-testing func
    """

    args = []
    args.extend(argsList)
    keepfiles = 0
    verbose = 0
    debug = 0
    filename = None

    for s in ["-h", "-help", "--h", "--help", "-?"]:
        if s in args:
            usage()
            sys.exit(0)

    if len(args) > 0:
        if "-k" in args:
            print "keepfiles = 1"
            keepfiles = 1
            args.remove("-k")
        if "-v" in args:
            print "verbose = 1"
            verbose = 1
            args.remove("-v")
        if "-d" in args:
            print "debug = 1"
            debug = 1
            args.remove("-d")
    if len(args) > 0:
        filename = args[0]

    p = Parser(verbose=verbose, keepfiles=keepfiles)
    tree = p.run(file=filename, debug=debug)
    return tree

if __name__ == "__main__":
    main(*(sys.argv[1:]))

