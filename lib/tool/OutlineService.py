#----------------------------------------------------------------------------
# Name:         OutlineService.py
# Purpose:      Outline View Service for pydocview
#
# Author:       Morgan Hua
#
# Created:      8/3/04
# CVS-ID:       $Id: OutlineService.py,v 1.1 2005/06/27 23:26:07 ricard Exp $
# Copyright:    (c) 2005 I-Team
# License:      wxWindows License
#----------------------------------------------------------------------------

import wx
import wx.lib.docview
import wx.lib.pydocview
import Service
_ = wx.GetTranslation


#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
SORT_NONE = 0
SORT_ASC = 1
SORT_DESC = 2


OUTLINE_ICON            = 0
UNKNOWN_ICON            = 1
PRIVATE_FUNCTION_ICON   = 2
PUBLIC_FUNCTION_ICON    = 3
GLOBAL_FUNCTION_ICON    = 4
PRIVATE_VARIABLE_ICON   = 5
PUBLIC_VARIABLE_ICON    = 6
PRIVATE_TYPE_ICON       = 7
PUBLIC_TYPE_ICON        = 8
PROPERTY_ICON           = 9

class OutlineView(Service.ServiceView):
    """ Reusable Outline View for any document.
        As a default, it uses a modified tree control (OutlineTreeCtrl) that allows sorting.
        Subclass OutlineTreeCtrl to customize the tree control and call SetTreeCtrl to install a customized tree control.
        When an item is selected, the document view is called back (with DoSelectCallback) to highlight and display the corresponding item in the document view.
    """

    
    #----------------------------------------------------------------------------
    # Overridden methods
    #----------------------------------------------------------------------------

    def __init__(self, service):
        Service.ServiceView.__init__(self, service)
        self._actionOnSelect = True


    def _CreateControl(self, parent, id):
        treeCtrl = OutlineTreeCtrl(parent, id)
        wx.EVT_TREE_SEL_CHANGED(treeCtrl, treeCtrl.GetId(), self.DoSelection)
        wx.EVT_SET_FOCUS(treeCtrl, self.DoSelection)
        wx.EVT_ENTER_WINDOW(treeCtrl, treeCtrl.CallDoLoadOutlineCallback)
        wx.EVT_RIGHT_DOWN(treeCtrl, self.OnRightClick)

        return treeCtrl


    def GetIcon(self):
        return getOutlineIcon()


    #----------------------------------------------------------------------------
    # Service specific methods
    #----------------------------------------------------------------------------

    def OnRightClick(self, event):
        menu = wx.Menu()

        menu.AppendRadioItem(OutlineService.SORT_NONE, _("Unsorted"), _("Display items in original order"))
        menu.AppendRadioItem(OutlineService.SORT_ASC, _("Sort A-Z"), _("Display items in ascending order"))
        menu.AppendRadioItem(OutlineService.SORT_DESC, _("Sort Z-A"), _("Display items in descending order"))

        config = wx.ConfigBase_Get()
        sort = config.ReadInt("OutlineSort", SORT_NONE)
        if sort == SORT_NONE:
            menu.Check(OutlineService.SORT_NONE, True)
        elif sort == SORT_ASC:
            menu.Check(OutlineService.SORT_ASC, True)
        elif sort == SORT_DESC:
            menu.Check(OutlineService.SORT_DESC, True)

        self.GetControl().PopupMenu(menu, event.GetPosition())
        menu.Destroy()


    #----------------------------------------------------------------------------
    # Tree Methods
    #----------------------------------------------------------------------------

    def DoSelection(self, event):
        if not self._actionOnSelect:
            return
        item = self.GetControl().GetSelection()
        if item:
            self.GetControl().CallDoSelectCallback(item)
        event.Skip()


    def ResumeActionOnSelect(self):
        self._actionOnSelect = True


    def StopActionOnSelect(self):
        self._actionOnSelect = False


    def SetTreeCtrl(self, tree):
        self.SetControl(tree)
        wx.EVT_TREE_SEL_CHANGED(self.GetControl(), self.GetControl().GetId(), self.DoSelection)
        wx.EVT_ENTER_WINDOW(self.GetControl(), treeCtrl.CallDoLoadOutlineCallback)
        wx.EVT_RIGHT_DOWN(self.GetControl(), self.OnRightClick)


    def GetTreeCtrl(self):
        return self.GetControl()


    def OnSort(self, sortOrder):
        treeCtrl = self.GetControl()
        treeCtrl.SetSortOrder(sortOrder)
        treeCtrl.SortAllChildren(treeCtrl.GetRootItem())


    def ClearTreeCtrl(self):
        if self.GetControl():
            self.GetControl().DeleteAllItems()


    def GetExpansionState(self):
        expanded = []

        treeCtrl = self.GetControl()
        if not treeCtrl:
            return expanded

        parentItem = treeCtrl.GetRootItem()

        if not parentItem:
            return expanded

        if not treeCtrl.IsExpanded(parentItem):
            return expanded

        expanded.append(treeCtrl.GetItemText(parentItem))

        (child, cookie) = treeCtrl.GetFirstChild(parentItem)
        while child.IsOk():
            if treeCtrl.IsExpanded(child):
                expanded.append(treeCtrl.GetItemText(child))
            (child, cookie) = treeCtrl.GetNextChild(parentItem, cookie)
        return expanded


    def SetExpansionState(self, expanded):
        if not expanded or len(expanded) == 0:
            return

        treeCtrl = self.GetControl()
        parentItem = treeCtrl.GetRootItem()
        if expanded[0] != treeCtrl.GetItemText(parentItem):
            return

        (child, cookie) = treeCtrl.GetFirstChild(parentItem)
        while child.IsOk():
            if treeCtrl.GetItemText(child) in expanded:
                treeCtrl.Expand(child)
            (child, cookie) = treeCtrl.GetNextChild(parentItem, cookie)

        if parentItem:
            treeCtrl.EnsureVisible(parentItem)


class OutlineTreeCtrl(wx.TreeCtrl):
    """ Default Tree Control Class for OutlineView.
        This class has the added functionality of sorting by the labels
    """


    #----------------------------------------------------------------------------
    # Constants
    #----------------------------------------------------------------------------
    ORIG_ORDER = 0
    VIEW = 1
    CALLBACKDATA = 2


    #----------------------------------------------------------------------------
    # Overridden Methods
    #----------------------------------------------------------------------------

    def __init__(self, parent, id, style=wx.TR_HAS_BUTTONS|wx.TR_DEFAULT_STYLE):
        wx.TreeCtrl.__init__(self, parent, id, style = style)
        self._origOrderIndex = 0
        self._sortOrder = SORT_NONE
        imgList = wx.ImageList(16, 16)
        imgList.AddIcon(getOutlineIcon())
        imgList.AddIcon(getUnknownIcon())
        imgList.AddIcon(getPrivateFunctionIcon())
        imgList.AddIcon(getPublicFunctionIcon())
        imgList.AddIcon(getGlobalFunctionIcon())
        imgList.AddIcon(getPrivateVariableIcon())
        imgList.AddIcon(getPublicVariableIcon())
        imgList.AddIcon(getPrivateTypeIcon())
        imgList.AddIcon(getPublicTypeIcon())
        imgList.AddIcon(getPropertyIcon())
        self.AssignImageList(imgList)


    def DeleteAllItems(self):
        self._origOrderIndex = 0
        wx.TreeCtrl.DeleteAllItems(self)


    #----------------------------------------------------------------------------
    # Sort Methods
    #----------------------------------------------------------------------------

    def SetSortOrder(self, sortOrder = SORT_NONE):
        """ Sort Order constants are defined at top of file """
        self._sortOrder = sortOrder


    def OnCompareItems(self, item1, item2):
        if self._sortOrder == SORT_ASC:
            return cmp(self.GetItemText(item1).lower(), self.GetItemText(item2).lower())  # sort A-Z
        elif self._sortOrder == SORT_DESC:
            return cmp(self.GetItemText(item2).lower(), self.GetItemText(item1).lower())  # sort Z-A
        else:
            return (self.GetPyData(item1)[self.ORIG_ORDER] > self.GetPyData(item2)[self.ORIG_ORDER]) # unsorted


    def SortAllChildren(self, parentItem):
        if parentItem and self.GetChildrenCount(parentItem, False):
            self.SortChildren(parentItem)
            (child, cookie) = self.GetFirstChild(parentItem)
            while child.IsOk():
                self.SortAllChildren(child)
                (child, cookie) = self.GetNextChild(parentItem, cookie)


    #----------------------------------------------------------------------------
    # Select Callback Methods
    #----------------------------------------------------------------------------

    def CallDoSelectCallback(self, item):
        """ Invoke the DoSelectCallback of the given view to highlight text in the document view
        """
        data = self.GetPyData(item)
        if not data:
            return

        view = data[self.VIEW]
        cbdata = data[self.CALLBACKDATA]
        if view:
            view.DoSelectCallback(cbdata)


    def SelectClosestItem(self, position):
        tree = self
        distances = []
        items = []
        self.FindDistanceToTreeItems(tree.GetRootItem(), position, distances, items)
        mindist = 1000000
        mindex = -1
        for index in range(0, len(distances)):
            if distances[index] <= mindist:
                mindist = distances[index]
                mindex = index
        if mindex != -1:
            item = items[mindex]
            self.EnsureVisible(item)
            os_view = wx.GetApp().GetService(OutlineService).GetView()
            if os_view:
               os_view.StopActionOnSelect()
            self.SelectItem(item)
            if os_view:
               os_view.ResumeActionOnSelect()


    def FindDistanceToTreeItems(self, item, position, distances, items):
        data = self.GetPyData(item)
        this_dist = 1000000
        if data and data[2]:
            positionTuple = data[2]
            if position >= positionTuple[1]:
                items.append(item)
                distances.append(position - positionTuple[1])

        if self.ItemHasChildren(item):
            child, cookie = self.GetFirstChild(item)
            while child and child.IsOk():
                self.FindDistanceToTreeItems(child, position, distances, items)
                child, cookie = self.GetNextChild(item, cookie)
        return False


    def SetDoSelectCallback(self, item, view, callbackdata):
        """ When an item in the outline view is selected,
        a method is called to select the respective text in the document view.
        The view must define the method DoSelectCallback(self, data) in order for this to work
        """
        self.SetPyData(item, (self._origOrderIndex, view, callbackdata))
        self._origOrderIndex = self._origOrderIndex + 1


    def CallDoLoadOutlineCallback(self, event):
        """ Invoke the DoLoadOutlineCallback
        """
        rootItem = self.GetRootItem()
        if rootItem:
            data = self.GetPyData(rootItem)
            if data:
                view = data[self.VIEW]
                if view and view.DoLoadOutlineCallback():
                    self.SortAllChildren(self.GetRootItem())


    def GetCallbackView(self):
        rootItem = self.GetRootItem()
        if rootItem:
            return self.GetPyData(rootItem)[self.VIEW]
        else:
            return None


class OutlineService(Service.Service):


    #----------------------------------------------------------------------------
    # Constants
    #----------------------------------------------------------------------------
    SHOW_WINDOW = wx.NewId()  # keep this line for each subclass, need unique ID for each Service
    SORT = wx.NewId()
    SORT_ASC = wx.NewId()
    SORT_DESC = wx.NewId()
    SORT_NONE = wx.NewId()


    #----------------------------------------------------------------------------
    # Overridden methods
    #----------------------------------------------------------------------------

    def __init__(self, serviceName, embeddedWindowLocation = wx.lib.pydocview.EMBEDDED_WINDOW_BOTTOM):
        Service.Service.__init__(self, serviceName, embeddedWindowLocation)
        self._validTemplates = []


    def _CreateView(self):
        return OutlineView(self)


    def InstallControls(self, frame, menuBar = None, toolBar = None, statusBar = None, document = None):
        Service.Service.InstallControls(self, frame, menuBar, toolBar, statusBar, document)

        wx.EVT_MENU(frame, OutlineService.SORT_ASC, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, OutlineService.SORT_ASC, frame.ProcessUpdateUIEvent)
        wx.EVT_MENU(frame, OutlineService.SORT_DESC, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, OutlineService.SORT_DESC, frame.ProcessUpdateUIEvent)
        wx.EVT_MENU(frame, OutlineService.SORT_NONE, frame.ProcessEvent)
        wx.EVT_UPDATE_UI(frame, OutlineService.SORT_NONE, frame.ProcessUpdateUIEvent)


        if wx.GetApp().GetDocumentManager().GetFlags() & wx.lib.docview.DOC_SDI:
            return True

        viewMenu = menuBar.GetMenu(menuBar.FindMenu(_("&View")))
        self._outlineSortMenu = wx.Menu()
        self._outlineSortMenu.AppendRadioItem(OutlineService.SORT_NONE, _("Unsorted"), _("Display items in original order"))
        self._outlineSortMenu.AppendRadioItem(OutlineService.SORT_ASC, _("Sort A-Z"), _("Display items in ascending order"))
        self._outlineSortMenu.AppendRadioItem(OutlineService.SORT_DESC, _("Sort Z-A"), _("Display items in descending order"))
        viewMenu.AppendMenu(wx.NewId(), _("Outline Sort"), self._outlineSortMenu)

        return True


    #----------------------------------------------------------------------------
    # Event Processing Methods
    #----------------------------------------------------------------------------

    def ProcessEvent(self, event):
        if Service.Service.ProcessEvent(self, event):
            return True

        id = event.GetId()
        if id == OutlineService.SORT_ASC:
            self.OnSort(event)
            return True
        elif id == OutlineService.SORT_DESC:
            self.OnSort(event)
            return True
        elif id == OutlineService.SORT_NONE:
            self.OnSort(event)
            return True
        else:
            return False


    def ProcessUpdateUIEvent(self, event):
        if Service.Service.ProcessUpdateUIEvent(self, event):
            return True

        id = event.GetId()
        if id == OutlineService.SORT_ASC:
            event.Enable(True)

            config = wx.ConfigBase_Get()
            sort = config.ReadInt("OutlineSort", SORT_NONE)
            if sort == SORT_ASC:
                self._outlineSortMenu.Check(OutlineService.SORT_ASC, True)
            else:
                self._outlineSortMenu.Check(OutlineService.SORT_ASC, False)

            return True
        elif id == OutlineService.SORT_DESC:
            event.Enable(True)

            config = wx.ConfigBase_Get()
            sort = config.ReadInt("OutlineSort", SORT_NONE)
            if sort == SORT_DESC:
                self._outlineSortMenu.Check(OutlineService.SORT_DESC, True)
            else:
                self._outlineSortMenu.Check(OutlineService.SORT_DESC, False)

            return True
        elif id == OutlineService.SORT_NONE:
            event.Enable(True)

            config = wx.ConfigBase_Get()
            sort = config.ReadInt("OutlineSort", SORT_NONE)
            if sort == SORT_NONE:
                self._outlineSortMenu.Check(OutlineService.SORT_NONE, True)
            else:
                self._outlineSortMenu.Check(OutlineService.SORT_NONE, False)

            return True
        else:
            return False


    def OnSort(self, event):
        id = event.GetId()
        if id == OutlineService.SORT_ASC:
            wx.ConfigBase_Get().WriteInt("OutlineSort", SORT_ASC)
            self.GetView().OnSort(SORT_ASC)
            return True
        elif id == OutlineService.SORT_DESC:
            wx.ConfigBase_Get().WriteInt("OutlineSort", SORT_DESC)
            self.GetView().OnSort(SORT_DESC)
            return True
        elif id == OutlineService.SORT_NONE:
            wx.ConfigBase_Get().WriteInt("OutlineSort", SORT_NONE)
            self.GetView().OnSort(SORT_NONE)
            return True


    #----------------------------------------------------------------------------
    # Service specific methods
    #----------------------------------------------------------------------------

    def LoadOutline(self, view, position=-1, force=False):
        if not self.GetView():
            return

        if hasattr(view, "DoLoadOutlineCallback"):
            self.SaveExpansionState()
            if view.DoLoadOutlineCallback(force=force):
                self.GetView().OnSort(wx.ConfigBase_Get().ReadInt("OutlineSort", SORT_NONE))
                self.LoadExpansionState()
            if position >= 0:
                self.SyncToPosition(position)


    def SyncToPosition(self, position):
        if not self.GetView():
            return
        self.GetView().GetTreeCtrl().SelectClosestItem(position)


    def OnCloseFrame(self, event):
        Service.Service.OnCloseFrame(self, event)
        self.SaveExpansionState(clear = True)

        return True


    def SaveExpansionState(self, clear = False):
        if clear:
            expanded = []
        elif self.GetView():
            expanded = self.GetView().GetExpansionState()
        wx.ConfigBase_Get().Write("OutlineLastExpanded", expanded.__repr__())


    def LoadExpansionState(self):
        expanded = wx.ConfigBase_Get().Read("OutlineLastExpanded")
        if expanded:
            self.GetView().SetExpansionState(eval(expanded))


    #----------------------------------------------------------------------------
    # Timer Methods
    #----------------------------------------------------------------------------

    def StartBackgroundTimer(self):
        self._timer = wx.PyTimer(self.DoBackgroundRefresh)
        self._timer.Start(250)


    def DoBackgroundRefresh(self):
        """ Refresh the outline view periodically """
        self._timer.Stop()
        
        foundRegisteredView = False
        if self.GetView():
            currView = wx.GetApp().GetDocumentManager().GetCurrentView()
            if currView:
                for template in self._validTemplates:
                    type = template.GetViewType()
                    if isinstance(currView, type):
                        self.LoadOutline(currView)
                        foundRegisteredView = True
                        break

            if not foundRegisteredView:
                self.GetView().ClearTreeCtrl()
                    
        self._timer.Start(1000) # 1 second interval


    def AddTemplateForBackgroundHandler(self, template):
        self._validTemplates.append(template)


    def GetTemplatesForBackgroundHandler(self):
        return self._validTemplates


    def RemoveTemplateForBackgroundHandler(self, template):
        self._validTemplates.remove(template)

#----------------------------------------------------------------------
# This file was generated by D:\Develop\Libraries\wxWidgets.261\wxPython\wx\tools\img2py.py
#
from wx import ImageFromStream, BitmapFromImage
from wx import EmptyIcon
import cStringIO, zlib


def getOutlineData():
    return zlib.decompress(
'x\xda\xeb\x0c\xf0s\xe7\xe5\x92\xe2b``\xe0\xf5\xf4p\t\x02\xd2\x02 \xcc\xc1\
\x06$\xe5?\xffO\x04R,\xc5N\x9e!\x1c@P\xc3\x91\xd2\x01\xe4\xcf\xf6tq\x0c\xb1\
\xe8MNIIJ[\xc0b\x10\xb8d\xb6\x14\xdb\x86\x05\xdc.\xe1:\xff\xf9\x93\xf5\xdfv\
\xa4\xa8\xdcd`\x03\x82\xe0\xdc\xc8\xb6\xcb\xe1\xf2\xc6\x06@0\xc5\xef\xd6\xdf\
\xff\xe7\xe7\xdb\xf3\xec\xaf\xfb\x9b\xfa\xe3\xebg\xf1\xc3\xeb\xef\xd7K\x88mk\
1|d\xca\xb0lfcc\xe3B\xeb\xf3\xa2\xfb\xb5%f\xce0,\xf0\xb9\xba\xea\xff\x8d#\
\x1b\x13\xd7\xe8\x1d`+h\xe5\xf5c\x9e`z\x80\xa1\xe8\x86\x80\t\x83-\xdb"\xedi\
\xff\xdd\xaf\xf7\\gpb\x9b\x94\xc3p\xf6P\xc3\xc4\x1bgZN\xb3\xbaH7p\x07\xcc\
\x99\xc9\xc0\xf9\xcb\xe6:\xeb\xa1\xa7\xcb\x80\xced\xf0t\xf5sY\xe7\x94\xd0\
\x04\x00L\x95S\x10' )

def getOutlineBitmap():
    return BitmapFromImage(getOutlineImage())

def getOutlineImage():
    stream = cStringIO.StringIO(getOutlineData())
    return ImageFromStream(stream)

def getOutlineIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getOutlineBitmap())
    return icon

#----------------------------------------------------------------------
def getGlobalFunctionData():
    return zlib.decompress(
'x\xda\x016\x02\xc9\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x01\xedIDAT8\x8d\xd5\x91AH\x93a\x1c\xc6\x7f\
\xef\xbe\xc5R\xbf\xb9\xe96\x1d$\xd8\xc6\xca1v\xa8C\x03%(Oz1bP\x18\x18!BE\x11\
\xa5\x87\xe8\xd0\xa1\xdb\x82 \x12"\x08\xba\x94P_\rJ-\x08\xda\xc1\xda\xa1I0\
\xecP\xac\xcb\xd4\x829\xa5\xd9V\x1b\x9a\x8c\xed\xed\xe06\xbe4j\xd7\x1ex//\
\xcf\xf3{\x9f\xf7\xff\x87\xffF\xa1K\xa1\xdbs\xd7\xe7\xe4\xc8\xf1\x911\xfd\
\xbdRo\xb8\xdf\xd9\x7f\xde\xee\xb1\xe37\xf9\xfbd\xab\xcc\xc7?\xc6cu\x01&B\
\x13\xd3=\xe6\x9e\xa1\xce@\'\xad\x1b6Lk&\xba\x1c{\xfb\x9a:Tb\xefco\x8cz\xf3z\
.,\xe7S\x82\xc4\x82\x81\xf9\x14\\>\x13\x14~\xe1\x1f\xb0u\xd9PW\xcd\xb0\n\x16\
\x93\x85\xb2,s\xd2;tme`9\xfd\x1b\xa0\xc1zLlm\x90\x98\xfd\x84#\xed@\r\x98Q\
\x15\x15J\xd0\xd2\xd0Big\x89\x03"p\xd7\x00\x90\xcfh\xf2\xdc\xd9#\xd2n\xb7JEQ\
d\xf7\xc1n9~\xef\xa1\x04\x10R\xd0Vn#\xfe.N\xa1\xb1\x00&(\x18\x0bL\xbe\x9a$\
\xb3\xf8\x95\xea\x8b!\xe0\np\x01\xc8\x00\x8f\x80[\xc0\xe8\xe3\xe0\x13\xe9uz1\
\xeePH\x8be\\^\x17SO\xa7h^k\xe6\xcb\x8f\xcf5@\x02\xf0\xce\xbe\xbd\xc3z\xb1\
\x89\xdeC\xa7\xb0X\xad|\xcf\xe5DT\x8b&\x97\xb4\xb4\xdb\xb7\xcb\x87P\x04\xe1h\
\x18\x97\xba\x9bo\x1bY\xb2?\xb35@\x110\x02\xdbf\x00\xa0\x8dk3b\xc6p\xd8\xdd\
\xee\xe6yl\x9a\xf6F\'\xa9|\x8a=\'<\xcf\x0c\x15\x8f~\x98Rw\x00\x18\xbc8\xd8\
\xbb\xe4K=H\xae$\x11\x06\x03\x8b\xd9\x05\xf6\x9f\xde\xf7z\xf8\xeap\xb0\xea)\
\xea\x03[\x01\xb5&7\xb5\xc8\xa8gLF\xeeG>l\xdbV%\xd0\xf17\xc0\x9fT\xad\xfe\
\x02\xf0\x02G\xd9\xdc\x02@\xae\x1e\x80\x1et\x03H\xb3\xf9\x9d\x97\x15\xe0?\
\xf5\x0b\xdb\x15\xa8\xad>\xac\x8dD\x00\x00\x00\x00IEND\xaeB`\x82\n\x89\x02\
\x0b' )

def getGlobalFunctionBitmap():
    return BitmapFromImage(getGlobalFunctionImage())

def getGlobalFunctionImage():
    stream = cStringIO.StringIO(getGlobalFunctionData())
    return ImageFromStream(stream)

def getGlobalFunctionIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getGlobalFunctionBitmap())
    return icon

#----------------------------------------------------------------------
def getPrivateFunctionData():
    return zlib.decompress(
'x\xda\x01\xb8\x02G\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x02oIDAT8\x8d\x95\x93[HS\x01\x18\xc7\xff\xe7l\
m\x1d;j\xde\xce\xba@:\xd1JA\xb7\x9aN\x96\x04\x11>\xb8\xd2\x87\tRY-LQ\x0bM\
\x88\x08ab\xbet\xa1^\xa2|X=\x04\xa3,$\x10\r\xed\x02&=H\xdaB70:\x8b\xda\x94j9\
\xe7\xe6\xe66w9\xb4sz\x88\x89\x15\xd1\xfa?~|\xff\xdfw\xe1\xfb\x80$\xd5\xd3\
\xdas\xc3r\xcd"\xe8u\xfa\x8e\xf5qQ\xb2f\x9d\\w!\xbb \x1b\x8a\x14\x85\x96K\
\xe5\x96\xad6\xab9)\x80\xb1\xdb\xf8\xa8JV\xd5\x92\xab\xceEf,\x0b\xd2\xb0\x14\
E\xb2"\xed\x06F\xc2\x99g\xcd\x13\xe2\xf5\xc9\x11\xffc\xc1\xe1$\xc0\xce\x91p8\
\x81\x8b\xaduD\x05]q4kW\x16ho*\xe0\x05\xd2\xa5\xe9\xe0\x05\x1eM\xa5\xa7\xaf\
\xb8\x97\x17]\xbf\x00\xa8\xcd\xf5\xc4\xef\x1d\xb0S6\xe4,\xe4\x80V\xa7\x82\
\x16\xd1@\x1c\xc8\xa02\x10\xdf\x18\xc7>I\xe5=q\xa2r`\x15\xf0\xf8\tx|\xc0\xd2\
\n\x01\x8f\x8f@\xdb\x89:\x82\x10\x080<\x83i\xf34Tj\x15h\x8eF\x88\x0faht\x08\
\x9e\xf9%\x88\xffV9!RD\x02q`\x1b\xb9\x153og \xdf-\xc7\xf0\xe00\xd2\xc2i A\
\xfe\x04\xbc|r\xd7\xc2H\x9c\xcah$\x8c8\xe7\x86\x94)\xc3\x0b\x0b\xff\xbc\xeb|\
\xa7vSu\x8a\x95}\xca*\x8b\xb7\x17C&\x92\xc1d4AN\xe7a9\xe6\x03/\x08 \xcd\xe3\
\x0f\xe7\x18\x89S\x99_\xa6BIQ\x10J\x05\x05\xf6u?Ni\xf3\xab\x01\xa0\xa6\xa5fO\
\xa8<8\xc2:YDcQ\x80\x17\x10\xfe\x1e\x817\xec\xc5\x96\x1a\xd9\x03\x92\xe2>\
\xe4\xe5\xab\x14\xa0(\x0e}\xb7\x9f\xc1\xf95\x80c\x878\x8c\r\x18\xd6\xc6h64\
\xd7~\xca\xfbh\xb4/\xdaA\x90$\xe6}s(l(\x18i\xbf\xda~\x92\x04\x80\x98\xdf\x05\
\xee\xcb\x18t\xbaJ\x00\x80o%\xfe\xc7.\x0c7\rgB{\x83C\x81P\x00\x9a\x0e\xcdT\
\xa3\xa1\xb1\x16\x00\x88[\xd7\xbbL\xea\xdc\x05}\xc5\xceY\xc4(5\xc2\xaeI\x8c\
\xbe\x12@\x176M\xe8\x8ew\xee\xff\xd7\xa1\x01\x00&\xc7\x07>\xdb\xc7\x1a\x84\
\xa8\xadM\xb8\xdf[*\x9c;\xab\xbf\x94\x94\x11\x00\t\x00\x9a\x83Gv$\x02\x87\
\x0f\x10(g\xac\xbd\xff\x05H\xc8\xef_\x85i0\x887\x0b%\xdd\xc9\x02\xd6\x9e\xc9\
\xe1\xce|\xbf\xfa\xed]\xbd=\xa6\xe9\xee\xbb\xd3\x7f9Y\xc0\x0f\xd6V\xf15\xb7x\
\x9b\xac\x00\x00\x00\x00IEND\xaeB`\x82\x85\xdc2/' )

def getPrivateFunctionBitmap():
    return BitmapFromImage(getPrivateFunctionImage())

def getPrivateFunctionImage():
    stream = cStringIO.StringIO(getPrivateFunctionData())
    return ImageFromStream(stream)

def getPrivateFunctionIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getPrivateFunctionBitmap())
    return icon

#----------------------------------------------------------------------
def getPublicFunctionData():
    return zlib.decompress(
'x\xda\x01\xed\x01\x12\xfe\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\
\x10\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\
\x08\x08\x08\x08|\x08d\x88\x00\x00\x01\xa4IDAT8\x8d\xd5\x8e?L\x13q\x18\x86\
\x9f\xef\xd7&\x15\xbd\x82\xa5\x94?b\x82m\xaa\x90\xa6\x83\x86\xd8\x81I\'XX\
\x9ah \xa9\x03\xc1\xa8\x89\x03\xc8\xe0\xe4\xe0V&\x12\x12\x17Ge\xb9\xa4\x89\
\x02\xc6\xa9\x03\xda\xa5\x04R`\xc0\xe0R`9\xce\x86+W\x01AH\xdasB\xaa\tI\xd9\
\xf4\x19\xbf|\xcf\xfb\xbe\xf0\xdf\x90\x1cM\xbeZ\x1e_v\x86\xef\x0f\x8fU\xdf]\
\xb5\xca}\xad}O\x9b\xc2MD=\xd1^\xa7\xd1\xd9\xcb}\xc9ek\n\x98JN\xcd\xf6x{\x12\
\x1d\xb1\x0e\x1a\x8f\xfcx\x0e<t\x06n\xf4^\xba\xaa\x91]\xc9~vW?\x1f\x96R\xce\
\xba!\xacm(\xd6\rx\xfe8.Q\x89\xf6\xfb;\xfdhE/\x14\xa1\xc1\xd3@\xc5\xa9\xf0\
\xa0+\xf1\xb2\xd0\xff\xcd\xfc#\xa0\xee\xf2=\xf9{\xc1\xda\xfcW\x02f\x00-\xe6E\
siP\x06_\x9d\x8f\xf2\x852\xb7%\xf6\xda}\xd2\xbc\xfb\x03\xac\x92`\xd9\xb0\xfd\
]\xb0l\xe1I".\xe2\x08\xcd\x95fr\x0b9\xbac\xddh\xc7\x1a\xfb\x95}\xa6?Ncmn\xe3\
>\xab\xf9\x04\xe5RP\x86+\xaa\x8d\xa5\xc5%\x82]Af\xde\xcdP\x7fP\x8fB\xf1[<,\
\xa5\x9c\x9d]0-\xa1P\x14LKx8\x18\x97\x8c\x9e\xc9o\xe9f(\xd2\x1eA\\B*\x93"\
\xa8]c\xe7\xc8\xc6\xfeisfs5\xfa\xa4>\'s\xeaN\xa8%\xc4\x87\xec,-\x17[1\xf6\
\x0c\xae\x0f\x86\xdf\xabZ\x02\x06F\x06\xeenE\x8c\xb7\xf9B\x1eQ\x8aM{\x83[\
\x8fn~\x1az1\x14\xaf\xc5?]2\xa1\xa7\x9f\x85\xc7\x9c\xf4\x9b\xf4\xea\xb9\xc4\
\x7f\x9b_n\xa3\x94*\x8bd_a\x00\x00\x00\x00IEND\xaeB`\x820\x89\xdd[' )

def getPublicFunctionBitmap():
    return BitmapFromImage(getPublicFunctionImage())

def getPublicFunctionImage():
    stream = cStringIO.StringIO(getPublicFunctionData())
    return ImageFromStream(stream)

def getPublicFunctionIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getPublicFunctionBitmap())
    return icon

#----------------------------------------------------------------------
def getPublicVariableData():
    return zlib.decompress(
"x\xda\x01\x80\x01\x7f\xfe\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\
\x10\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\
\x08\x08\x08\x08|\x08d\x88\x00\x00\x017IDAT8\x8dc`\x18\xf4 \xbe\xb8\xb5\xa7v\
\xd3\xd3\xff\x86\xd6\xae\x9e\xd8\xe4\x99\xf1i\xcej\x986G\xc4,,[\x80\x97\x8bA\
\xc6\xc4+\x9a\xf3\xcf\x87\xe7\xf7\xae]8K\x94\x01\xb1\x85-\x9db\x16\xe1\xb9\
\x12\xa2\xc2\x0c<\xbc\xbc\x0c\xac,L\x0c\x02\xaa\x96\xbe\xca\xd2\xa2\xbc\x17\
\x8f\xef\xdd\x05S\xc7\x84MsDv]\x93\x9ccb\x99\x90\x80\x00\x03'''\x03\x03\x03\
\x03\x830\x1f7\x83\x8a$?\xc3\xa9\xe7Z\xc5\xc8jY\xb0\x19\xa0\xe2\x91Y+*\xc0\
\xc5\xc0\xc3\xc3\xc5\xf0\xe7\x1f\x03\x03\x1b\xf3\x7f\x06A\x8e\xbf\x0c9u\xdb\
\x19\x1e=\xfd\x80\xa2\x16\xab\x0b\xee\xbf\xf8\xc8\xf0\xf1\xcbO\x06Q\xce\xbf\
\x0c\\,\xff\x19\x849\xfe2\x94\xd5o\xc1\xd0\x8c\xd3\x05\x0c\x0c\x0c\x0c7\x9f\
\xbcc\xf8\xf5\xf7?\x83\xb1\x920Cs\xf7.\x86\x1bw^aU\x87\xd5\x05\x8c0\x97<\x7f\
\xcf\x10\x18;\x97\xe1\xf4\x85'p\xb9\xeeTI\x94X\xc0j\x80\x0e\xdb\x83\x93\xff\
\xff\xff\xc7\x10\x9fWe\xf8\xb24\xc3\xcf\x84\xa0\x01\x15\xf1\x1e\x16\x1a\xffo\
\x1e@6d^\x95\xe1\xcb\xa4`3\t\xac\xfe\xc0\x05\xaa'.\xdf\xca`<\xf3\xff\x9a\xbd\
\xf70\x9d3|\x00\x00\xcf`bF\xc6J/\x90\x00\x00\x00\x00IEND\xaeB`\x82\xb3\xbc\
\xa6\x7f" )

def getPublicVariableBitmap():
    return BitmapFromImage(getPublicVariableImage())

def getPublicVariableImage():
    stream = cStringIO.StringIO(getPublicVariableData())
    return ImageFromStream(stream)

def getPublicVariableIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getPublicVariableBitmap())
    return icon

#----------------------------------------------------------------------
def getPrivateVariableData():
    return zlib.decompress(
'x\xda\x01M\x02\xb2\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x02\x04IDAT8\x8d\xbd\x92OH\xd3q\x18\xc6?\xceU\
\xfe\xd6\xe6fsR\xcc\x92\x96\x1e\x12\x9c\x86\x92\xe9.J\n%I,\xc9j!\xa1\x14\xa4\
\xab(\x04\xed\xe2\xb2,\xb0,:,\xba\xcc \x18\xd1%:\x98!\x89\x82\xd1AZ%v\xc8L*#\
\xffL<l33u\xec\xd7\xb7\xd3~L\xac\xa1\x97\x9e\xe3\x0b\x9f\xe7}\x9e\xf7\xfb\
\x85\xff\xa5S\x8d7n\xb7tM\x89=\xb6\xf2\x83\xb1\xf3\xc4\xb5\xc0\r\xad\xf7;S\
\xf7V;\r:\r\xe9\x05\x15\'\xa5H\xc8\xffud\xf8\xdd\x9a\x0cj.]\xbf\x99\xb6\xef\
\xd8\xf9\xad&#Z\x9d\x8e\rj\x15\x86\xac\xa2\xca]f\x93\xee\xfd`\x7f\xaf*\x1e|\
\xdc\xe9\xba\xb6\xa3\xb4\xb6i\x8b\xc1\x80$I\x00\x18\x937\x93\xb9M\x8f\xcf\
\x9f\xdd\x08\xa0\x8eg\x90y\xa0\xbe\xc5d\xd0\xa0\xd5j\x88\xfc\x86\x8d\x89\x82\
\x94$\x99s\xae\x1e\xbeO\x85\x00\x88\x9b`|f\x8e\xb9\x9f\xcb\x98$\x19\x8dZ`L\
\x92i\xba\xd2\xad\xc0J\x02w\xfb\xc5G%yz\xc7\xd2\xe2/\xe4\xf0,\xdb\xb3\x0fa\
\xce9\x9a\x00\xf0i2@X\x16\xe4[\x8c\xb4u\xf42\xfayv\xc5\x12U\x14\xb6\x14\xe4\
\x93\xb3{\x9e\xbc\\\x89\x97]w\xf88\xe8\r&D\x93\xf8\x83\xd8k\x1e\xf0fxR\x01\
\xdb\xeb\xd2|\x00\xaa\x92<\xbd\xc3\x92\x9f\x8b$\x85\xb9\xe7\xeeaj\xf2\x07\'*\
\xc2\xbc}\xd1a\xb0\xa5\x06\xbe\t!VU\xf34[\xa7/;\xed\x85\xca\r\x96C3\x84\'\
\xfa\xb0\xdbm\x00\x04\xe7d\x00\xea\xed\xc5;\xb3"#\xfd\xb1&\x9ef\xeb\xf4\x99\
\xea"\xb3R\xe1\xe1\xb3\xb1\xb6\xb1\xa1\xd7lZ\xf4aNO&E\xfd\x81\xe7\x03\x82\
\x89H\xb1\x07\xe0jCUY\xc6\xc2P7\xc0\x93[\xe5\xc4\xc2\x8a:\xdd\xae\x9e/}\x0e\
\xb14zVx[\xad\xa2\xea\xf0\xfe\xd3\xf1^\xe7\xaf\x8a\x1a\x04\x06r\x85\xb7\xd5\
\xba\xba\xf8?\xb4\xe2#\x85B\x0b<~:\xcf\xab\xf1\x8c\xbau\'\xb0\x15Z\xcb\xee^\
\xb0\x88#\x95\xa5\xb5\xeb\xe1\xfe\x0041\xb9\xf1Nd\x88\xc0\x00\x00\x00\x00IEN\
D\xaeB`\x82\xaf\xfc\x18\x93' )

def getPrivateVariableBitmap():
    return BitmapFromImage(getPrivateVariableImage())

def getPrivateVariableImage():
    stream = cStringIO.StringIO(getPrivateVariableData())
    return ImageFromStream(stream)

def getPrivateVariableIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getPrivateVariableBitmap())
    return icon

#----------------------------------------------------------------------
def getPropertyData():
    return zlib.decompress(
'x\xda\x01\x91\x02n\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x02HIDAT8\x8d\xa5\x92]H\x93q\x14\xc6\x7f\xbe\
\xaeI*\x14\x84Tfa!\x98eY`\xda\xd0\xbe\x88\x04?&^H\xa2\xcc\xd0.\xa2\xba\x8a$\
\x84\xa80\xea\xb6n\n\xbb\xf2\xa2fJfi\xc5 \n\x84\x84\xccD\x8aJL\x8d\xb2\xfc\
\xa2\r\xb7\xb9\xb5\xed\xdd\xde\xed\xdd\xe9\xc2\x8f\xc8\x8f+\x1f\xf8\xc3\xe1\
\xe1\x9c\x87\xdf\xffp`\x95\x8aYl\xbcl\xb7Jb\xa2\x97\x96\xa7\xfd\xb7\x8a\x8f\
\xa6\xd6\xb9<\x82.\x10\nih\x81\x10\xcei;\xdbw\xe7\x0e\x84S\x8c\x99\xf1k\x930\
,\x0e\x18\x9b\xb2\xf7\xd5T\xa6\xe5\xbc\xb6\xf5\xd4\x15\x9dp.\xf8a\xd5\x80\
\xe6\xf7\xf3{*H\x87\xad+3\xca&\xec\xfbR\x88]\x1c\xe0\x9aq\xdb\xa7\x86\'+\x1b\
\xae\xaf\xa3\xa3\xddI\xc6\xae\x04\xb4\x80\x81h$\x88\xeewa\x8c\tpx\xdb\x00\
\x93j2]\xfe\x8f\x18\xae6\x8d\xca\xfc\xf0\x9e\xada\x14\x87\r\xf3\x91\x19\x1a\
\xef\rp\xfe\\\x12e\xe6nZ\xef\x9b\xd0U7a\x9f\x03=\xe8\xc1\xebr3\xe5\xf2\xe0\
\x8axP\x00\xd2w\xa6\x92\x9fiDq\xd8(:\xe4\xa3\xfeZ?\xe6\xc2t\xce\xd69\xe8|q\
\x90\xaa\xd3CL\xff\x1c\xe1\xdb\xe0wF\x86&x\xf0*\x96\xb7\x833DC\xea\xec\x0e6\
\xac\xf1\xf2g\xa4\rS\x96\xca\xe5\x86^6\xef\xb7p\xfb\xae\x95\xfa\xda\x8dT\xd5\
\xb8\xe9\xb8\xf3\x83\xec\x8a\x00_\x7f\x86IK\xf6\x92{\xc0L\xfc\x8ex~\xf9\x87\
\xe0M\xef\'\x11\x11\xb1\xdb\xc7\xe4\xe6\xa5S291*""\x1f\xfa\xba\xe5Bm\x91\x0c\
>\xda+[\xd6\'H\x8d\xa5Z\x1a\x9b\x9a\x17\xbe\xdb\xda\xd5,\x19gRd!@\xf4\xf0\
\xec\x9bS$\x1a\x95\xf6\'\x9dr\xd1rL\xbe\x0c\x8f\xcb\xb8S\xa4\xa0\xb4\\\x16/\
\xdd K,\x88\x8a\x10\xd1\xa1\xa0\xd0L\xfe\xf1\x12\x00|\xa1\xe5\x0f\xc9\xa0(\n\
\x00\xd6\xe7\xef\x01\xb0\x94\xe5\xf1\xf0Y\xcf\x92\xc6\xec\x9c\xbc\xe5\x03d\
\x0e\xa1\xba4\x17\x00]\xe0d\xb1\tU\x8b\xa2j\x10\x0c\x0b\xbeP\x14M_\x81`\xbe\
\x98\'\xa8(\xc9\xa3\xcd\xf6nIcF\x96i\xf9\x80\xb8\xb8\xb8\xff\x08T\x05\xca\
\xcbfq\x03\x1a\xf8U\xf0i\xb0\x02\x00\x86\xc7-Vn\\\xf9\x8cb0\xfes\x15\x05\x11\
\x1d\x11!\x06\x10\x00\x11Xz\xf9\xab\xd7_\x83\xdb&e$\x01R\x84\x00\x00\x00\x00\
IEND\xaeB`\x82\xa5\xef8\x8b' )

def getPropertyBitmap():
    return BitmapFromImage(getPropertyImage())

def getPropertyImage():
    stream = cStringIO.StringIO(getPropertyData())
    return ImageFromStream(stream)

def getPropertyIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getPropertyBitmap())
    return icon

#----------------------------------------------------------------------
def getPublicTypeData():
    return zlib.decompress(
'x\xda\x01:\x02\xc5\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x01\xf1IDAT8\x8dc`@\x03\x99\x89\xdeE\x0fW\x89\
\xfcO\x8b\xf3\xc8G\x97#\x08`\x9a\xbf>l\xfd\x7fb\x868i\x86\xc04\x7fy\xd8\xf6\
\xff\xcb\xb5\xec\xff\x1fO\x07`5\xc4H\xd1\xc8\x04\x99\xcf\x04\xd3\\\xe1y\xb2W\
\xc4\xac\x90\xe1\xff\xd7g\x0c\xff\xbe>e\xf8\xf7\xf5\t\x83\xaa,\x1b\x834\xcb\
\x91\t0C\x8c\x14\x8dL\x16\xb8,<\x8dl\x083\\\xb3y\x11\xc3\xbfo\xcf\x19\xfe\
\x7f}\x025\xe0\x05\xc3\x94\xb5\x1f\x18\x9e\xfe\xb1)8w\xed\xcdQ\tv\t\xa9\x05.\
\x0bOkx\xa83\xd8\xfc\xb3M;\xf9\xfe\xe4\xd6\xe7\x1f\x9e?c^[\xf5\xfe\x98\x88y!\
\xc3\xffo\x10\x9b!\x06\xbcD\xd1\xfc\xef\xed?\x06\x98f\x86\xd7\x8c\x0c_?}e`{\
\xca\x96\xf6\x86\xfb\xcdV\xc6\xb48\x8f|\x0b\xa9c\x13\x82\x83\x1d\x19\xfe}}\
\xca\xc0\xf0\xef7\xc3\xa4%w\x19\xde\xc9u#\xc2\xe7\x8d\x13\x83\x92\xa3"\x03\
\xc3kF\x86\x877\x1e2l\xdc\xbb\x91a\xd9\x97\xa5\xa6L\xc2L\x10\x05iq\x1e\xf9\
\xf3*\xf8\xfe\xbf\xdb&\xf3\x7f^\x05\x1f<\xe0\xf2\x1b\xa6\xff\x87\x19\xb2\xc9\
f\xf3\xff\xdb\x05w\xfe\xf7\xe8\xf6\xfe7R42\x81\x01&\x06\x06\x06\x86Y\x8bvL<\
\xf1\xcc\xaa`\xc3\xa1O\x0c\'\x9eY\x15\xccZ\xb4c"z,\xf9\x1d\xf1e\xdc\xb8w#\
\xc3\x16\xfe\x13qp\x9b\xd1\x01z\x94!\xbb\x00\x06\xa2g\xdf\xf8ol\xe7\xe1\x8f\
\xe2\x02\x18\xc0f3\xbaf;#u\x06\x01\xd3\xc6\r\xff\xb9D\xa4\xe1\xe9\x80\x18\
\x00\xd3\xfc\xf8\xf9+\x06\t\r~\x86\xb3_\x037\xfc\xe7\x12\x91f\xc4\xa7\t\xe6\
\x857\xd2\x8e\x0c0\xcd\xf7\x9f\xbfgX:\xe3 \x831\xf7\xfa\x00\x19Av\x06\xbc\
\x06\xc0\x80\xb1\x9d\x87?\x93j\xee\x065\x0be\x14\xcd\x0c\x0c\x0c\x0c\xcc\xc4\
\x18\xf0\xfc\xe1\x9d\x9b\x92\xbc\xef.\xec<\xce\x19\x81\xac\x99\x81\x81\x818\
\x17 \xbb\x04Y3\x03\x03\x03\x03\x00\xfd\x82\xf8\xeaW\xe7\xb5\x00\x00\x00\x00\
\x00IEND\xaeB`\x82\xbee\x02@' )

def getPublicTypeBitmap():
    return BitmapFromImage(getPublicTypeImage())

def getPublicTypeImage():
    stream = cStringIO.StringIO(getPublicTypeData())
    return ImageFromStream(stream)

def getPublicTypeIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getPublicTypeBitmap())
    return icon

#----------------------------------------------------------------------
def getPrivateTypeData():
    return zlib.decompress(
'x\xda\x01\xd7\x02(\xfd\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\
\x00\x00\x00\x10\x08\x06\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\
\x08\x08\x08|\x08d\x88\x00\x00\x02\x8eIDAT8\x8d\x95\x92_H\x93Q\x18\x87\x7f\
\xdf&\x1b\xd3\xd0\xaca\xe27#\x17K\xbd\x08e\x9b\xa9d\x82\xe1\xc5,*\x87tQ\x91\
\x12\xc1@"L\xeb"\xca\x8bI\xd8M\x04fRF7*\xcd\xb4\xb0\xb6jd\xa0f\xb1\xa6shb\
\xe5\x9fh\xc9rsk\xae6u\xdflK\xbf\xd3\x85}\xb6\x99\x10\xbdW\xe7\x9c\x97\xe79\
\x87\xf7w\x80uUy\xea`\x8d\xfd\x81\x98h\xcaUU\xeb{\xff,\x0ef\xec\xf5d\xb0y\
\xdb\xffI88`\xbfJ\x02\xe3g\xc8\xbc\xb5tC\x89<M\xae\x8c\xdc\xf38\xf8b\x89\xe5\
\xbaxO5\x083\x0b\x96q\x82e\x1c\x90\xa5\n@\xc7\x98\x1a8\x89<M\xael)n\xb5FJ\
\xf8kpn\r\xd8\xa0\x0b\x84q\xfc\x16\xb8\xd1\xd4\xe5\x87s\xb9\xe0\xdc\xc8\xb8\
\xf7M\xb209\xa5\xa5\xb8\xd5\x9a\xa1JG\x01\xbbOc\xf1Y\x8c.\xbfk\x96\xdfu\xc9g\
\x16\xe7V\x83\x04Wo^\x15|\x8d\x82\xd9o,8\x18s\x14\x98\x05\x06\x02\xa7@\xe3\
\x8d\xf3\x1a)M\xb9\xaa*/\xc5\xdcPVV\x04\x96q\x02\xecO4\xde\xb3\xe1\xfb\xf6k\
\x7f\xe6\xe3\xdd\x0fiQ\x1a0G\xc1>i\x87\xa1\xd7\x80\xf6\x80.\x87\xb7\x95\x07\
\n\x00FM\x1d\x0e~`\x82\xfe\xb1\x14\xc4J\xd8\x03a\x92\x12/\xde\xb2\xdd\xae\
\x05\x81\xea\x86\xb6\x92\x02\x80\'\x05OI\xa623\n\x06\x00\xdeP_\xfb4?0AK\x95\
\n\xec\xce\\Dv\x96\x08\x13f\x1d*J\xa4\xaa\xc8i\x1f6\x1d\xa2\x0c\xbd\x06<K\
\x18,\xe7`\x00\xe0\x89\xc2S;\xa4\x8a,\x88Da4\xdd|\x0e\xa7c\x01\xc7\x0e\x84\
\xd1\xd3y\xf9\xaf\xa8/\xbc;O\xd1\x15W\xdaH\xac\x98\x8e\x8a1\xe4w#<\xd3\x03\
\xb5z/\x00\xc07\xbf\xb2\xe1_9qw\x92\x14\xca\xd3\xb19\xa7N\xcfIx/\xc7Bm\x1fG,\
\x10.\r\x81\x96\xc4#1\xe6\x03\x8c\xfd\x04\x9bd\xa7M\x1b\xc13.\x0f\x923\x120\
\xcc\xa8\xf5$VLS\x000\xd0\xd7\xf9%\x895\xa4\xd2\x92x<\xec0\xc3\xea\xc9\xd66\
\xdej\xab\xab\xd2\xde&\x00\xe0\xa5\x8b\xc0\xc1\xd3.\x1ft\xcd\xaf\xa0\x88{\\*\
I\x14\xae\xa6\x00\x00\xb6\x9e\xe3\x84\x96\xc4#\xe8\x1e\x80\xb1\x9f\xe0\xa4vl\
\xad\xa7(T\x1d\xe1\xc9\xce\xeaw\xe5\xed\x8c\x82\x01 &\xf2\x99~?\x83\xfb\x8f\
\x16a\x0b\xe5\xd7\x02ck\xe7\xc3\xaf\xbb\r\n\xa0T7\xaa\xd6G\xc2\x00\xc0\xe7\
\x16\x9f=[\xc6\x99\xd9\xf7Gm\xa1\xfc\xda\xa6;\xba\xfa\xf5\x03t\xd9?M)d\xcb\
\xa3\x910\x00\xfc\x02z\x7f@gy\xaf=\xef\x00\x00\x00\x00IEND\xaeB`\x82\xd6,G\
\x1e' )

def getPrivateTypeBitmap():
    return BitmapFromImage(getPrivateTypeImage())

def getPrivateTypeImage():
    stream = cStringIO.StringIO(getPrivateTypeData())
    return ImageFromStream(stream)

def getPrivateTypeIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getPrivateTypeBitmap())
    return icon

#----------------------------------------------------------------------
def getUnknownData():
    return zlib.decompress(
"x\xda\xeb\x0c\xf0s\xe7\xe5\x92\xe2b``\xe0\xf5\xf4p\t\x02\xd2\x02 \xcc\xc1\
\x06$\xe5?\xffO\x04R,\xc5N\x9e!\x1c@P\xc3\x91\xd2\x01\xe4\xb7{\xba8\x86X\xf4\
\x9e\xbd\xb0\x96\xf7P\x82H\xebCu\x96\xc0\x83\x1c<\x01\x9d.\n\xda\x92\x9d\xb6\
\x12\x8b\x98C\x94&x\x1cs\x08S\xd0\xeaz`\x969K\xadF\x9e\xf1\xd0\xec\x98\xe2\
\xc9\x9f'\x1f\xde\xbf\xc9\xb1\xd9\xdajN\xe0\xd2.\xef-/\xd4\xaf'\xceO\nh\x7f\
\xd0b\xca%\xf3\xa8\xdc\xd7P\xe2\xd2\xe9\x8b\xf9\x96Y\x93|\x1b\x93;\xf4w>\x9a\
\xfa\xbc\xa1\xd6\xe4\xf63)\x9e\x93\xbae;\x93v\xb4:^\xfaU\xbc\xc7\xf6\xce\xf1\
\x8a\x9b\t\x1f\x17+\xc5?a\xfc\xa8\xa781\xf8\xedT\x90K\x19<]\xfd\\\xd69%4\x01\
\x00p\x87M_" )

def getUnknownBitmap():
    return BitmapFromImage(getUnknownImage())

def getUnknownImage():
    stream = cStringIO.StringIO(getUnknownData())
    return ImageFromStream(stream)

def getUnknownIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(getUnknownBitmap())
    return icon

