#----------------------------------------------------------------------------
# Name:         xmlutils.py
# Purpose:      XML and Marshaller Utilities
#
# Author:       Jeff Norton
#
# Created:      6/2/05
# CVS-ID:       $Id: xmlutils.py,v 1.1.2.1 2005/12/20 02:12:01 RD Exp $
# Copyright:    (c) 2004-2005 ActiveGrid, Inc.
# License:      wxWindows License
#----------------------------------------------------------------------------

from lib.util.lang import *
import os
import time
import urllib
import logging
from lib.util.lang import *
import lib.util.objutils as objutils
import lib.util.xmlmarshaller as xmlmarshaller
import lib.util.aglogging as aglogging

xmlLogger = logging.getLogger("lib.util.xml")
    
def load(fileName, knownTypes=None, knownNamespaces=None):
    loadedObject = None
    fileObject = file(fileName)
    timeStart = time.time()
    try:
        xml = fileObject.read()
        loadedObject = unmarshal(xml, knownTypes=knownTypes, knownNamespaces=knownNamespaces, xmlSource=fileName)
        loadedObject.fileName = os.path.abspath(fileName)
        if hasattr(loadedObject, 'initialize'):
            loadedObject.initialize()
    finally:
        fileObject.close()
        timeDone = time.time()
        aglogging.info(xmlLogger, ('Load statistics for file %s: elapsed time = %f secs' % (fileName, timeDone-timeStart)))
    return loadedObject

def loadURI(uri, knownTypes=None, knownNamespaces=None, xmlSource=None):
    loadedObject = None
    xml = urllib.urlopen(uri).read()
    loadedObject = unmarshal(xml, knownTypes=knownTypes, knownNamespaces=knownNamespaces, xmlSource=xmlSource)
    loadedObject.fileName = uri
    if hasattr(loadedObject, 'initialize'):
        loadedObject.initialize()
    return loadedObject

def unmarshal(xml, knownTypes=None, knownNamespaces=None, xmlSource=None):
    if (knownTypes == None): 
        knownTypes, knownNamespaces = getAgKnownTypes()
    return xmlmarshaller.unmarshal(xml, knownTypes=knownTypes, knownNamespaces=knownNamespaces, xmlSource=xmlSource)    

def save(fileName, objectToSave, prettyPrint=True, marshalType=True, knownTypes=None, knownNamespaces=None, encoding='utf-8'):
    if hasattr(objectToSave, '_xmlReadOnly') and objectToSave._xmlReadOnly == True:
        raise xmlmarshaller.MarshallerException('Error marshalling object to file "%s": object is marked "readOnly" and cannot be written' % (fileName))        
    timeStart = time.time()
    xml = marshal(objectToSave, prettyPrint=prettyPrint, marshalType=marshalType, knownTypes=knownTypes, knownNamespaces=knownNamespaces, encoding=encoding)
    fileObject = file(fileName, 'w')
    try:
        fileObject.write(xml)
        fileObject.flush()
    except Exception, errorData:
        fileObject.close()
        raise xmlmarshaller.MarshallerException('Error marshalling object to file "%s": %s' % (fileName, str(errorData)))
    fileObject.close()
    timeDone = time.time()
    aglogging.info(xmlLogger, ('Save statistics for file %s: elapsed time = %f secs' % (fileName, timeDone-timeStart)))
    
def marshal(objectToSave, prettyPrint=True, marshalType=True, knownTypes=None, knownNamespaces=None, encoding='utf-8'):
    if (knownTypes == None): 
        knownTypes, knownNamespaces = getAgKnownTypes()
    return xmlmarshaller.marshal(objectToSave, prettyPrint=prettyPrint, marshalType=marshalType, knownTypes=knownTypes, knownNamespaces=knownNamespaces, encoding=encoding)
    
def addNSAttribute(xmlDoc, shortNamespace, longNamespace):
    if not hasattr(xmlDoc, "__xmlnamespaces__"):
        xmlDoc.__xmlnamespaces__ = {shortNamespace:longNamespace}
    elif shortNamespace not in xmlDoc.__xmlnamespaces__:
        if (hasattr(xmlDoc.__class__, "__xmlnamespaces__") 
            and (xmlDoc.__xmlnamespaces__ is xmlDoc.__class__.__xmlnamespaces__)):
            xmlDoc.__xmlnamespaces__ = dict(xmlDoc.__xmlnamespaces__)
        xmlDoc.__xmlnamespaces__[shortNamespace] = longNamespace

def genShortNS(xmlDoc, longNamespace=None):
    if not hasattr(xmlDoc, "__xmlnamespaces__"):
        return "ns1"
    elif longNamespace != None and longNamespace in xmlDoc.__xmlnamespaces__.items():
        for key, value in xmlDoc.__xmlnamespaces__.iteritems():
            if value == longNamespace:
                return key
    i = 1
    while ("ns%d" % i) in xmlDoc.__xmlnamespaces__:
        i += 1
    return ("ns%d" % i)
    
def genTargetNS(fileName, applicationName=None, type=None):
    if (applicationName != None):
        if (type != None):
            tns = "urn:%s:%s:%s" % (applicationName, type, fileName)
        else:
            tns = "urn:%s:%s" % (applicationName, fileName)
    else:
        tns = "urn:%s" % fileName
    return tns
    
def splitType(typeName):
    index = typeName.rfind(':')
    if index != -1:
        ns = typeName[:index]
        complexTypeName = typeName[index+1:]
    else:
        ns = None
        complexTypeName = typeName
    return (ns, complexTypeName)
        
def cloneObject(objectToClone, knownTypes=None, marshalType=True, knownNamespaces=None, encoding='utf-8'):
    if (knownTypes == None): 
        knownTypes, knownNamespaces = getAgKnownTypes()
    xml = xmlmarshaller.marshal(objectToClone, prettyPrint=True, marshalType=marshalType, knownTypes=knownTypes, knownNamespaces=knownNamespaces, encoding=encoding)
    clonedObject = xmlmarshaller.unmarshal(xml, knownTypes=knownTypes, knownNamespaces=knownNamespaces)
    if hasattr(objectToClone, 'fileName'):
        clonedObject.fileName = objectToClone.fileName
    if hasattr(objectToClone, "_parentDoc"):
        clonedObject._parentDoc = objectToClone._parentDoc
    try:
        clonedObject.initialize()
    except AttributeError:
        pass
    return clonedObject

def getAgVersion(fileName):
    fileObject = file(fileName)
    try:
        xml = fileObject.read()
    finally:
        fileObject.close()
    i = xml.find(' ag:version=')
    if i >= 0:
        i += 12
    else:
        i2 = xml.find('<ag:')
        if i2 >= 0:
            i = xml.find(' version=', i2)
            if i > 0:
                i += 9
        elif xml.find('<project version="10"') >= 0:
            return "10"
        else:
            return None
    version = None
    if xml[i:i+1] == '"':
        j = xml.find('"', i+1)
        if (j > i+1):
            version = xml[i+1:j]
    return version

def escape(data):
    """Escape ', ", &, <, and > in a string of data.

    Basically, everything that saxutils.escape does (and this calls that, at
    least for now), but with " added as well.

    XXX TODO make this faster; saxutils.escape() is really slow
    """

    import xml.sax.saxutils as saxutils

    data=saxutils.escape(data)
    data=data.replace("\"", "&quot;")

    # IE doesn't support &apos;
    # data=data.replace("\'", "&apos;")
    data=data.replace("\'", "&#039;")

    return data

def unescape(data):
    """Unescape ', ", &, <, and > in a string of data.

    Basically, everything that saxutils.unescape does (and this calls that, at
    least for now), but with " added as well.

    XXX TODO make this faster; saxutils.unescape() is really slow
    """

    import xml.sax.saxutils as saxutils

    data=data.replace("&quot;", "\"")
    data=data.replace("&apos;", "\'")
    return saxutils.unescape(data)

    
AG_NS_URL = "http://www.activegrid.com/ag.xsd"
SA_NS_URL = "http://www.softarchi.com/ag.xsd"
BPEL_NS_URL = "http://schemas.xmlsoap.org/ws/2003/03/business-process"
HTTP_WSDL_NS_URL = "http://schemas.xmlsoap.org/wsdl/http/"
MIME_WSDL_NS_URL = "http://schemas.xmlsoap.org/wsdl/mime/"
SOAP_NS_URL = "http://schemas.xmlsoap.org/wsdl/soap/"
SOAP12_NS_URL = "http://schemas.xmlsoap.org/wsdl/soap12/"
WSDL_NS_URL = "http://schemas.xmlsoap.org/wsdl/"
XFORMS_NS_URL = "http://www.w3c.org/xform.xsd"
XMLSCHEMA_NS_URL = "http://www.w3.org/2001/XMLSchema"
XSI_NS_URL = "http://www.w3.org/2001/XMLSchema-instance"
XACML_NS_URL = "urn:oasis:names:tc:xacml:2.0:policy:schema:os"

KNOWN_NAMESPACES = { AG_NS_URL          :  "ag",
                     SA_NS_URL          :  "sa",
                     BPEL_NS_URL        :  "bpws",
                     HTTP_WSDL_NS_URL   :  "http",
                     MIME_WSDL_NS_URL   :  "mime",
                     SOAP_NS_URL        :  "soap",
                     SOAP12_NS_URL      :  "soap12",
                     WSDL_NS_URL        :  "wsdl", 
                     XFORMS_NS_URL      :  "xforms",                             
                     XMLSCHEMA_NS_URL   :  "xs",
                     XACML_NS_URL       :  "xacml",
                   }
    
global agXsdToClassName
agXsdToClassName = None
def getAgXsdToClassName():
    global agXsdToClassName
    if (agXsdToClassName == None):
        agXsdToClassName = {
            "ag:append"          : "lib.model.processmodel.AppendOperation",
            "ag:attribute"       : "lib.model.identitymodel.Attribute",
            "ag:body"            : "lib.model.processmodel.Body",
            "ag:category_substitutions"    : "lib.server.layoutrenderer.CategorySubstitutions",
            "ag:command"         : "lib.model.wsdl.Command",
            "ag:css"             : "lib.server.layoutrenderer.CSS", 
            "ag:cssRule"         : "lib.model.processmodel.CssRule",
            "ag:databaseService" : "lib.server.deployment.DatabaseService",
            "ag:datasource"      : "lib.data.dataservice.DataSource",
            "ag:dataObjectList"  : "lib.data.datalang.DataObjectList",
            "ag:debug"           : "lib.model.processmodel.DebugOperation",
            "ag:deployment"      : "lib.server.deployment.Deployment",
            "ag:generator"       : "lib.server.layoutrenderer.SerializableGenerator", 
            "ag:head"            : "lib.server.layoutrenderer.Head", 
            "ag:hr"              : "lib.model.processmodel.HorizontalRow",
            "ag:identity"        : "lib.model.identitymodel.Identity",
            "ag:identityref"     : "lib.server.deployment.IdentityRef",
            "ag:image"           : "lib.model.processmodel.Image",
            "ag:label"           : "lib.model.processmodel.Label",
            "ag:layout"          : "lib.server.layoutrenderer.Layout", 
            "ag:layouts"         : "lib.server.layoutrenderer.Layouts", 
            "ag:ldapsource"      : "lib.model.identitymodel.LDAPSource",
            "ag:localService"    : "lib.server.deployment.LocalService",
            "ag:parameter"       : "lib.server.layoutrenderer.Parameter",
            "ag:parameters"      : "lib.server.layoutrenderer.Parameters",
            "ag:processref"      : "lib.server.deployment.ProcessRef",
            "ag:query"           : "lib.model.processmodel.Query",
            "ag:soapService"     : "lib.server.deployment.SoapService",
            "ag:requiredFile"    : "lib.server.layoutrenderer.RequiredFile", 
            "ag:resource"        : "lib.model.identitymodel.IDResource",
            "ag:restService"     : "lib.server.deployment.RestService",
            "ag:rewrite"         : "lib.model.wsdl.Rewrite",
            "ag:role"            : "lib.model.identitymodel.IDRole",
            "ag:roledefn"        : "lib.model.identitymodel.RoleDefn",
            "ag:rssService"      : "lib.server.deployment.RssService",
            "ag:rule"            : "lib.model.identitymodel.IDRule",
            "ag:schemaOptions"   : "lib.model.schema.SchemaOptions",
            "ag:schemaref"       : "lib.server.deployment.SchemaRef",
            "ag:serviceCache"    : "lib.server.deployment.ServiceCache",
            "ag:serviceExtension": "lib.model.wsdl.ServiceExtension",
            "ag:serviceExtensions": "lib.model.wsdl.ServiceExtensions",
            "ag:serviceParameter": "lib.server.deployment.ServiceParameter",
            "ag:serviceref"      : "lib.server.deployment.ServiceRef",
            "ag:set"             : "lib.model.processmodel.SetOperation",
            "ag:skinref"         : "lib.server.deployment.SkinRef",
            "ag:skin"            : "lib.server.layoutrenderer.Skin",
            "ag:skin_element_ref": "lib.server.layoutrenderer.SkinElementRef",
            "ag:skin_element"    : "lib.server.layoutrenderer.SkinElement",
            "ag:skins"           : "lib.server.layoutrenderer.Skins",
            "ag:substitution"    : "lib.server.layoutrenderer.Substitution", 
            "ag:text"            : "lib.model.processmodel.Text",
            "ag:title"           : "lib.model.processmodel.Title",
            "ag:usertemplate"    : "lib.model.identitymodel.UserTemplate",
            "ag:xformref"        : "lib.server.deployment.XFormRef",
            "bpws:case"          : "lib.model.processmodel.BPELCase",
            "bpws:catch"         : "lib.model.processmodel.BPELCatch",
            "bpws:faultHandlers" : "lib.model.processmodel.BPELFaultHandlers",
            "bpws:flow"          : "lib.model.processmodel.BPELFlow",
            "bpws:invoke"        : "lib.model.processmodel.BPELInvoke",
            "bpws:onMessage"     : "lib.model.processmodel.BPELOnMessage",
            "bpws:otherwise"     : "lib.model.processmodel.BPELOtherwise",
            "bpws:pick"          : "lib.model.processmodel.BPELPick",
            "bpws:process"       : "lib.model.processmodel.BPELProcess",
            "bpws:receive"       : "lib.model.processmodel.BPELReceive",
            "bpws:reply"         : "lib.model.processmodel.BPELReply",
            "bpws:scope"         : "lib.model.processmodel.BPELScope",
            "bpws:sequence"      : "lib.model.processmodel.BPELSequence",
            "bpws:switch"        : "lib.model.processmodel.BPELSwitch",
            "bpws:terminate"     : "lib.model.processmodel.BPELTerminate",
            "bpws:variable"      : "lib.model.processmodel.BPELVariable",
            "bpws:variables"     : "lib.model.processmodel.BPELVariables",
            "bpws:while"         : "lib.model.processmodel.BPELWhile",
            "http:address"       : "lib.model.wsdl.HttpAddress",
            "http:binding"       : "lib.model.wsdl.HttpBinding",
            "http:operation"     : "lib.model.wsdl.HttpOperation",
            "http:urlEncoded"    : "lib.model.wsdl.HttpUrlEncoded",
            "mime:content"       : "lib.model.wsdl.MimeContent",
            "mime:mimeXml"       : "lib.model.wsdl.MimeMimeXml",
            "soap:address"       : "lib.model.wsdl.SoapAddress",
            "soap:binding"       : "lib.model.wsdl.SoapBinding",
            "soap:body"          : "lib.model.wsdl.SoapBody",
            "soap:fault"         : "lib.model.wsdl.SoapFault",
            "soap:header"        : "lib.model.wsdl.SoapHeader",
            "soap:operation"     : "lib.model.wsdl.SoapOperation",
            "soap12:address"     : "lib.model.wsdl.Soap12Address",
            "soap12:binding"     : "lib.model.wsdl.Soap12Binding",
            "soap12:body"        : "lib.model.wsdl.Soap12Body",
            "soap12:fault"       : "lib.model.wsdl.Soap12Fault",
            "soap12:header"      : "lib.model.wsdl.Soap12Header",
            "soap12:operation"   : "lib.model.wsdl.Soap12Operation",
            "wsdl:binding"       : "lib.model.wsdl.WsdlBinding",
            "wsdl:definitions"   : "lib.model.wsdl.WsdlDocument",
            "wsdl:documentation" : "lib.model.wsdl.WsdlDocumentation",
            "wsdl:fault"         : "lib.model.wsdl.WsdlFault",
            "wsdl:import"        : "lib.model.wsdl.WsdlImport",
            "wsdl:input"         : "lib.model.wsdl.WsdlInput",
            "wsdl:message"       : "lib.model.wsdl.WsdlMessage",
            "wsdl:operation"     : "lib.model.wsdl.WsdlOperation",
            "wsdl:output"        : "lib.model.wsdl.WsdlOutput",
            "wsdl:part"          : "lib.model.wsdl.WsdlPart",
            "wsdl:port"          : "lib.model.wsdl.WsdlPort",
            "wsdl:portType"      : "lib.model.wsdl.WsdlPortType",
            "wsdl:service"       : "lib.model.wsdl.WsdlService",
            "wsdl:types"         : "lib.model.wsdl.WsdlTypes",
            "xacml:Action"       : "lib.model.identitymodel.XACMLAction",
            "xacml:ActionAttributeDesignator" : "lib.model.identitymodel.XACMLActionAttributeDesignator",
            "xacml:ActionMatch"  : "lib.model.identitymodel.XACMLActionMatch",
            "xacml:Actions"      : "lib.model.identitymodel.XACMLActions",
            "xacml:AttributeValue" : "lib.model.identitymodel.XACMLAttributeValue",
            "xacml:Policy"       : "lib.model.identitymodel.XACMLPolicy",
            "xacml:Resource"     : "lib.model.identitymodel.XACMLResource",
            "xacml:ResourceAttributeDesignator" : "lib.model.identitymodel.XACMLResourceAttributeDesignator",
            "xacml:ResourceMatch" : "lib.model.identitymodel.XACMLResourceMatch",
            "xacml:Resources"    : "lib.model.identitymodel.XACMLResources",
            "xacml:Rule"         : "lib.model.identitymodel.XACMLRule",
            "xacml:Target"       : "lib.model.identitymodel.XACMLTarget",
            "xforms:copy"        : "lib.model.processmodel.XFormsCopy",
            "xforms:group"       : "lib.model.processmodel.XFormsGroup",
            "xforms:include"     : "lib.model.processmodel.XFormsInclude",
            "xforms:input"       : "lib.model.processmodel.XFormsInput",
            "xforms:item"        : "lib.model.processmodel.XFormsItem",
            "xforms:itemset"     : "lib.model.processmodel.XFormsItemset",
            "xforms:label"       : "lib.model.processmodel.XFormsLabel",
            "xforms:model"       : "lib.model.processmodel.XFormsModel",
            "xforms:output"      : "lib.model.processmodel.XFormsOutput",
            "xforms:secret"      : "lib.model.processmodel.XFormsSecret",
            "xforms:select1"     : "lib.model.processmodel.XFormsSelect1",
            "xforms:submission"  : "lib.model.processmodel.XFormsSubmission",
            "xforms:submit"      : "lib.model.processmodel.XFormsSubmit",
            "xforms:value"       : "lib.model.processmodel.XFormsValue",
            "xforms:xform"       : "lib.model.processmodel.View",
            "xforms:xforms"      : "lib.model.processmodel.XFormsRoot",
            "xs:all"             : "lib.model.schema.XsdSequence",
            "xs:any"             : "lib.model.schema.XsdAny",
            "xs:attribute"       : "lib.model.schema.XsdAttribute",
            "xs:complexContent"  : "lib.model.schema.XsdComplexContent",
            "xs:complexType"     : "lib.model.schema.XsdComplexType",
            "xs:element"         : "lib.model.schema.XsdElement",
            "xs:enumeration"     : "lib.model.schema.XsdEnumeration",
            "xs:extension"       : "lib.model.schema.XsdExtension",
            "xs:field"           : "lib.model.schema.XsdKeyField",
            "xs:import"          : "lib.model.schema.XsdInclude",
            "xs:include"         : "lib.model.schema.XsdInclude",
            "xs:key"             : "lib.model.schema.XsdKey",
            "xs:keyref"          : "lib.model.schema.XsdKeyRef",
            "xs:length"          : "lib.model.schema.XsdLength",
            "xs:list"            : "lib.model.schema.XsdList",
            "xs:maxLength"       : "lib.model.schema.XsdMaxLength",
            "xs:restriction"     : "lib.model.schema.XsdRestriction",
            "xs:schema"          : "lib.model.schema.Schema",
            "xs:selector"        : "lib.model.schema.XsdKeySelector",              
            "xs:sequence"        : "lib.model.schema.XsdSequence",
            "xs:simpleContent"   : "lib.model.schema.XsdSimpleContent",
            "xs:simpleType"      : "lib.model.schema.XsdSimpleType",
            "xs:totalDigits"     : "lib.model.schema.XsdTotalDigits",
        }
    return agXsdToClassName
    
global agKnownTypes
agKnownTypes = None
def getAgKnownTypes():
    global agKnownTypes
    if agKnownTypes == None:
        try:
            tmpAgKnownTypes = {}
            import lib.model.processmodel
            import lib.model.schema
            import lib.server.deployment
            import lib.model.wsdl
            ifDefPy()
            import lib.data.dataservice
            endIfDef()
            for keyName, className in getAgXsdToClassName().iteritems():
                classType = objutils.classForName(className)
                if (classType == None):
                    raise Exception("Cannot get class type for %s" % className)
                else:
                    tmpAgKnownTypes[keyName] = classType
            if len(tmpAgKnownTypes) > 0:
                agKnownTypes = tmpAgKnownTypes
        except ImportError:
            agKnownTypes = {}
    if len(agKnownTypes) == 0:     # standalone IDE and XmlMarshaller don't contain known AG types
        noKnownNamespaces = {}
        return agKnownTypes, noKnownNamespaces            
    return agKnownTypes, KNOWN_NAMESPACES
