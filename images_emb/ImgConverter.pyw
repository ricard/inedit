import sys,os
import  wx
import  wx.lib.filebrowsebutton as filebrowse
import img2py

#---------------------------------------------------------------------------
# Create and set a help provider.  Normally you would do this in
# the app's OnInit as it must be done before any SetHelpText calls.
provider = wx.SimpleHelpProvider()
wx.HelpProvider_Set(provider)

#---------------------------------------------------------------------------

class TestDialog(wx.Dialog):
    
    def __init__(
            self, parent, ID, title, size=wx.DefaultSize, pos=wx.DefaultPosition, 
            style=wx.DEFAULT_DIALOG_STYLE
            ):

        config = wx.ConfigBase_Get()
        assert(isinstance(config, wx.ConfigBase))
        
        # Instead of calling wx.Dialog.__init__ we precreate the dialog
        # so we can set an extra style that must be set before
        # creation, and then we create the GUI dialog using the Create
        # method.
        pre = wx.PreDialog()
        pre.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        pre.Create(parent, ID, title, pos, size, style)

        # This next step is the most important, it turns this Python
        # object into the real wrapper of the dialog (instead of pre)
        # as far as the wxPython extension is concerned.
        self.PostCreate(pre)

        # Now continue with the normal construction of the dialog
        # contents
        sizer = wx.BoxSizer(wx.VERTICAL)

        label = wx.StaticText(self, -1, "This is a wx.Dialog")
        label.SetHelpText("This is the help text for the label")
        sizer.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        self.fbbhSrc = filebrowse.FileBrowseButtonWithHistory(
            self, -1, size=(450, -1),  changeCallback = self.fbbhSrcCallback
            )
        self.fbbhSrc.SetName('Source')
        self.fbbhSrc.SetHelpText("Choose an image file to convert")
        self.fbbhSrc.SetHistory(eval(config.Read('SETTINGS/%sHistory' % self.fbbhSrc.GetName(), '[]')))
        self.fbbhSrc.SetValue(config.Read('SETTINGS/%s' % self.fbbhSrc.GetName()))
        sizer.Add(self.fbbhSrc, 0, wx.GROW|wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)

        label = wx.StaticText(self, -1, "Name:")
        label.SetHelpText("This is the help text for the label")
        box.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        self.tcName = wx.TextCtrl(self, -1, "", size=(80,-1))
        self.tcName.SetHelpText("Enter here the name that should be used to customize the access fucntions, (getNameBitmap, etc.)")
        self.tcName.SetValue(config.Read('SETTINGS/Name'))
        box.Add(self.tcName, 1, wx.ALIGN_CENTRE|wx.ALL, 5)

        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        self.cbCat = wx.CheckBox(self, -1, "Maintain a catalog", name = 'Catalogue')
        self.cbAppend = wx.CheckBox(self, -1, "Append to existing file", name = 'Append')
        self.cbCompress = wx.CheckBox(self, -1, "Don't use compression", name = 'DontCompress')
        self.cbIcon = wx.CheckBox(self, -1, "Add a getIcon() function", name = 'UseIcon')
        self.cbCat.SetValue(config.ReadBool('SETTINGS/Catalogue', False))
        self.cbAppend.SetValue(config.ReadBool('SETTINGS/Append', False))
        self.cbCompress.SetValue(config.ReadBool('SETTINGS/DontCompress', False))
        self.cbIcon.SetValue(config.ReadBool('SETTINGS/UseIcon', False))
        self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.cbCat)
        self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.cbAppend)
        self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.cbCompress)
        self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.cbIcon)
        sizer.Add(self.cbCat, 0, wx.LEFT, 100)
        sizer.Add(self.cbAppend, 0, wx.LEFT, 100)
        sizer.Add(self.cbCompress, 0, wx.LEFT, 100)
        sizer.Add(self.cbIcon, 0, wx.LEFT, 100)

        self.fbbhDst = filebrowse.FileBrowseButtonWithHistory(
            self, -1, size=(450, -1), changeCallback = self.fbbhDstCallback
            )
        self.fbbhDst.SetName('Destination')
        self.fbbhDst.SetHelpText("Choose an destination file")
        self.fbbhDst.SetHistory(eval(config.Read('SETTINGS/%sHistory' % self.fbbhDst.GetName(), '[]')))
        self.fbbhDst.SetValue(config.Read('SETTINGS/%s' % self.fbbhDst.GetName()))
        sizer.Add(self.fbbhDst, 0, wx.GROW|wx.ALL, 5)

        self.tcLog = wx.TextCtrl(self, -1, '', wx.DefaultPosition, wx.Size(-1, 100), wx.TE_MULTILINE)
        sizer.Add(self.tcLog, 1, wx.GROW | wx.ALL, 5)
        
        line = wx.StaticLine(self, -1, size=(20,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)

        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        
        if wx.Platform != "__WXMSW__":
            btn = wx.ContextHelpButton(self)
            btnsizer.Add(btn, 0, wx.ALL, 10)
        
        btn = wx.Button(self, -1, "Convert")
        btn.SetHelpText("Do the image convertion")
        btn.SetDefault()
        self.Bind(wx.EVT_BUTTON, self.OnConvert, btn)
        btnsizer.Add(btn, 0, wx.ALL, 10)

        btn = wx.Button(self, wx.ID_CANCEL)
        btn.SetHelpText("The Cancel button cnacels the dialog. (Cool, huh?)")
        btnsizer.Add(btn, 0, wx.ALL, 10)

        sizer.Add(btnsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def fbbhSrcCallback(self, evt):
        value = evt.GetString()
        if not value:
            return
        self.SaveFileBrowserHistory(self.fbbhSrc)
    
    def fbbhDstCallback(self, evt):
        value = evt.GetString()
        if not value:
            return
        self.SaveFileBrowserHistory(self.fbbhDst)

    def SaveFileBrowserHistory(self, fbbh):
        config = wx.ConfigBase_Get()
        name = fbbh.GetName()
        value = fbbh.GetValue()
        config.Write('SETTINGS/%s' % name, value)
        history = fbbh.GetHistory()
        if value not in history:
            history.append(value)
            fbbh.SetHistory(history)
            fbbh.GetHistoryControl().SetStringSelection(value)
            config.Write('SETTINGS/%sHistory' % name, repr(fbbh.GetHistory()))

    def EvtCheckBox(self, event):
        config = wx.ConfigBase_Get()
        cb = event.GetEventObject()
        assert(isinstance(cb, wx.CheckBox))
        config.WriteBool('SETTINGS/%s' % cb.GetName(), cb.GetValue())
    
    def OnConvert(self, event):
        config = wx.ConfigBase_Get()
        config.Write('SETTINGS/Name', self.tcName.GetValue())
        args = []
        if self.cbCat.IsChecked():
            args.append('-c')
        if self.cbAppend.IsChecked():
            args.append('-a')
        if self.cbCompress.IsChecked():
            args.append('-u')
        if self.cbIcon.IsChecked():
            args.append('-i')
        name = self.tcName.GetValue()
        if name:
            args.extend(['-n', name])
        args.extend([self.fbbhSrc.GetValue(), self.fbbhDst.GetValue()])
        sys.stdout = self.tcLog
        sys.stderr = self.tcLog
        img2py.main(args)
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
        

#---------------------------------------------------------------------------

        

#---------------------------------------------------------------------------

class MyApp(wx.App):
    def OnInit(self):
        self.SetAppName('ImgConverter')
        self.SetVendorName('SoftArchi')
        config = wx.FileConfig(localFilename = 'ImgConverter.conf', 
                               style = wx.CONFIG_USE_LOCAL_FILE | wx.CONFIG_USE_RELATIVE_PATH | wx.CONFIG_USE_NO_ESCAPE_CHARACTERS)
        wx.ConfigBase_Set(config)
        dlg = TestDialog(None, -1, "This is a Dialog", size=(350, 200),
                         #style = wxCAPTION | wxSYSTEM_MENU | wxTHICK_FRAME
                         style = wx.DEFAULT_DIALOG_STYLE
                         )
        dlg.CenterOnScreen()

        self.SetTopWindow(dlg)
        val = dlg.ShowModal()

        dlg.Destroy()
        config.Flush()
        return True
        

if __name__ == '__main__':
    app = MyApp(redirect=False)
    app.MainLoop()

