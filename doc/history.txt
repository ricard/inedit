************************************************
* [+] Nouvelle fonctionnalité/option 
* [-] Suppression d'une fonctionnalité/option 
* [*] Amélioration d'une fonctionnalité/option 
* [!] Correction de Bug
* [>] Notification importante
************************************************


***************************
* xx/03/2006 Version 0.3  * 
***************************
Build 40 (21/03/06)
[+] Documentation
[+] Début d'intégration de l'aide en ligne
[+] Folding : possibilité de replier les paragraphes du code source Anubis
[*] Corrections majeures dans le Navigateur de source, reconnaissance de plus de paragraphes
[*] Les marqueurs sont correctement restaurés à l'ouverture de l'application
[!] Correction fuite mémoire importante lors du parsing d'un fichier Anubis
Build 35 (07/02/06)
[+] Compilation Anubis : coloration des erreurs + ouverture du fichier incriminé sur double-clic
[+] Compilation Anubis : double clic sur une erreur ouvre le fichier concerné et se positionne
    à l'endroit de l'erreur
[+] Compilation Anubis : F4 / Shift + F4 permet de parcourir les erreurs de compilation
[*] Optimisation de la coloration lexicale
[+] Ajout d'onglets pour l'intégration des fenêtres d'outil, permettant d'avoir plusieurs outils 
    situés au même emplacement
[*] Les fenêtres outils possède chacune une icône dans leur onglet.
Build 34 (16/01/06)
[+] Exécution sommaire bytecode Anubis
[+] Ajout de la commande Make : compile le fichier de démarrage du projet courrant
[+] Possibilité de définir un fichier de démarrage, fichier qui sera compilé en cas de make 
    et exécuté en cas de run.
[+] Possibilité de définir des outils externes, appelable depuis In-Edit et pouvant s'appliquer
    au fichier courrant.
Build 33 (12/01/06)
[*] Refactoring gestion de projet avec double affichage arborescence virtuelle / réelle
    Un seul projet est visible à la fois
Build 32 (12/01/06)
[>] Maj vers Python 2.4.2 et wxWidgets 2.6.2.1
[+] Possibilité de définir la ligne de commande du compilateur Anubis
[+] Mise en évidence de la parenthèse, crochet ou accolade correspondante
[*] Utilisation du nouveau parser pour la fenêtre de configuration des styles
[!] L'impression de code source fonctionne
Build 29 (10/01/06)
[!] Utilisation du nouveau parser pour le navigateur de source
[!] Correction gros memory leaks du parser Anubis
Build 26 (05/01/06)
[!] Correction : CPU à 100% lorsqu'on tente d'afficher certain fichiers comportant des erreurs 
    de syntaxe
Build 24 (05/01/06)
[*] Réécriture du moteur d'analyse du code source Anubis, énorme optimisation, 
    amélioration de la coloration syntaxique 
[!] Correction de la fonction 'Add current file to project'
[*] Optimisations globales avec l'utilisation de Psyco
Build 19 (20/12/05)
[+] Fenêtre de configuration pour la coloration syntaxique
[*] Le navigateur de source Anubis supporte maintenant les commentaires placés entre chaque mots clés
[*] Amélioration de la coloration syntaxique du code Anubis 
**Bugs**
[!] Correction bug empèchant de créer de nouveaux projets
[!] Installation : raccourci se créant dans le menu 'Démarrage' au lieu du 'Demarrage rapide'
Build 18 (15/12/05)
[+] Chargement / Sauvegarde en UTF-8
[+] Détecter l'encodage automatiquement au chargement
[+] Sauver les Bookmarks
[+] Visualisation des fichiers modifiés
[+] Retenir les fichiers précédemment ouverts
[+] Gestion des fins de lignes (CR / LF / CR+LF)
[+] Double-clic sur fenêtre de sortie : va directement au fichier désigné
[*] Améliorer la gestion de projets
	[+] Définition d'un projet de démarrage
	[+] Permettre la définition du fichier de démarrage
	[+] Sauvegarde des arguments et répertoire d'exécution par projet
	[+] Affichage de l'arborescence des répertoires

[+] Compilation source Anubis
[+] Package d'installation
[+] Navigateur de source pour Anubis
**Bugs**
[!] Amélioration de la gestion des documents avec accents
[!] Réparer les guides d'indentation
[!] Recherche se décallant d'un caractère si un accent est présent dans le source AVANT la chaine recherchée

***************************
* 01/06/2005 Version 0.2  * 
***************************
[+] Intégration archi pydocview + ActiveGrid
[+] Raccourcis claviers standard (ouverture / fermeture)
[+] Recherche rapide avec F3 (mode VisualC)
[+] Positionnement de marqueur (F2)

***************************
* 15/05/2005 Version 0.1  * 
***************************
[+] Essais d'utilisation du wxStyledTextCtrl
[+] Tentative de coloration syntaxique

