/* lexer.y *******************************************************************************

                                             Anubis 1. 
                                    The lexer of the compiler. 

*****************************************************************************************/

/*
    This LEX/FLEX file is the lexer for the Anubis compiler. 
*/ 


%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Python.h"
typedef struct _YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
  int start_pos;
  int end_pos;
} _YYLTYPE;
#define YYLTYPE _YYLTYPE
#define YYSTYPE void *
#include "tokens.h"
extern void *py_parser;
extern void (*py_input)(PyObject *parser, char *buf, int *result, int max_size);
#define returntoken(tok) \
    *pyylval = PyString_FromFormat("%d:%d:%d:%d:%s", lineno, colno, tokenStart, yyoffset, yytext); \
    pyylloc->first_line = linenoStart; \
    pyylloc->first_column = colnoStart; \
    pyylloc->last_line = lineno; \
    pyylloc->last_column = colno; \
    pyylloc->start_pos = tokenStart; \
    pyylloc->end_pos = yyoffset; \
    tokenStart = yyoffset; \
    return (tok)

#define YY_INPUT(buf,result,max_size) {(*py_input)(py_parser, buf, &result, max_size);}


//#include "compil.h"

#define line_in(x)       (((x)>>9)&0x7FFF)
#define col_in(x)        (((x)>>1)&0xFF)
#define new_integer(n)           ((n)<<1)

#define errfile stderr

int current_par_line = 0;      /* line number of first line of paragraph currently read */ 
int linenoStart = 0;
int colnoStart = 0;
extern int yyoffset;
extern int tokenStart;
extern int lineno;
extern int colno; 

   
/* Constructing the lexer */   
//int yylex(void);        /* this  is the  function called  by yyparse  (it is  defined just
//                           below) */ 

int linecol(void);
void updateTokenStart();
   
   
   
/* YY_PROTO(( void )) */

   
   
/* Managing character  strings. We  use the  buffer 'str_buf' for  reading the  content of
   character strings. */ 
#define str_buf_size (10000)
int str_index = 0; 
char str_buf[str_buf_size]; 

/* This function is  used for storing one  character into the buffer. It  is defined after
   the lexer proper, because it uses the definition of the state 'STRTL'. */ 
void store_str_char(char);

   
   
   
/* When we read a new file, we must push into stacks a lot of informations. First of all a
   YY_BUFFER_STATE, because FLEX  needs to be able  to return to the previous  file at the
   right place. We  must push the path of  the file currently being read,  and the current
   line number in the previous file. The  current directory is managed by another stack in
   'mallocz.c'.  */ 
   
   
/* the number of nested includes is limited to 100. */    
//#define                  max_include      (100)
      
//YY_BUFFER_STATE      include_stack[max_include];   /* buffer states for FLEX */ 
//char *               include_names[max_include];   /* absolute paths of files */ 
//int                  include_lines[max_include];   /* current line number in files */ 
//char *               include_dir[max_include];     /* current directory */ 

//int                  include_stack_ptr = 0;        /* common stack pointer */

/* The 4 stacks: 'include_stack'
                 'include_lines'
                 'include_names'
                 'include_dir' 
   
   are growing in parallel. The common stack pointer is 'include_stack_ptr'. */    
   
//YY_BUFFER_STATE new_yy_buffer; 



 

void err_line_col(int lc)
{
	fprintf(errfile, "ERROR in Line %d, Col %d, token '%s'\n", line_in(lc), col_in(lc), yytext); 
}



   
   
   
//#define YY_NO_UNPUT


/* counting lines and columns -------------------------------------------------*/
int comlevel = 0;
int bracelevel = 0;
int bracketlevel = 0;
int tab_seen = 0; 
int tab_width = 4; 


/* updating line and column counters ---------------------------------------------*/
void update_lc(void)
{
  int i = 0;
  linenoStart = lineno;
  colnoStart = colno;
  //fprintf(errfile, "STATE %d %d %d --> '%s'\n", YY_START, bracelevel, bracketlevel, yytext);
  while (yytext[i] != 0)
  {
    if (yytext[i] == '\t') { tab_seen = 1; colno += tab_width; }
    if (yytext[i] == '\n') { lineno++; colno = 1; }
    else                   colno++; 
    i++; 
  }
  yyoffset += i;
}

#define YY_USER_ACTION      	update_lc(); 


#define YY_DECL int yylex ( YYSTYPE * pyylval, YYLTYPE * pyylloc )

/* return current line, column and file id into an expression integer -------------*/ 
int linecol(void)
{
  return new_integer((lineno<<8)|colno); 
}

void lexical_error(void)
{
  err_line_col(linecol()); 
}

void lexical_kwread_error(void)
{
	lexical_error();
}


%}

%option noyywrap



/* states ------------------------------------------------------------------------------*/
%x COM
%x PAR
%x INCL
%x STR
%x STRTL
   
/* state STRTL is used when a string is too long. */ 

/* the lexer ----------------------------------------------------------------------------*/
%%
<PAR>"/*"                         { comlevel = 1; BEGIN COM; }
<COM>"/*"                         { comlevel++; }
<COM>"*/"                         { comlevel--; if (!comlevel) { BEGIN PAR; } }
<COM>\n                           { }
<COM>.                            { }
<PAR>\/\/.*$                      { }
<PAR>\"                           { BEGIN STR; updateTokenStart(); tokenStart -= 1; colno -= 1; str_index = 0; }
<STR>\"                           { BEGIN PAR; 
                                    str_buf[str_index] = 0; 
                                    yytext = str_buf;
                                    returntoken( yy__anb_string ); }
<STR>\\\"                         { store_str_char('\"'); }
<STR>\\n                          { store_str_char('\n'); }
<STR>\\r                          { store_str_char('\r'); }
<STR>\\t                          { store_str_char('\t'); }
<STR>\\\\                         { store_str_char('\\'); }
<STR>\n                           { store_str_char('\n'); }
<STR>.                            { store_str_char(yytext[0]); }
<STRTL>\"                         { BEGIN PAR; returntoken( yy__anb_string );}
<STRTL>\n                         { }
<STRTL>.                          { }
<PAR>\'[^\'\n\t\r\"]\'            { returntoken( yy__char ); }
<PAR>\'\\n\'                      { returntoken( yy__char ); }
<PAR>\'\\r\'                      { returntoken( yy__char ); }
<PAR>\'\\t\'                      { returntoken( yy__char ); }
<PAR>\'\\\"\'                     { returntoken( yy__char ); }
<PAR>\'\\\\\'                     { returntoken( yy__char ); }
<PAR>\'\\\'\'                     { returntoken( yy__char ); }
<PAR>"("                               { bracketlevel++; returntoken( yy__lpar ); }
<PAR>")"                               { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rpar ); }
                                         else lexical_error(); }
<PAR>\,[\ \t\r\n]*\)                   { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rpar ); }
                                         else lexical_error(); }
<PAR>"{"                               { bracelevel++; returntoken( yy__lbrace ); }
<PAR>"}"                               { if(bracelevel > 0) { bracelevel--; returntoken( yy__rbrace ); } 
                                         else lexical_error(); }
<PAR>\,[\ \t\r\n]*\}                   { if(bracelevel > 0) { bracelevel--; returntoken( yy__rbrace ); }
                                         else lexical_error(); }
<PAR>"["                               { bracketlevel++; returntoken( yy__lbracket ); }
<PAR>"]"                               { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rbracket ); }
                                         else lexical_error(); }
<PAR>\,[\t\n\r\ ]*\]                   { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rbracket ); }
                                         else lexical_error(); }
<PAR>":"                               { returntoken( yy__colon ); }
<PAR>";"                               { returntoken( yy__semicolon ); }
<PAR>"-"                               { returntoken( yy__minus ); }
<PAR>"."                               { if(bracketlevel == 0 && bracelevel == 0) BEGIN(INITIAL); 
                                         returntoken( yy__dot ); }
<PAR>\%                                { returntoken( yy__percent ); }
<PAR>","                               { returntoken( yy__comma ); }
<PAR>"~"                               { returntoken( yy__tilde ); }
<PAR>"="                               { returntoken( yy__equals ); }
<PAR>"/="                              { returntoken( yy__non_equal ); }
<PAR>"+"                               { returntoken( yy__plus ); }
<PAR>"*"                               { returntoken( yy__star ); }
<PAR>"^"                               { returntoken( yy__carret ); }
<PAR>"\&"                              { returntoken( yy__ampersand ); }
<PAR>"|"                               { returntoken( yy__vbar ); }
<PAR>">>"                              { returntoken( yy__right_shift ); }
<PAR>"<<"                              { returntoken( yy__left_shift ); }
<PAR>"=>"                              { returntoken( yy__implies ); }
<PAR>"?"                               { returntoken( yy__forall ); }
<PAR>"!"                               { returntoken( yy__exists ); }
<PAR>"!!"                              { returntoken( yy__exists_unique ); }
<PAR>"#!"                              { returntoken( yy__description ); }
<PAR>"/"                               { returntoken( yy__slash ); }
<PAR>\([\t ]*mod[\t ]                  { bracketlevel++; returntoken( yy__mod ); }
<PAR>"<"                               { returntoken( yy__less ); }
<PAR>">"                               { returntoken( yy__greater ); }
<PAR>"=<"                              { returntoken( yy__lessoreq ); }
<PAR>">="                              { returntoken( yy__greateroreq ); }
<PAR>"<-"                              { returntoken( yy__write ); }
<PAR>"<->"                             { returntoken( yy__exchange ); }
<PAR>"->"                              { returntoken( yy__arrow ); }
<PAR>"->>"                             { returntoken( yy__arroww ); }
<PAR>"|->"                             { returntoken( yy__mapsto ); }
<PAR>"|->>"                            { returntoken( yy__mapstoo ); }
<PAR>"..."                             { if(bracketlevel == 0 && bracelevel == 0) BEGIN(INITIAL); 
                                         returntoken( yy__dots ); }
<PAR>\)[ \t\n]*\-\>                    { if(bracketlevel > 0) { bracketlevel--; returntoken( yy__rpar_arrow ); }
                                         else lexical_error(); }
<PAR>String                            { returntoken( yy__type_String ); }
<PAR>ByteArray                         { returntoken( yy__type_ByteArray ); }
<PAR>Int32                             { returntoken( yy__type_Int32 ); }
<PAR>Omega                             { returntoken( yy__type_Omega ); }
<PAR>bit_width                         { returntoken( yy__bit_width ); }
<PAR>is_indirect_type                  { returntoken( yy__indirect ); }
<PAR>\�vcopy                           { returntoken( yy__vcopy ); }
<PAR>\�load_module                     { returntoken( yy__load_module ); }
<PAR>\�serialize                       { returntoken( yy__serialize ); }
<PAR>\�unserialize                     { returntoken( yy__unserialize ); }
<PAR>Float                             { returntoken( yy__type_Float ); }
<PAR>\�Listener                        { returntoken( yy__type_Listener ); }
<PAR>\�StructPtr                       { returntoken( yy__StructPtr ); }
<PAR>\�avm                             { returntoken( yy__avm ); }
<PAR>if                                { returntoken( yy__if ); }
<PAR>^[Pp]roof                         { returntoken( yy__proof ); }
<PAR>Proof                             { returntoken( yy__type_Proof ); }
<PAR>since                             { returntoken( yy__since ); }
<PAR>is                                { returntoken( yy__is ); }
<PAR>then                              { returntoken( yy__then ); }
<PAR>alert                             { returntoken( yy__alert ); }
<PAR>protect                           { returntoken( yy__protect ); }
<PAR>lock                              { returntoken( yy__lock ); }
<PAR>succeeds                          { returntoken( yy__succeeds ); }
<PAR>succeeds[\ \t\n]+as               { returntoken( yy__succeeds_as ); }
<PAR>wait[\ \t\n]+for                  { returntoken( yy__wait_for ); }
<PAR>checking[\ \t\n]+every            { returntoken( yy__checking_every ); }
<PAR>delegate                          { returntoken( yy__delegate ); }
<PAR>else                              { returntoken( yy__else ); }
<PAR>with                              { returntoken( yy__with ); }
<PAR>alternative_number                { returntoken( yy__alt_number ); }
<PAR>RAddr                             { returntoken( yy__RAddr ); }
<PAR>WAddr                             { returntoken( yy__WAddr ); }
<PAR>RWAddr                            { returntoken( yy__RWAddr ); }
<PAR>GAddr                             { returntoken( yy__GAddr ); }
<PAR>Var                               { returntoken( yy__Var ); }
<PAR>MVar                              { returntoken( yy__MVar ); }
<PAR>connect[\ \t\n]+to[\ \t\n]+file    { returntoken( yy__connect_to_file ); }
<PAR>connect[\ \t\n]+to[\ \t\n]+network { returntoken( yy__connect_to_IP ); }
<PAR>debug                             { returntoken( yy__debug_avm ); }
<PAR>terminal                          { returntoken( yy__terminal ); }
<PAR>[Ww]e[\ \t\n]+have                { returntoken( yy__we_have ); }
<PAR>[Ii]t[\ \t\n]+is[\ \t\n]+enough[\ \t\n]+to[\ \t\n]+prove([\ \t\n]+that)? { 
                                         returntoken( yy__enough ); } 
<PAR>[Ll]et                            { returntoken( yy__let ); } 
<PAR>[Aa]ssume([\ \t\n]+that)?         { returntoken( yy__assume ); }
<PAR>[Hh]ence                          { returntoken( yy__hence ); } 
<PAR>[Ii]ndeed                         { returntoken( yy__indeed ); } 
^to[\ \t]+do\:.*\n             { printf(yytext); fflush(stdout); }
^[Tt]ype                       { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__type ); }
^[Pp]ublic[\ \t\n]+type        { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__p_type ); }
^[Vv]ariable                   { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__variable ); }
^[Pp]ublic[\ \t\n]+variable    { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__p_variable ); }
^[Tt]heorem                    { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__theorem ); }
^[Pp]ublic[\ \t\n]+theorem     { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__p_theorem ); }
^[Oo]peration                  { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__operation ); }
^[Gg]lobal[\ \t\n]+operation   { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__g_operation ); }
^[Dd]efine                     { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__operation ); }
^[Gg]lobal[\ \t\n]+define      { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__g_operation ); }
^[Pp]ublic[\ \t\n]+define      { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                 returntoken( yy__p_operation ); }
^[Rr]ead[\ \t]+                { BEGIN INCL; returntoken( yy__read ); }
^[Rr]eplaced[\ \t]+by[\ \t]+   { BEGIN INCL; returntoken( yy__replaced_by ); }
^C[\ \t]+constructors[\ \t]+for { BEGIN PAR; current_par_line = lineno; bracelevel = 0; bracketlevel = 0;
                                  returntoken( yy__C_constr_for ); }
<INCL>[^\ \t\n\r]+    { 
													BEGIN INITIAL; 
													returntoken( yy__anb_string ); 
											}
<INCL>\n                { lexical_kwread_error(); }
<INCL>.                 { lexical_kwread_error(); }
<<EOF>>                 {     yyterminate(); }
<PAR>[0-9]+                            { returntoken( yy__integer ); }
<PAR>[0-9]+\.[0-9]+                    { returntoken( yy__float ); }
<PAR>\$[A-Z][A-Za-z0-9\_]*             { returntoken( yy__utvar ); }
<PAR>[A-Z][A-Za-z0-9\_]*               { returntoken( yy__Symbol ); }
<PAR>[a-z\_][A-Za-z0-9\_]*             { returntoken( yy__symbol ); }
<PAR>\|\-[a-z\_][A-Za-z0-9\_]*\-\>     { returntoken( yy__rec_mapsto ); }
<PAR>\|\-[a-z\_][A-Za-z0-9\_]*\-\>\>   { returntoken( yy__rec_mapstoo ); }
<PAR>\�[A-Z][A-Za-z0-9\_]*             { returntoken( yy__Symbol ); }
<PAR>\�[a-z\_][A-Za-z0-9\_]*           { returntoken( yy__symbol ); }
<PAR>\�                                { returntoken( yy__symbol ); }
<PAR>[\t \r]                           { updateTokenStart(); }
<PAR>\n                                { updateTokenStart(); }
<PAR>.                                 { lexical_error(); }
\n                                 { updateTokenStart(); }
.                                  { updateTokenStart(); }
%%

//bool yywrap() { return true; }

void updateTokenStart()
{
  tokenStart = yyoffset;
  linenoStart = lineno;
  colnoStart = colno;
}

/* Definition of functions using the definitions lexer of states. */ 

/* storing a character into the string buffer 'str_buf'. */ 
void store_str_char(char c)
{
	if (str_index >= str_buf_size-1)
	{
		str_buf[str_buf_size-1] = 0; 
		BEGIN STRTL;
	}
	else
		str_buf[str_index++] = c; 
}

/* end of lexer -----------------------------------------------------------------------*/

