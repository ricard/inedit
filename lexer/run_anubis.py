import anubis
import sys
import time

import gc

def count_object_types():
    objs = gc.get_objects()
    objMap = {}
    for obj in objs:
        key = str(type(obj))
        if not objMap.has_key(key):
            objMap[key] = 0
        objMap[key] += 1
    print '\n********\n'
    for key, val in objMap.items():
        print key + '   ==>   ' + str(val)
        
    return objMap

def print_delta(map1, map2, types):
    objs = gc.get_objects()
    delta = {}
    for key in types:
        delta[key] = []
    for obj in objs:
        key = str(type(obj))
        if key in types:
            delta[key].append(obj)
    for key, objList in delta.items():
        print key, '     %d' % (map2[key] - map1[key])
        #for obj in objList[map1[key]:map2[key]]:
            #print '    ', obj
        
def print_all(types):
    objs = gc.get_objects()
    delta = {}
    for key in types:
        delta[key] = []
    for obj in objs:
        key = str(type(obj))
        if key in types:
            delta[key].append(obj)
    for key, objList in delta.items():
        print key
        for obj in objList:
            if key == "<type 'frame'>":
                print '    ', obj, obj.f_back
            else:
                print '    ', obj

def test_gc():
    print len(gc.get_objects())
    parser = anubis.Parser(keepfiles=1)
    count_object_types()
    print len(gc.get_objects())

    tree = parser.run(file=sys.argv[-1], debug=0)
    objMap1 = count_object_types()
    print len(gc.get_objects()), 'Parse 1'

    tree = parser.run(file=sys.argv[-1], debug=0)
    print len(gc.get_objects()), 'Parse 2'
    gc.collect()
    print len(gc.get_objects()), 'Parse 2'

    objMap2 = count_object_types()

    print 'tree REFERERS'
    print tree
    print '    --> ', gc.get_referrers(tree)[1:]
    
    print 'del parser'
    del parser
    print len(gc.get_objects())
    print 'collect'
    gc.collect()
    print len(gc.get_objects())

    print 'tree REFERERS'
    print tree
    print '    --> ', gc.get_referrers(tree)[1:]
    
    print 'del tree'
    del tree
    print len(gc.get_objects())
    print 'collect'
    gc.collect()
    print len(gc.get_objects())

    print '\nLeak:'
    print_delta(objMap1, objMap2, ("<type 'instance'>",))
    #print_all(("<type 'instance'>",))
    print len(gc.get_objects())

    return


    f = open(sys.argv[-1], 'rt')
    parser = anubis.Parser()
    print 'parser.getToken'
    token = parser.getToken(reset = True, file = f, debug = 0, verbose = 0)
    print len(gc.get_objects())
    while token:
        val = parser.token
        print token, '==>', val
        print '      pos =', parser.getTokenPos()
        tokenStart, tokenEnd, value = val.split(':', 4)[2:]
        token = parser.getToken(file = f)
    del token
    del parser
    print 'collect'
    gc.collect()
    print len(gc.get_objects())

    print gc.garbage
    for obj in gc.garbage:
        print repr(obj), '  -->\n', gc.get_referrers(obj)[2:], '\n'
    
    #for obj in gc.garbage:
        #del obj
    #print 'collect'
    #gc.collect()
    #print len(gc.get_objects())


if __name__ == "__main__":
    #gc.set_debug(gc.DEBUG_LEAK)
    gc.set_debug(gc.DEBUG_COLLECTABLE 
                 | gc.DEBUG_UNCOLLECTABLE 
                 | gc.DEBUG_INSTANCES 
                 | gc.DEBUG_OBJECTS)
                 #| gc.DEBUG_SAVEALL)
    #tree = anubis.main(*(sys.argv[1:]))
    #print tree.toprettyxml()
    #tree.dump()
    #open('output.xml', 'wt').write(tree.toprettyxml())
    #print '################################################################################'
    #tree = anubis.main(*(sys.argv[1:]))
    #tree.dump()
    #del tree
    #del parser
    for i in range(1):
        test_gc()
        