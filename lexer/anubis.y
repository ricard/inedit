/* grammar.y *****************************************************************

                                    Anubis
                             The language grammar. 

*****************************************************************************/ 
 
/* 
    This YACC/BISON file contains the grammar for the Anubis compiler. 

*/ 


%{

   
%}

/* types of data in the parser stack --------------------------*/




/* all (non one char) tokens begin by a double underscore -----------------------------*/ 
%token     yy__if yy__is yy__then yy__type yy__operation yy__variable yy__utvar yy__float yy__alert
%token     yy__theorem yy__p_theorem yy__proof yy__read yy__replaced_by yy__exchange
%token     yy__symbol yy__Symbol yy__lpar yy__lbracket yy__dot yy__rbracket yy__comma yy__bit_width
%token     yy__semicolon yy__plus yy__minus yy__star yy__carret yy__ampersand yy__vbar 
%token     yy__equals yy__write yy__percent
%token     yy__rbrace yy__lbrace yy__rpar yy__colon yy__implies yy__forall yy__exists yy__exists_unique
%token     yy__decimal_digit yy__anb_string yy__type_String yy__type_ByteArray yy__description
%token     yy__arrow yy__ndarrow yy__with yy__type_Int32 yy__type_Float yy__type_Listener yy__indirect
%token     yy__serialize yy__unserialize yy__vcopy yy__non_equal yy__load_module
%token     yy__wait_for yy__delegate yy__tilde yy__type_Omega yy__type_Proof
%token     yy__checking_every 
%token     yy__rpar_arrow yy__rpar_ndarrow yy__dots yy__g_operation
%token     yy__less yy__greater yy__lessoreq yy__greateroreq yy__mod yy__slash yy__else
%token     yy__RAddr yy__WAddr yy__RWAddr yy__succeeds yy__succeeds_as yy__connect_to_file 
%token     yy__C_constr_for yy__connect_to_IP yy__GAddr yy__Var yy__MVar yy__StructPtr
%token     yy__debug_avm yy__terminal yy__avm
%token     yy__left_shift yy__right_shift yy__since
%token     yy__p_operation yy__p_type yy__p_variable yy__protect yy__lock yy__alt_number
%token     yy__config_file yy__verbose yy__stop_after yy__mapsto yy__rec_mapsto yy__language
%token     yy__mapstoo yy__rec_mapstoo yy__arroww   
%token     yy__we_have yy__enough yy__let yy__assume yy__indeed yy__hence
   
%token  yy__integer yy__char

%token yy__comment


/* valued non terminals ----------------------------------------------------------------*/



/* precedence and associations rules ---------------------------------------------------*/
%right yy__protect yy__lock
%right yy__debug_avm yy__terminal
%right yy__assume yy__let yy__enough yy__we_have yy__hence
%right yy__comma
%right yy__mapsto yy__rec_mapsto  
%right yy__colon
%right yy__is yy__with
%right yy__then yy__else
%right yy__semicolon
%right prec_symbol yy__symbol
%right yy__rpar
%right prec_par_term
%right prec_of_type
%right prec_sym_type
%right yy__vbar
%right yy__ampersand
%right yy__implies yy__left_shift yy__right_shift
%right yy__tilde   
%right yy__less yy__greater yy__lessoreq yy__greateroreq yy__equals yy__write yy__non_equal yy__exchange
%right yy__connect_to_file yy__connect_to_IP 
%right yy__mod
%right yy__plus
%left  yy__minus
%right yy__star
%right yy__percent   
%left  yy__slash yy__dot
%right yy__carret
%right unaryminus
%right yy__arrow yy__ndarrow yy__rpar_arrow yy__rpar_ndarrow
%right yy__lpar yy__lbrace yy__lbracket



%start Start


%%





Start :            Text                                 
;

Text :             Paragraphs                           
; 

Paragraphs :
|                 Paragraphs Paragraph                 
; 



Paragraph :        Par  
;





Par :     TypeDefinition                       
|        OperationDefinition                  
|        OperationDeclaration                 
|        VariableDeclaration                  
|        Theorem                              
|        C_constr                             
|        yy__read yy__anb_string              
|        yy__replaced_by yy__anb_string
|        error yy__dot                        
;  


C_constr :   yy__C_constr_for yy__Symbol yy__equals Type yy__dot    
;

OpKW :      
    yy__operation                                  
|   yy__p_operation                                
|   yy__g_operation                                
; 
   
ThKW :
    yy__theorem                                    
|   yy__p_theorem                                  
;   
   
VarKW :     
    yy__variable                                   
|   yy__p_variable                                 
;    
   
TypeKW :    yy__type     
|          yy__p_type   
;
   
TypeDefinition :   TypeKW yy__Symbol yy__equals Type yy__dot
|                  TypeKW yy__Symbol yy__colon Alternatives
|                  TypeKW yy__Symbol yy__lpar TypeVars1 yy__rpar yy__colon Alternatives
;

TypeVars1 :       yy__utvar                         
|                yy__utvar yy__comma TypeVars1     
; 

SymbolOrDecimalDigit :     yy__symbol               
|                         yy__decimal_digit        
; 






Alternatives :   yy__dot                              
|               yy__dots                             
|               Alternatives1                        
;   
   
Alternatives1 :  
    Alternative yy__dot                              
|   Alternative yy__comma yy__dots                   
|   Alternative yy__comma Alternatives1              
;

Alternative :  
    yy__symbol                                       
|   yy__decimal_digit                                
|   yy__lbracket yy__rbracket                        
|   yy__symbol yy__lpar AltOperands1 yy__rpar        
|   yy__lbracket AltOperand yy__dot AltOperand yy__rbracket 
|   AltOperand yy__plus AltOperand                     
|   AltOperand yy__star AltOperand                     
|   AltOperand yy__percent AltOperand                  
|   AltOperand yy__carret AltOperand                   
|   AltOperand yy__vbar AltOperand                     
|   AltOperand yy__ampersand AltOperand                
|   AltOperand yy__arrow AltOperand                    
|   AltOperand yy__equals AltOperand                   
|   AltOperand yy__implies AltOperand                  
|   AltOperand yy__left_shift AltOperand               
|   AltOperand yy__right_shift AltOperand              
|   AltOperand yy__minus AltOperand                    
|   AltOperand yy__slash AltOperand                    
|   AltOperand yy__mod AltOperand yy__rpar             
|   AltOperand yy__less AltOperand                     
|   AltOperand yy__non_equal AltOperand                
|   AltOperand yy__lessoreq AltOperand                 
|   yy__tilde AltOperand                               
|   yy__star  AltOperand                               
|   error
;

AltOperands1 :     
    AltOperand                                         
|   AltOperand yy__comma AltOperands1                  
; 

AltOperand :   
    Type yy__symbol                                    
|   Type              %prec yy__comma                  
; 







Type :             
    yy__lpar Type yy__rpar                         
|   yy__Symbol    %prec prec_sym_type              
|   yy__utvar                                      
|   yy__type_String                                
|   yy__type_ByteArray                             
|   yy__type_Int32                                 
|   yy__type_Proof yy__lpar Term yy__rpar          
|   yy__type_Float                                 
|   yy__type_Listener                              
   
|   yy__RAddr yy__lpar Type yy__rpar               
|   yy__WAddr yy__lpar Type yy__rpar               
|   yy__RWAddr yy__lpar Type yy__rpar              
   
|   yy__GAddr yy__lpar Type yy__rpar               
|   yy__Var yy__lpar Type yy__rpar                 
|   yy__MVar yy__lpar Type yy__rpar                
|   yy__Symbol yy__lpar Types1 yy__rpar            
|   yy__StructPtr yy__lpar yy__Symbol yy__rpar     
                                                   
|   Type yy__arrow Type                            
|   yy__Symbol yy__lpar Types1 yy__rpar_arrow Type 
|   yy__lpar TypesArgs1 yy__rpar_arrow Type        
|   yy__lpar Types2 yy__rpar                       
|   yy__lbrace Type yy__rbrace                     
; 

Types1 :      
    Type                                           
|   Type yy__comma Types1                          
;  

TypesArgs1 :      
    Type                                           
|   Type yy__symbol                                
|   Type yy__comma TypesArgs1                      
|   Type yy__symbol yy__comma TypesArgs1           
;  

Types2 :        
    Type yy__comma Type                            
|   Type yy__comma Types2                          
;
 






VariableDeclaration : VarKW Type yy__symbol yy__equals Term yy__dot 
; 





OperationDefinition : OpKW Type SymbolOrDecimalDigit yy__equals Term yy__dot
|                     OpKW Type yy__symbol yy__lpar OpArgs yy__rpar yy__equals Term yy__dot
|                     OpKW Type  OpArg SimpleBinaryOp OpArg  yy__equals Term yy__dot
|                     OpKW Type SimpleUnaryOp OpArg  yy__equals Term yy__dot
|                     OpKW Type  OpArg yy__mod OpArg yy__rpar yy__equals Term yy__dot
|                     OpKW Type  OpArg SimpleBinaryOp OpArg  yy__dot
|                     OpKW Type SimpleUnaryOp OpArg  yy__dot
|                     OpKW Type  OpArg yy__mod OpArg yy__rpar yy__dot
;



OperationDeclaration : OpKW Type yy__symbol yy__lpar OpArgs yy__rpar yy__dot
|                      OpKW Type yy__symbol yy__dot
;


SimpleBinaryOp :     yy__plus                  
|                   yy__star                  
|                   yy__percent               
|                   yy__carret                
|                   yy__vbar                  
|                   yy__ampersand             
|                   yy__arrow                 
|                   yy__equals                
|                   yy__implies               
|                   yy__left_shift            
|                   yy__right_shift           
|                   yy__minus                 
|                   yy__slash                 
|                   yy__less                  
|                   yy__non_equal             
|                   yy__lessoreq              
; 

SimpleUnaryOp :      yy__minus                 
|                   yy__tilde                 
; 







OpArgs :   
|           OpArgs1                      
; 

OpArgs1 :    OpArg                        
|           OpArg yy__comma OpArgs1      
; 

OpArg :      Type yy__symbol              
|            error
; 



   
FArgs1 :    FArg                            
|          FArg yy__comma FArgs1           
;
   
FArg :      Type yy__symbol                 
;    


Term : yy__alert                                     
|     yy__alt_number yy__lpar Term yy__rpar         
|     yy__protect Term                              
|     yy__lock Term yy__comma Term                  
|     yy__debug_avm Term                            
|     yy__avm yy__lbrace AVM yy__rbrace             
|     yy__terminal Term                             
|     yy__symbol                %prec prec_symbol   
|     yy__lpar yy__rpar           %prec prec_symbol 
|     yy__integer               %prec prec_symbol   
|     yy__char                  %prec prec_symbol   
|     yy__float                 %prec prec_symbol   
|     yy__lpar Term yy__rpar            %prec prec_par_term   
|     yy__lpar Terms2 yy__rpar          %prec prec_par_term   
|     yy__lpar Type yy__rpar Term           %prec prec_of_type 
|     yy__lpar yy__colon Term yy__rpar Term %prec prec_of_type 
|     yy__lpar yy__colon Type yy__rpar Term %prec prec_of_type 
|     AppTerm                                     
|     Conditional                                 
|     yy__lbracket List                           
|     yy__star Term                               
|     Term yy__write Term                         
|     Term yy__exchange Term                      
|     Term yy__semicolon Term                    
|     yy__load_module yy__lpar Term yy__rpar              
|     yy__serialize yy__lpar Term yy__rpar                
|     yy__unserialize yy__lpar Term yy__rpar              
|     yy__bit_width yy__lpar Type yy__rpar                
|     yy__indirect yy__lpar Type yy__rpar                 
|     yy__vcopy yy__lpar Term yy__comma Term yy__rpar     
|     yy__lpar Type yy__rpar yy__connect_to_file Term     
|     yy__lpar Type yy__rpar yy__connect_to_IP Term yy__colon Term  
|     yy__anb_string                                                
|     yy__with yy__symbol yy__equals Term yy__comma WithTerm        
|     yy__checking_every Term yy__milliseconds yy__comma yy__wait_for Term yy__then Term  
|     yy__delegate Term yy__comma Term                      
|     yy__lbrace yy__symbol yy__colon Type yy__comma Term yy__rbrace 
|     yy__lpar FArgs1 yy__rpar yy__mapsto Term            
|     yy__lpar FArgs1 yy__rpar yy__rec_mapsto Term        
|     yy__forall yy__symbol yy__colon Type yy__comma Term 
|     yy__forall yy__symbol yy__colon Term yy__comma Term 
|     yy__exists yy__symbol yy__colon Type yy__comma Term 
|     yy__exists_unique yy__symbol yy__colon Type yy__comma Term 
|     yy__description yy__symbol yy__colon Type yy__comma Term   
|     yy__we_have Term yy__dot Justif Term %prec yy__we_have             
|     yy__enough Term yy__dot Justif Term  %prec yy__enough         
|     yy__assume Term yy__dot Term    %prec yy__assume              
|     yy__let yy__symbol yy__colon Term yy__dot Term %prec yy__let  
|     Hence Term                  %prec yy__hence                   
|     error
; 

Terms2 :         Term yy__comma Term          
|               Term yy__comma Terms2        
; 



WithTerm :       Term   %prec yy__comma                          
|               yy__symbol yy__equals Term yy__comma WithTerm   
; 

   
   
List :      yy__rbracket                  
|          Term yy__rbracket             
|          Term yy__comma List                 
|          Term yy__dot Term yy__rbracket
;




AppTerm : Term yy__lpar Terms yy__rpar    
|         Term yy__lbracket List 
|         Term yy__plus        Term 
|         Term yy__star        Term 
|         Term yy__percent     Term 
|         Term yy__carret      Term 
|         Term yy__vbar        Term 
|         Term yy__ampersand   Term 
|         Term yy__arrow       Term 
|         Term yy__equals      Term 
|         Term yy__implies     Term 
|         Term yy__left_shift  Term 
|         Term yy__right_shift Term 
|         Term yy__minus       Term 
|         Term yy__slash       Term 
|         Term yy__less        Term 
|         Term yy__non_equal   Term 
|         Term yy__greater     Term 
|         Term yy__lessoreq    Term 
|         Term yy__greateroreq Term 
|         Term yy__mod Term yy__rpar
|         yy__minus Term %prec unaryminus 
|         yy__tilde Term  
; 

Terms :                                 
|           Terms1                     
; 

Terms1 :     Term                       
|           Term yy__comma Terms1      
; 




Conditional :   yy__if Term yy__is yy__lbrace Clauses yy__rbrace 
|               yy__if Term yy__succeeds_as yy__symbol yy__then Term
|               yy__if Term yy__succeeds yy__then Term
|               yy__if Term yy__is Clause 
|               yy__since Term yy__is Head yy__comma Term 
|               yy__if Term yy__then Term yy__else Term
|               yy__if Term yy__is Clause yy__else Term  
; 



   
Clauses :                                    
|              Clause Clauses               
|              Clause yy__comma Clauses     
;
   
Clause :       Head yy__then Term             
; 

Head :         yy__symbol                             
|             yy__decimal_digit                      
|             yy__lbracket yy__rbracket              
|             yy__symbol yy__lpar yy__rpar           
|             yy__symbol yy__lpar ResurSym1 yy__rpar 
|             yy__lbracket ResurSym yy__dot ResurSym yy__rbracket 
|              ResurSym yy__plus ResurSym           
|              ResurSym yy__star ResurSym           
|              ResurSym yy__percent ResurSym        
|              ResurSym yy__carret ResurSym         
|              ResurSym yy__vbar ResurSym           
|              ResurSym yy__ampersand ResurSym      
|              ResurSym yy__arrow ResurSym          
|              ResurSym yy__equals ResurSym         
|              ResurSym yy__implies ResurSym        
|              ResurSym yy__left_shift ResurSym     
|              ResurSym yy__right_shift ResurSym    
|              ResurSym yy__minus ResurSym          
|              ResurSym yy__slash ResurSym          
|              ResurSym yy__mod ResurSym yy__rpar   
|              ResurSym yy__less ResurSym           
|              ResurSym yy__non_equal ResurSym      
|              ResurSym yy__lessoreq ResurSym       
|              yy__tilde ResurSym                   
|              yy__lpar ResurSym2 yy__rpar          
;

ResurSym :      yy__symbol                   
|              Type yy__symbol              
;

ResurSym1 :     ResurSym                     
|              ResurSym yy__comma ResurSym1 
; 

ResurSym2 :     ResurSym yy__comma ResurSym  
|              ResurSym yy__comma ResurSym2 
;

   
AVM :      
|      AVM_Instr AVM 
; 

AVM_Instr :     yy__symbol 
|              yy__lpar yy__symbol IntMCons 
;

IntMCons :      yy__rpar                   
|              yy__dot yy__integer yy__rpar
|              yy__integer IntMCons       
|              yy__anb_string IntMCons    
;

   
   
yy__milliseconds :    yy__symbol 
; 
   
Theorem :       ThKW yy__symbol yy__colon Term yy__dot yy__proof yy__colon Term yy__dot 
;
   
  
Hence :         yy__hence              
|              yy__hence yy__comma    
;    
   
Justif :              
|              yy__indeed Term yy__dot  
;    

   
%%

