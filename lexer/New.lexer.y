/* lexer.y *******************************************************************************

                                             Anubis 1. 
                                    The lexer of the compiler. 

*****************************************************************************************/

/*
    This LEX/FLEX file is the lexer for the Anubis compiler. 
*/ 


%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "compil.h"
#include "grammar.tab.h"       /* declarations produced by BISON from file 'grammar.y' */ 

int current_par_line = 0;      /* line number of first line of paragraph currently read */ 
   
   
/*** Managing external (off paragraph) comments (used when making an index). ***/ 
char *ext_com = NULL; 
int ext_com_index = 0; 
int ext_com_size = 1000; 

  
/* We may have to unput at most one token */    
int unput_token = 0; // since David's initialisation 
   

   
/* Constructing the lexer */   
int yylex(void);        /* this  is the  function called  by yyparse  (it is  defined just
                           below) */ 
int anb_yylex(void);    /* this is the actual lexer constructed by FLEX */ 
   
   
/* Definition of the function  called by the parser (yyparse). If a  token has been unput,
   we return it. Otherwise, we get a new token from the actual lexer. */ 
int yylex(void)
   {
     if (unput_token != 0) 
       {
         int aux = unput_token; 
         unput_token = 0; 
         return aux;
       }
     return anb_yylex(); 
   }

   
/* This declaration is  required by FLEX. It tells  FLEX that the name of the  lexer to be
   constructed is anb_yylex. */ 
#define YY_DECL    int anb_yylex(void)     
/* YY_PROTO(( void )) */

   
   
/* Managing character  strings. We  use the  buffer 'str_buf' for  reading the  content of
   character strings. */ 
#define str_buf_size (10000)
int str_index = 0; 
char str_buf[str_buf_size]; 

/* This function is  used for storing one  character into the buffer. It  is defined after
   the lexer proper, because it uses the definition of the state 'STRTL'. */ 
void store_str_char(char);

   
   
/* Nested includes  of files. In Anubis, files  may be read recursively  using the keyword
   'read'. The compiler searches for files in the following order: 

   1. in the current directory, 
   2. in 'my_anubis'
   3. in 'anubis'
   
   Definition of the current directory: The compiler is started in some directory, that we
   call the 'starting' directory, and which  may be any directory. The 'current' directory
   is the same as the starting directory when the compiler starts.
   
   When a 'read' is encountered, the  'current' directory may change. It works as follows.
   Assume that we  encounter 'read gaga/toto.anubis', and that  the 'current' directory is
   'cr'. The file 'toto.anubis' is first searched for in 'cr/gaga/'.
   
      1. If it is found, the current directory becomes 'cr/gaga/'
   
      2. If  it  is not  found,  the  file is  searched  for  in  'my_anubis/gaga' and  in
           'anubis'/gaga', in  this order. If  again it is  not found, the  compiler stops
           with a  message.  If it is found,  the 'current' directory does  not change and
           remains 'cr'.
   
   When the file  'toto.anubis' has been read, the current directory  returns to its value
   before the 'read' paragraph, i.e. 'cr' in all cases.
  
*/
   
   
/* When we read a new file, we must push into stacks a lot of informations. First of all a
   YY_BUFFER_STATE, because FLEX  needs to be able  to return to the previous  file at the
   right place. We  must push the path of  the file currently being read,  and the current
   line number in the previous file. The  current directory is managed by another stack in
   'mallocz.c'.  */ 
   
   
/* the number of nested includes is limited. */    
#define                  max_include      (100)
      
YY_BUFFER_STATE      include_stack[max_include];   /* buffer states for FLEX */ 
char *               include_names[max_include];   /* absolute paths of files */ 
int                  include_lines[max_include];   /* current line number in files */ 
char *               include_dir[max_include];     /* current directory */ 

int                  include_stack_ptr = 0;        /* common stack pointer */

/* The 4 stacks: 'include_stack'
                 'include_lines'
                 'include_names'
                 'include_dir' 
   
   are growing in parallel. The common stack pointer is 'include_stack_ptr'. */    
   
YY_BUFFER_STATE new_yy_buffer; 

   
/* We also record the absolute paths of files which have been already read in. */    
char *already_included[max_already_included];   // paths of included files
int next_already_included = 0; 

#define path_prefix_length (3000)
char path_prefix_buf[path_prefix_length];   
   
char *path_prefix(char *name)   
  {
    int i = strlen(name); 
    if (i >= path_prefix_length-5)
      {
        err_line_col(linecol()); 
        fprintf(errfile,
                msgtext_file_path_too_long[language],
                name);
        anb_exit(1); 
      }
   
    while (i > 0)
      {
        i--; 
        if (name[i] == '\\' || name[i] == '/') break; 
      }
    if (i == 0) /* either no slash or only one slash at position 0 */ 
      {
        path_prefix_buf[0] = 0; /* empty path */ 
      }
    else
      {
        int j = 0; 
        while (j < i) { path_prefix_buf[j] = name[j]; j++; }
        path_prefix_buf[j] = 0; 
      }
    return path_prefix_buf; 
  }
   
      
/* When the compiler successfully opens a source  file, the absolute path of this file may
   be found  here. This  character string is  allocated each  time a file  is successfully
   open. Hence it must be used or freed. */ 
char *fopensrc_abs_path; 
/* Similarly, the new  current directory is found  here. It is also allocated  each time a
   source file is opened. */ 
char *fopensrc_cur_dir; 
   
extern   void includePathsCheckFile(char *name);

/* open an Anubis source file. */ 
FILE *fopensrc(char *name)
{
  /* (1) try to open the file from the current directory */ 
  FILE *result;
   
  /* compute absolute path of file */ 
  if (name[0] == '/' || name[0] == '\\')
    fopensrc_abs_path = name;
  else
    fopensrc_abs_path = strdupppz(include_dir[include_stack_ptr],"/",name); 
   
  /* open the file */ 
  result = fopen(fopensrc_abs_path,"rt"); 
   
  if (result != NULL) 
   {
     fopensrc_cur_dir = strdupz(path_prefix(fopensrc_abs_path)); 
     return result; 
   }

  /* if path is absolute do not try the libraries */ 
  if (name[0] == '/' || name[0] == '\\') 
    {
      err_line_col(linecol()); 
      fprintf(errfile,
              msgtext_cannot_find_src_file[language],
              name,
              include_dir[include_stack_ptr],
              my_library_directory,
              library_directory); 
      anb_exit(1); 
      return NULL; 
    }
  else 
    freez(fopensrc_abs_path); 
  
  includePathsCheckFile(name);

  /* (2) try to open the file from the private library */ 

  if (strlen(fopensrc_abs_path))
  {
    result = fopen(fopensrc_abs_path,"rt");
    if (result != NULL) 
    {
        fopensrc_cur_dir  = strdupz(path_prefix(fopensrc_abs_path)); 
        return result;
    }
  }

  freez(fopensrc_abs_path);

//   
//  /* (3) try to open the file from the shared library directory */ 
//  fopensrc_abs_path = strdupppz(library_directory,"/",name); 
//  result = fopen(fopensrc_abs_path,"rt");
//  if (result != NULL) 
//    {
//      fopensrc_cur_dir  = strdupppz(library_directory,"/",path_prefix(name)); 
//      return result;
//    }
//  else
//    {
//      freez(fopensrc_abs_path); 
//    }
   
  /* The file is not found: send a message and exit */ 
  err_line_col(linecol()); 
  fprintf(errfile,
          msgtext_cannot_find_src_file[language],
          name,
          include_dir[include_stack_ptr],
          my_library_directory,
          library_directory); 
  anb_exit(1);
  return NULL; 
}
   
   
   
 
   
/* managing visibility of files */ 
U16 visibility[max_already_included][max_already_included];
U16 number_of_visible[max_already_included];
U8  is_relay_file[max_already_included];
   
struct MissingRead_st {
    U16 which;
    U16 from;
    U32 line;
      }; 
   
#define max_missing_read  (max_already_included*max_already_included)
struct MissingRead_st missing_reads[max_missing_read]; 
int next_missing_read = 0; 
   
void add_missing_read(int which)
    {
      int i; 
      for (i = 0; i < next_missing_read; i++)
        if (which == missing_reads[i].which &&
            file_id == missing_reads[i].from)
          return; 
      missing_reads[next_missing_read].which = which; 
      missing_reads[next_missing_read].from = file_id; 
      missing_reads[next_missing_read].line = current_par_line;
      next_missing_read++; 
    }
   
void show_missing_reads(void)
    {
      int i; 
      FILE *fp;
   
      fp = fopenz("add_missing_reads.anubis.data","wt"); 
   
      fprintf(fp,"\n\npublic type MissingRead:\n  mr(String which, String from, Int32 line).\n\n"); 
      fprintf(fp,"\n\npublic define List(MissingRead) missing_reads = [\n"); 
   
      for (i = 0; i < next_missing_read; i++)
        fprintf(fp,"  mr(\"%s\",\"%s\",%d),\n",
               already_included[missing_reads[i].which],
               already_included[missing_reads[i].from],
               (int)(missing_reads[i].line)); 
      fprintf(fp,"  ].\n\n"); 
      fclose(fp); 
    }
   
void add_to_visible(U16 which, U16 from)
    {
      int i;
      //if (show_reads) fprintf(show_reads_file,"--- Adding visibility of %d from %d\n",which,from); 
      for (i = 0; i < number_of_visible[from]; i++)
        {
	  if (visibility[from][i] == which) return;
        }
      visibility[from][(number_of_visible[from])] = which;
      number_of_visible[from]++; 
    }
   
   
void initialize_visibility(void)
    {
      int i; 
      for (i = 0; i < max_already_included; i++)
        {
          is_relay_file[i] = 0; 
          number_of_visible[i] = 0;
        }
      visibility[0][0] = 0;             // predef visible from predef
      number_of_visible[0] = 1; 
      visibility[1][0] = 0;             // predef visible from main file
      visibility[1][1] = 1;             // main file visible from main file
      number_of_visible[1] = 2; 
    }

   
int is_visible(char *filename)   // visibility from current file (= file_id)
    {
      int i; 
      int id = get_file_id(filename);
   
      if (!read1) return 1; 
   
      if (phase == 2) return 1; 
      if (id == 0) return 1;   // predef.anubis
   
      for (i = 0; i < number_of_visible[file_id]; i++)
        if (visibility[file_id][i] == id)
          return 1;
      return 0; 
    }

   
/* The next function make all files visible from 'f1' also visible from 'f2'. */ 
void transmit_visibility(U16 f1, U16 f2)
   {
     int j; 
   
     for(j = 0; j < number_of_visible[f1]; j++)
       {
       add_to_visible(visibility[f1][j],f2);
       }
   }
   
   
   
void add_to_already_included(char *name)
    {
      //printf("Adding '%s'.\n",name);
      if (next_already_included == max_already_included)
	{
          err_line_col(linecol()); 
	  fprintf(errfile,
		  msgtext_too_many_files[language]); 
	  anb_exit(1); 
	}
      already_included[next_already_included++] = strdupz(name); 
    }

   
   
   
   
   
int is_already_included(char *name)
    {
      int i; 
      FILE *fp; 
   
      fp = fopensrc(name); 
   
      for (i = 0; i < next_already_included; i++)
        {
	if (!strcmp(already_included[i],fopensrc_abs_path))
          {
            fclose(fp); 
	    return 1; 
          }
        }
      fclose(fp); 
      return 0; 
    }

   
   
int get_file_id(char *name)
    {
      int i; 
      for (i = 0; i < next_already_included; i++)
        {
          if (!strcmp(name,already_included[i]))
            return i; 
        }
      fprintf(stderr,"'%s' not found among included files.\n",name); 
      fflush(stderr); 
      anb_exit(1); 
      return 0; 
    }
   

   
   
int show_reads = 0; 
FILE *show_reads_file = NULL;    
   
static void print_push(void)
   {
     int i; 
     for (i = 0; i < include_stack_ptr; i++)
       fprintf(show_reads_file,"  "); 
     fprintf(show_reads_file,"> "); 
   }
   
static void print_pop(void)
   {
     int i; 
     for (i = 0; i < include_stack_ptr; i++)
       fprintf(show_reads_file,"  "); 
     fprintf(show_reads_file,"< "); 
   }
   

void show_visible_files(U16 from, char *filename)
   {
     int i; 
     assert(show_reads_file != NULL); 
     fprintf(show_reads_file,"                               --- visible files ---\n");
     for (i = 0; i < number_of_visible[from]; i++)
       fprintf(show_reads_file,"                               %s\n",
               already_included[(visibility[from][i])] == (void *)NULL ?
               filename : already_included[(visibility[from][i])]); 
     fprintf(show_reads_file,"                               ---------------------\n");
   }
   
void register_read(char *filename,int file_id)   
   {
     assert(show_reads_file != NULL); 
   
     if (is_already_included(filename))
       {
         print_push();
         fprintf(show_reads_file,"%s (already read)\n",filename); 
       }
     else
       {
         print_push(); 
         fprintf(show_reads_file,"%s\n",filename); 
       }  
     show_visible_files((U16)file_id,filename); 
   }
   
void show_come_back(char *filename)
   {
     assert(show_reads_file != NULL); 
     print_pop(); 
     fprintf(show_reads_file,"%s\n",filename); 
     show_visible_files((U16)get_file_id(filename),filename); 
   }
   
void finish_show_reads(void)
   {
     int i; 
     if (show_reads_file == NULL) return; 
     fprintf(show_reads_file,msgtext_show_reads_file_text[language]); 
     for (i = 1; i < next_already_included; i++)
       fprintf(show_reads_file,"[%d] %s\n",
                         i,already_included[i]); 
     fprintf(show_reads_file,"\n"); 
     fclose(show_reads_file); 
   }
   

   
   
   
#define YY_NO_UNPUT


/* counting lines and columns -------------------------------------------------*/
int lineno = 1;
int colno = 1; 
int file_id = 0;       /* index of current file in 'already_included' */ 
int comlevel = 0;
int tab_seen = 0; 
int tab_width = 4; 


/* updating line and column counters ---------------------------------------------*/
void update_lc(void)
{
   int i = 0;
   while (yytext[i] != 0)
     {
	if (yytext[i] == '\t') { tab_seen = 1; colno += tab_width; }
	if (yytext[i] == '\n') { lineno++; colno = 1; }
	else                   colno++; 
	i++; 
     } 
}

#define YY_USER_ACTION           update_lc(); 


/* return current line, column and file id into an expression integer -------------*/ 
Expr linecol(void)
{
  return new_integer((file_id<<23)|(lineno<<8)|colno); 
}

void end_of_par(void);

void lexical_error(void)
{
  err_line_col(linecol()); 
  fprintf(errfile,
	  msgtext_lexical_error[language],yytext[0]); 
  anb_exit(5); 
}

void lexical_kwread_error(void)
{
   err_line_col(linecol()); 
   fprintf(errfile,
	   msgtext_lexical_kwread_error[language]); 
   anb_exit(5); 
}

   
/* The function 'store_external_comment' is used while making the global index. 
 */
static void store_external_comment(char c)
{
  if (ext_com_index+10 >= ext_com_size)
    {
      ext_com_size += 1000; 
      ext_com = (char *)reallocz(ext_com,ext_com_size); 
    }
  ext_com[ext_com_index++] = c; 
}



/* extract 'name' from '|-name->' */ 
static char *rec_name(char *s, int del)
{
  snprintf(str_buf,str_buf_size-10,s+2); 
  str_buf[strlen(s)-del] = 0; 
  return str_buf;
}
   
   

%}



%option noyywrap



/* states ------------------------------------------------------------------------------*/
%x COM
%x PAR
%x INCL
%x STR
%x STRTL
%x CONF
%x CONFCOM
   
/* state STRTL is used when a string is too long. */ 
/* states CONF and CONFCOM are used to read 'compiler.conf'.*/ 

/* the lexer ----------------------------------------------------------------------------*/
%%
<PAR>"/*"                         { comlevel = 1; BEGIN COM; }
<COM>"/*"                         { comlevel++; }
<COM>"*/"                         { comlevel--; if (!comlevel) { BEGIN PAR; } }
<COM>\n                           { }
<COM>.                            { }
<PAR>\/\/.*$                      { /* dbl_slash_comment(yytext+2); */ }
<PAR>\"                           { BEGIN STR; str_index = 0; }
<STR>\"                           { BEGIN PAR; 
                                    str_buf[str_index] = 0; 
                                    yylval.expr = new_string(str_buf); 
                                    return yy__anb_string; }
<STR>\\\"                         { store_str_char('\"'); }
<STR>\\n                          { store_str_char('\n'); }
<STR>\\r                          { store_str_char('\r'); }
<STR>\\t                          { store_str_char('\t'); }
<STR>\\\\                         { store_str_char('\\'); }
<STR>\n                           { store_str_char('\n'); }
<STR>.                            { store_str_char(yytext[0]); }
<STRTL>\"                         { BEGIN PAR; }
<STRTL>\n                         { }
<STRTL>.                          { }
<PAR>\'[^\'\n\t\r\"]\'            { yylval.integer = (int)(unsigned char)yytext[1]; return yy__char; }
<PAR>\'\\n\'                      { yylval.integer = (int)'\n'; return yy__char; }
<PAR>\'\\r\'                      { yylval.integer = (int)'\r'; return yy__char; }
<PAR>\'\\t\'                      { yylval.integer = (int)'\t'; return yy__char; }
<PAR>\'\\\"\'                     { yylval.integer = (int)'\"'; return yy__char; }
<PAR>\'\\\\\'                     { yylval.integer = (int)'\\'; return yy__char; }
<PAR>\'\\\'\'                     { yylval.integer = (int)'\''; return yy__char; }
<PAR>"("                               { yylval.expr = linecol(); return yy__lpar; }
<PAR>")"                               { yylval.expr = linecol(); return yy__rpar; }
<PAR>\,[\ \t\r\n]*\)                   { yylval.expr = linecol(); return yy__rpar; }
<PAR>"{"                               { yylval.expr = linecol(); return yy__lbrace; }
<PAR>"}"                               { yylval.expr = linecol(); return yy__rbrace; }
<PAR>\,[\ \t\r\n]*\}                   { yylval.expr = linecol(); return yy__rbrace; }
<PAR>"["                               { yylval.expr = linecol(); return yy__lbracket; }
<PAR>"]"                               { yylval.expr = linecol(); return yy__rbracket; }
<PAR>\,[\t\n\r\ ]*\]                   { yylval.expr = linecol(); return yy__rbracket; }
<PAR>":"                               { yylval.expr = linecol(); return yy__colon; }
<PAR>";"                               { yylval.expr = linecol(); return yy__semicolon; }
<PAR>"-"                               { yylval.expr = linecol(); return yy__minus; }
<PAR>"."                               { yylval.expr = linecol(); return yy__dot; }
<PAR>\%                                { yylval.expr = linecol(); return yy__percent; }
<PAR>","                               { yylval.expr = linecol(); return yy__comma; }
<PAR>"~"                               { yylval.expr = linecol(); return yy__tilde; }
<PAR>"="                               { yylval.expr = linecol(); return yy__equals; }
<PAR>"/="                              { yylval.expr = linecol(); return yy__non_equal; }
<PAR>"+"                               { yylval.expr = linecol(); return yy__plus; }
<PAR>"*"                               { yylval.expr = linecol(); return yy__star; }
<PAR>"^"                               { yylval.expr = linecol(); return yy__carret; }
<PAR>"\&"                              { yylval.expr = linecol(); return yy__ampersand; }
<PAR>"|"                               { yylval.expr = linecol(); return yy__vbar; }
<PAR>">>"                              { yylval.expr = linecol(); return yy__right_shift; }
<PAR>"<<"                              { yylval.expr = linecol(); return yy__left_shift; }
<PAR>"=>"                              { yylval.expr = linecol(); return yy__implies; }
<PAR>"?"                               { yylval.expr = linecol(); return yy__forall; }
<PAR>"!"                               { yylval.expr = linecol(); return yy__exists; }
<PAR>"!!"                              { yylval.expr = linecol(); return yy__exists_unique; }
<PAR>"#!"                              { yylval.expr = linecol(); return yy__description; }
<PAR>"/"                               { yylval.expr = linecol(); return yy__slash; }
<PAR>\([\t ]*mod[\t ]                  { yylval.expr = linecol(); return yy__mod; }
<PAR>"<"                               { yylval.expr = linecol(); return yy__less; }
<PAR>">"                               { yylval.expr = linecol(); return yy__greater; }
<PAR>"=<"                              { yylval.expr = linecol(); return yy__lessoreq; }
<PAR>">="                              { yylval.expr = linecol(); return yy__greateroreq; }
<PAR>"<-"                              { yylval.expr = linecol(); return yy__write; }
<PAR>"<->"                             { yylval.expr = linecol(); return yy__exchange; }
<PAR>"->"                              { yylval.expr = linecol(); return yy__arrow; }
<PAR>"->>"                             { yylval.expr = linecol(); return yy__arroww; }
<PAR>"|->"                             { yylval.expr = linecol(); return yy__mapsto; }
<PAR>"|->>"                            { yylval.expr = linecol(); return yy__mapstoo; }
<PAR>"..."                             { yylval.expr = linecol(); return yy__dots; }
<PAR>\)[ \t\n]*\-\>                    { yylval.expr = linecol(); return yy__rpar_arrow; }
<PAR>String                            { yylval.expr = linecol(); return yy__type_String; }
<PAR>ByteArray                         { yylval.expr = linecol(); return yy__type_ByteArray; }
<PAR>Int32                             { yylval.expr = linecol(); return yy__type_Int32; }
<PAR>Omega                             { yylval.expr = linecol(); return yy__type_Omega; }
<PAR>bit_width                         { yylval.expr = linecol(); return yy__bit_width; }
<PAR>is_indirect_type                  { yylval.expr = linecol(); return yy__indirect; }
<PAR>\�vcopy                           { if (reading_predef)
                                           { yylval.expr = linecol(); return yy__vcopy; }
                                         else lexical_error(); }
<PAR>\�load_module                     { if (reading_predef)
                                           { yylval.expr = linecol(); return yy__load_module; }
                                         else lexical_error(); }
<PAR>\�serialize                       { if (reading_predef)
                                           { yylval.expr = linecol(); return yy__serialize; }
                                         else lexical_error(); }
<PAR>\�unserialize                     { if (reading_predef)
                                           { yylval.expr = linecol(); return yy__unserialize; }
                                         else lexical_error(); }
<PAR>Float                             { yylval.expr = linecol(); return yy__type_Float; }
<PAR>\�Listener                        { if (reading_predef)
                                          { yylval.expr = linecol(); return yy__type_Listener; }
                                         else lexical_error(); }
<PAR>\�StructPtr                       { if (reading_predef)
                                          { yylval.expr = linecol(); return yy__StructPtr; }
                                         else lexical_error(); }
<PAR>\�avm                             { if (reading_predef)
                                          { yylval.expr = linecol(); return yy__avm; }
                                         else lexical_error(); }
<PAR>if                                { yylval.expr = linecol(); return yy__if; }
<PAR>^[Pp]roof                         { yylval.expr = linecol(); return yy__proof; }
<PAR>Proof                             { yylval.expr = linecol(); return yy__type_Proof; }
<PAR>since                             { yylval.expr = linecol(); return yy__since; }
<PAR>is                                { return yy__is; }
<PAR>then                              { yylval.expr = linecol(); return yy__then; }
<PAR>alert                             { yylval.expr = linecol(); return yy__alert; }
<PAR>protect                           { yylval.expr = linecol(); return yy__protect; }
<PAR>lock                              { yylval.expr = linecol(); return yy__lock; }
<PAR>succeeds                          { yylval.expr = linecol(); return yy__succeeds; }
<PAR>succeeds[\ \t\n]+as               { yylval.expr = linecol(); return yy__succeeds_as; }
<PAR>wait[\ \t\n]+for                  { yylval.expr = linecol(); return yy__wait_for; }
<PAR>checking[\ \t\n]+every            { yylval.expr = linecol(); return yy__checking_every; }
<PAR>delegate                          { yylval.expr = linecol(); return yy__delegate; }
<PAR>else                              { yylval.expr = linecol(); return yy__else; }
<PAR>with                              { yylval.expr = linecol(); return yy__with; }
<PAR>alternative_number                { yylval.expr = linecol(); return yy__alt_number; }
<PAR>RAddr                             { yylval.expr = linecol(); return yy__RAddr; }
<PAR>WAddr                             { yylval.expr = linecol(); return yy__WAddr; }
<PAR>RWAddr                            { yylval.expr = linecol(); return yy__RWAddr; }
<PAR>GAddr                             { yylval.expr = linecol(); return yy__GAddr; }
<PAR>Var                               { yylval.expr = linecol(); return yy__Var; }
<PAR>MVar                              { yylval.expr = linecol(); return yy__MVar; }
<PAR>connect[\ \t\n]+to[\ \t\n]+file    { yylval.expr = linecol(); return yy__connect_to_file; }
<PAR>connect[\ \t\n]+to[\ \t\n]+network { yylval.expr = linecol(); return yy__connect_to_IP; }
<PAR>debug                             { yylval.expr = linecol(); return yy__debug_avm; }
<PAR>terminal                          { yylval.expr = linecol(); return yy__terminal; }
<PAR>[Ww]e[\ \t\n]+have                { yylval.expr = linecol(); return yy__we_have; }
<PAR>[Ii]t[\ \t\n]+is[\ \t\n]+enough[\ \t\n]+to[\ \t\n]+prove([\ \t\n]+that)? { yylval.expr = linecol(); 
                                         return yy__enough; } 
<PAR>[Ll]et                            { yylval.expr = linecol(); return yy__let; } 
<PAR>[Aa]ssume([\ \t\n]+that)?         { yylval.expr = linecol(); return yy__assume; }
<PAR>[Hh]ence                          { yylval.expr = linecol(); return yy__hence; } 
<PAR>[Ii]ndeed                         { yylval.expr = linecol(); return yy__indeed; } 
<CONFCOM>^stop[\ \t]+after                  { BEGIN CONF; return yy__stop_after; }
<CONFCOM>^djed                              { BEGIN CONF; return yy__djed; }
<CONFCOM>^verbose                           { return yy__verbose; }
<CONFCOM>^language                          { BEGIN CONF; return yy__language; }
<CONFCOM>^[^\ \n\r]        { fprintf(errfile,msgtext_syntax_error_in_conf_file[language],
                             my_anubis_directory,lineno); anb_exit(1); }
<CONFCOM>\n                                 { }
<CONFCOM>.                                  { }
<CONF>\"[^\"]*\"                            { 
                                              yytext[yyleng-1] = 0; 
                                              yylval.expr = new_string(yytext+1); 
                                              yytext[yyleng-1] = '\"'; 
                                              return yy__conf_string; }
<CONF>[\/\\a-zA-Z\_][\/\\a-zA-Z0-9\_\-\.\:]* { 
                                              yylval.expr = new_string(yytext); 
                                              return yy__conf_symbol; }
<CONF>[0-9]+                                { yylval.expr = new_integer(atoi(yytext)); return yy__conf_int; }
<CONF>[\ \t]                                { }
<CONF>\n                                    { BEGIN CONFCOM; }
<CONF>.                    { BEGIN CONFCOM; fprintf(errfile,msgtext_syntax_error_in_conf_file[language],
                             my_anubis_directory,lineno); anb_exit(1); }
^to[\ \t]+do\:.*\n             { printf(yytext); fflush(stdout); }
^[Tt]ype                       { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__type; }
^[Pp]ublic[\ \t\n]+type        { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__p_type; }
^[Vv]ariable                   { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__variable; }
^[Pp]ublic[\ \t\n]+variable    { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__p_variable; }
^[Tt]heorem                    { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__theorem; }
^[Pp]ublic[\ \t\n]+theorem     { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__p_theorem; }
^[Oo]peration                  { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__operation; }
^[Gg]lobal[\ \t\n]+operation   { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__g_operation; }
^[Dd]efine                     { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__operation; }
^[Gg]lobal[\ \t\n]+define      { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__g_operation; }
^[Pp]ublic[\ \t\n]+define      { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                 yylval.expr = linecol(); return yy__p_operation; }
^[Rr]ead[\ \t]+                { if (polish) { yylval.expr = linecol(); BEGIN INCL; 
                                               return yy__read; }
                                 else if (!gindex && !errors) { BEGIN INCL; } }
^[Rr]eplaced[\ \t]+by[\ \t]+   { is_relay_file[file_id] = 1; 
                                 //printf("'%s' is a relay file.\n",source_file_name);
                                 if (polish) { yylval.expr = linecol(); BEGIN INCL; 
                                               return yy__replaced_by; }
                                 else if (!gindex && !errors) { BEGIN INCL; } }
^C[\ \t]+constructors[\ \t]+for { BEGIN PAR; par_seen = 1; current_par_line = lineno; 
                                  yylval.expr = linecol(); return yy__C_constr_for; }
<INCL>[^\ \t\n\r]+    { if (polish) 
                          {
                            yylval.expr = new_string(yytext); 
                            BEGIN INITIAL; 
                            return yy__anb_string; 
                          }
                        else if (is_already_included(yytext))
                        /* 'is_already_included' sets the variable 'fopensrc_abs_path'.
                           it does'nt change the value of 'file_id'. */ 
                          {
                            char *path = fopensrc_abs_path; 
                            if (verbose) printf("%s is already included.\n",path); 
                            include_stack_ptr++; 
                            if (show_reads) register_read(yytext,get_file_id(path)); 
                            include_stack_ptr--; 
                            if (show_reads) show_come_back(source_file_name);
                            add_to_visible((U16)get_file_id(path),file_id); 
                            //add_to_visible((U16)get_file_id(path),(U16)get_file_id(source_file_name)); 
                            BEGIN INITIAL; 
                          }
                        else
                          { 
                            if (is_relay_file[file_id])
                              {
                                printf(msgtext_relay_file[language],
                                       source_file_name,
                                       fopensrc_abs_path);
                              }        
                            if (include_stack_ptr >= max_include)
                            {
                              err_line_col(linecol()); 
			      fprintf(errfile,
				      msgtext_include_too_deeply[language],
				      max_include);  
			      anb_exit(1); 
			    }
			  yyin = fopensrc(yytext); 
                          include_stack[include_stack_ptr] = YY_CURRENT_BUFFER; 
			  include_lines[include_stack_ptr] = lineno; 
			  lineno = 1; 
			  colno = 1; 
			  include_stack_ptr++;
			  source_file_name = include_names[include_stack_ptr] = fopensrc_abs_path; 
                          include_dir[include_stack_ptr] = fopensrc_cur_dir; 
                          if (verbose) printf("Push current directory: %s\n",fopensrc_cur_dir); 
                          if (!reading_predef) assert(next_already_included >= 2); 
                          visibility[next_already_included][0] = 0;  // predef visible from this file
                          visibility[next_already_included][1] = next_already_included; 
                          // this file visible from itself
                          number_of_visible[next_already_included] = 2;
                          if (!reading_predef) assert(next_already_included >= 2); 
                          if (show_reads) register_read(source_file_name,next_already_included);
                          add_to_already_included(source_file_name);  
                          file_id = get_file_id(source_file_name); 
			  if (verbose) printf(msgtext_compiling_file[language],source_file_name); 
			  if ((new_yy_buffer = yy_create_buffer(yyin,YY_BUF_SIZE)) == NULL)
			    {
			      fprintf(errfile,
				      msgtext_out_of_memory[language]); 
			      anb_exit(1); 
			    }
			  yy_switch_to_buffer(new_yy_buffer); 
			  BEGIN INITIAL; }}
<INCL>\n                { lexical_kwread_error(); }
<INCL>.                 { lexical_kwread_error(); }
<<EOF>>                 { if (include_stack_ptr == 0)
                            {
                              yyterminate(); 
                            }
                          else
			    { 
                              int previous_file_id = get_file_id(source_file_name); 
                              char * previous_source_file_name = strdup(source_file_name); 
			      fclose(yyin); 
                              yy_delete_buffer(YY_CURRENT_BUFFER); 
			      freez(include_names[include_stack_ptr]); 
                              if (verbose) printf("Pop current directory: %s\n",
                                                  include_dir[include_stack_ptr]); 
			      freez(include_dir[include_stack_ptr]); 
			      include_stack_ptr--; 
                              if (verbose) printf("Returning to directory: %s\n",
                                                  include_dir[include_stack_ptr]); 
			      source_file_name = include_names[include_stack_ptr]; 
                              file_id = get_file_id(source_file_name); 
                              /* if we are leaving a relay file everything visible from it 
                                 should be visible from the previous one. */
                              if (is_relay_file[previous_file_id])
                                {
                                  transmit_visibility(get_file_id(previous_source_file_name),
                                                      get_file_id(source_file_name)); 
                                }
                              add_to_visible((U16)previous_file_id,(U16)file_id);
                              if (show_reads) show_come_back(source_file_name); 
			      lineno = include_lines[include_stack_ptr]; 
			      colno = 1; 
			      if (verbose) printf(msgtext_returning_to_file[language],source_file_name);
			      yy_switch_to_buffer(include_stack[include_stack_ptr]);
			      //if (board_option) fprintf(stderr,"\n"); 
			      current_name = ""; 
                              free(previous_source_file_name);
                              end_of_par(); 
			    }
                        }
<PAR>[0-9]+                            { yylval.integer = atoi(yytext); return yy__integer; }
<PAR>[0-9]+\.[0-9]+                    { yylval.expr = new_string(yytext); return yy__float; }
<PAR>\$[A-Z][A-Za-z0-9\_]*             { yylval.expr = new_utvar(yytext+1); return yy__utvar; }
<PAR>[A-Z][A-Za-z0-9\_]*               { yylval.expr = new_string(yytext); return yy__Symbol; }
<PAR>[a-z\_][A-Za-z0-9\_]*             { yylval.expr = new_string(yytext); return yy__symbol; }
<PAR>\|\-[a-z\_][A-Za-z0-9\_]*\-\>     { yylval.expr = new_string(rec_name(yytext,4)); 
                                         return yy__rec_mapsto; }
<PAR>\|\-[a-z\_][A-Za-z0-9\_]*\-\>\>   { yylval.expr = new_string(rec_name(yytext,5)); 
                                         return yy__rec_mapstoo; }
<PAR>\�[A-Z][A-Za-z0-9\_]*             { if (reading_predef)
                                           { yylval.expr = new_string(yytext); return yy__Symbol; }
                                         else lexical_error(); 
                                       }
<PAR>\�[a-z\_][A-Za-z0-9\_]*           { if (reading_predef)
                                           { yylval.expr = new_string(yytext); return yy__symbol; }
                                         else lexical_error(); 
                                       } 
<PAR>\�                                { if (reading_predef)
                                           { yylval.expr = new_string(yytext); return yy__symbol; }
                                         else lexical_error(); 
                                       } 
<PAR>[\t \r]                           { }
<PAR>\n                                { }
<PAR>.                                 { lexical_error(); }
\n                                 { if (gindex) store_external_comment('\n'); }
.                                  { if (gindex) store_external_comment(yytext[0]); }
%%


void end_of_par(void)
{
  BEGIN INITIAL;
      
  if (errors && !gindex)
    {
      incorrect_pars++;  
      if (incorrect_pars >= stop_after) anb_exit(1); 
    }
  errors = 0; 
  clean_up_pairs(); 
  more = 0; 
  ext_com_index = 0; 
}

/* Definition of functions using the definitions lexer of states. */ 

/* storing a character into the string buffer 'str_buf'. */ 
void store_str_char(char c)
   {
     if (str_index >= str_buf_size-1)
       {
	 err_line_col(linecol()); 
	 fprintf(stderr,
		 msgtext_string_too_long[language]); 
	 str_buf[str_buf_size-1] = 0; 
	 BEGIN STRTL;
       }
     else
       str_buf[str_index++] = c; 
   }

/* going to state 'CONFCOM' */ 
void begin_CONFCOM(void)
    {
      BEGIN CONFCOM; 
    }
   
/* going to state INITIAL */    
void begin_INITIAL(void)
    {
      BEGIN INITIAL; 
    }
   
/* end of lexer -----------------------------------------------------------------------*/
   
