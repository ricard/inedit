a = Analysis([os.path.join(HOMEPATH,'support\\_mountzlib.py'), os.path.join(HOMEPATH,'support\\useUnicode.py'), 'in-editIDE.py'],
             pathex=['D:\\Develop\\Python\\InEdit'],
             hookspath=['D:\\Develop\\Python\\InEdit\\hooks'])
data = Tree('lib/tool/data', 'lib/tool/data', ('.svn',))
#images = Tree('images', 'images', ('.svn',))
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts + [('O','','OPTION')],
          exclude_binaries=1,
          name='buildin-editIDE/in-editIDE.exe',
          debug=0,
          strip=0,
          upx=1,
          console=0 , version='version.txt', icon='lib\\tool\\data\\IDE.ico')
coll = COLLECT( exe,
               a.binaries,
               data, [('stc-styles.rc.cfg', 'stc-styles.rc.cfg', 'DATA')],
               strip=0,
               upx=1,
               name='distin-editIDE')
