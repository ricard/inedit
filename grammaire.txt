      Par
          : TypeDefinition
            : TypeKW yy__Symbol yy__equals Type yy__dot
            | TypeKW yy__Symbol 																					yy__colon Alternatives
            | TypeKW yy__Symbol yy__lpar TypeVars1 yy__rpar 							yy__colon Alternatives

          | OperationDefinition
            : OpKW Type SymbolOrDecimalDigit 															yy__equals Term yy__dot
            | OpKW Type yy__symbol yy__lpar OpArgs yy__rpar 							yy__equals Term yy__dot
            | OpKW Type OpArg SimpleBinaryOp OpArg 												yy__equals Term yy__dot
            | OpKW Type SimpleUnaryOp OpArg 															yy__equals Term yy__dot
            | OpKW Type OpArg yy__mod OpArg yy__rpar 											yy__equals Term yy__dot
            | OpKW Type OpArg SimpleBinaryOp OpArg yy__dot
            | OpKW Type SimpleUnaryOp OpArg yy__dot
            | OpKW Type OpArg yy__mod OpArg yy__rpar yy__dot

          | OperationDeclaration
            : OpKW Type yy__symbol yy__lpar OpArgs yy__rpar yy__dot
            | OpKW Type yy__symbol yy__dot

          | VariableDeclaration
            : VarKW Type yy__symbol yy__equals Term yy__dot

          | Theorem
            : ThKW yy__symbol yy__colon Term yy__dot yy__proof yy__colon Term yy__dot

          | C_constr
            : yy__C_constr_for yy__Symbol yy__equals Type yy__dot

          | yy__read yy__anb_string
          | yy__replaced_by yy__anb_string
          | error yy__dot
