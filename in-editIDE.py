#!/usr/bin/env python
#Boa:PyApp:main

#----------------------------------------------------------------------------
# Name:         ActiveGridIDE.py
# Purpose:
#
# Author:       Lawrence Bruhmuller
#
# Created:      3/30/05
# CVS-ID:       $Id: in-editIDE.py,v 1.2 2005/07/06 17:18:50 ricard Exp $
# Copyright:    (c) 2005 I-Team.
# License:      wxWindows License
#----------------------------------------------------------------------------

import wx.lib.pydocview
import lib.tool.IDE

import os
import sys
import gc
#sys.stdout = sys.stderr

# This is here as the base IDE entry point.  Only difference is that -baseide is passed.

#sys.argv.append('-baseide');

# Put lib dir in path so python files can be found from py2exe
# This code should never do anything when run from the python interpreter
execDir = os.path.dirname(sys.executable)
try:
    sys.path.index(execDir)
except ValueError:
    sys.path.append(execDir)
app = lib.tool.IDE.IDEApplication(redirect = False)
app.GetTopWindow().Raise()  # sometimes it shows up beneath other windows.  e.g. running self in debugger
app.MainLoop()

#if "-debug" in sys.argv or "/debug" in sys.argv:
    #del app
    #gc.collect()
    #print gc.garbage